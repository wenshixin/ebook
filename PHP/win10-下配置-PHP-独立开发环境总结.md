>昨晚强迫症犯了，觉得自己电脑很乱，就重装了系统，借此也安装一下 PHP 的独立开发环境，之前都是用的集成环境 Wampserver， 在安装中遇到的一些坑，总结一下。

本文所用到的软件：[云盘下载](https://pan.baidu.com/s/1pLE4U9t) 密码：jwga

## 1.安装 Apache 服务器

Apache 的官网上是不能下载 Apache 服务器的安装包的，只提供 Apache 的源码，下载下来需要用 VC++ 重新编译一下，并且还要配置不少东西（心里有千万个草泥马驶过...）。所以不采用这种方法，而是在网上直接找 Apache 的安装版，这里我用的是 64 位的Apache HTTP server2.4 版本的 msi安装包。

安装完成后，我们来测试一下是否 Apache 安装成功。打开浏览器，在地址栏输入 localhost，或者 127.0.0.1，还可以是自己电脑上的ipv4地址，这三种输入都是可以的。看到 `It works！` 则说明安装成功。

**更改默认的网站位置**

在上面，我们访问 localhost 时，使用的是 Apache 安装目录中的 htdocs 文件夹里面的 index.html 文件，这个htdocs 文件夹也就是 Apache 的默认网站位置。在平时的开发中，放在这个文件夹下肯定是不方便的，下面我们就来修改它。修改也是很简单的，打开 Apache 安装目录中的 conf 文件夹下的 httpd.conf 文件，修改 `DocumentRoot` 后面的路径为你想要存放网站的位置，紧接着同样修改下面 `Directory` 中的路径和上面的一致。记住每次修改完 Apache 的配置文件 httpd.conf 都要重启一下 Apache 服务器才能使修改生效。在新的网站位置，我们可以新建 html 文件来测试是否修改成功，此时还没有安装 PHP，所以不能用 PHP 文件测试哦。

![修改默认网站目录](http://upload-images.jianshu.io/upload_images/5763525-11e06861ad6e80e5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**两点注意**：
- 1. 安装 Apache 的路径不要有中文出现，否则会启动失败，建议安装在一个独立的文件夹下，我的安装目录如下。
![安装目录](http://upload-images.jianshu.io/upload_images/5763525-7a828408302093ab.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 2.软件的位数，因为之后 Apache 和 PHP 解释器联合时，如果软件位数不一致，Apache 在加载 PHP 的 module 时会出现找不到的错误，Apache 是 64 位的，安装的php也要是 64 位的，云盘里里面的软件都是64位的。

## 2.安装 PHP

PHP 是可以在官网上下载的，PHP7 早已经发布了，但这里还是选择当前使用最广泛的版本PHP5.6 [下载传送门](http://php.net/downloads.php#v5.6.31)。我们在下载完 PHP 后，只需要把 PHP 解压到想安装的文件夹即可。然后修改目录中的  `php.ini-development` 文件名为 `php.ini`，我这里是复制了一份进行修改的。
![PHP目录](http://upload-images.jianshu.io/upload_images/5763525-ddd27052f279b49d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 3.PHP 和 Apache 的联合

Apache 和 PHP 安装好后，两者还是互相不认识的，如何让他们认识呢，我们就需要在 Apache 中加载 PHP。在Apache 的 配置文件 httpd.conf 中，我们加上下面几行（#后面的内容是注释），注意文件路径中是左斜杠。修改完后重启 Apache 服务器。

```
#1.加载PHP模块
LoadModule php5_module E:/php5.6/php5apache2_4.dll
#2.配置php.ini文件的正确路径
PHPIniDir E:/php5.6
#3.添加PHP类型文件到Apache服务器中
AddType application/x-httpd-php .php .html .htm
```
![Apache和PHP联合](http://upload-images.jianshu.io/upload_images/5763525-9771354a22b4f9c9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

下面是我们修改网站的默认主页，搜索 `DirectoryIndex` ，在后面添加 index.php 即可。

![修改网站的默认主页](http://upload-images.jianshu.io/upload_images/5763525-883328cbfe998fdb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


下面我们就测试一下 Apache 是否和 PHP联合成功了，我们可以在刚才我们修改过的网站目录下，新建一个 index.php，在里面写上如下代码保存，再在浏览器中输入 localhost，看到 PHP 的相关信息则说明 Apache 和 PHP 的联合成功。

```php
<?php 
phpinfo();
?>
```
![PHP信息](http://upload-images.jianshu.io/upload_images/5763525-e18b17fdb419a222.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



## 4.安装Mysql

安装 Mysql 就没有什么特别需要需注意的地方啦，一路 Next 都是可以的，当然你也可以根据自己需要选择要安装的组件。

## 5.PHP 开启 Mysql 的相关扩展

PHP 和 Mysql 建立关系，是通过 开启相应的 PHP 扩展来实现的。这是我们要打开 PHP 安装目录下的 php.ini 。查找里面的 mysql 相关的语句，删除每条语句前面的分号，这里分号也是注释的意思，记者重启 Apache 服务器。

![开启Mysql相关扩展](http://upload-images.jianshu.io/upload_images/5763525-592cdaa19dac1780.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**测试方法可有以下两种**：
- 1.使用我们之前写的 index.php 文件，在浏览器地址中输入 localhost，搜索 mysql 相关的信息，看到下图所示的信息，则可说明，Mysql相关的扩展开启成功。

![Mysql开启成功](http://upload-images.jianshu.io/upload_images/5763525-035204b6b28f28b8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 2.连接数据库来进行测试，可以将之前写的 index.php 文件里面的代码换成下面的代码。

```php
<?php 
$mysql = new mysqli('localhost','在这里写上你的mysql数据库管理员账号名','在这里写上账号对应的密码');
if($mysql->conncet_errno)
{
	die('数据库连接失败'.$mysql->conncet_error);
}
echo "数据库连接成功！";
?>
```

再次在浏览器中输入 localhost，显示 `数据库连接成功！`，则也可说明 Mysql 扩展开启成功！报的 undefined 信息可以忽略，也可以将其去除，修改方法是，在php.ini，修改 `error_reporting = E_ALL` 为 `error_reporting = E_ALL & ~E_NOTICE`

![去除未定义信息](http://upload-images.jianshu.io/upload_images/5763525-dcdabeeb36d25c12.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


>好了，到这里，我们的 PHP 独立开发环境就算是安装完成啦。当然对于初学者还是建议使用集成环境， phpstudy，Wampserver这些集成开发环境都不错的，这样不会因为环境而影响学习一门语言，入门 PHP 后，可以再试着装一装 PHP 的独立环境。
