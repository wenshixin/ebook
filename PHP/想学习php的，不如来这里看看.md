![php语言图片](http://upload-images.jianshu.io/upload_images/5763525-5399af2343924ce2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 基本命令
win+R打开命令行，cmd进DOS窗口
DOS命令开启关闭Apache和Mysql
Apache启动关闭命令
 ```
httpd -k stop  
httpd -k start  
httpd -k restart
 ```
Mysql启动关闭命令
```
net stop mysql
net stop mysql
```
### Apache默认首页
`index.php index.html index.htm //优先级从左到右`
在httpd.conf文件里面有优先级的定义

### php语言的简单介绍
php不是强类型语言，是弱类型语言（解析型语言），不需要提前声明，由程序自己判断，和强类型语言不同，不需要编译后再使用，apache可以调用php解析器直接解析php代码，html中直接嵌套php代码，当前要是在php文件中
### 基本知识
php中变量名区分大小写，函数名和类名不区分大小写，所以干脆变量名和函数名全小写
  utf-8编码时，一个中文三个字符
  gbk编码时，一个中文两个字符

### 变量输出

      echo $name;//输出变量
      var_dump($name);//输出数组，并且打印类型和长度
      print_r($name);//输出数组，但不打印类型和长度

- 1.定义普通变量

      $name = "小明";

- 2.可变变量

      $str = "name";
      $$str = "小红";//等价于$name = "小红";

- 3.变量引用
  
      $a = $b	是把$b的值赋给$a
      $a = &$b 是把$b家的地址给$a

- 4.输出
echo输出一个布尔类型时，true会变成1，false会变成空

- 5.资源
	连接数据库资源``` $conn = mysql_connect("主机名","用户名","密码");```
	文件打开资源
	目录打开资源
### 基本函数
- 6.isset函数
判断变量是否存在,没有定义或者为null

- 7.empty函数
判断变量是否为空,0  "" "0" false array() null 没有定义  这7种都为空的情况

- 8.变量类型测试函数
```
整型 is_int();
浮点型 is_float();
字符型 is_string();
布尔型 is_bool();
数组 is_array();
对象 is_object();
资源 is_resource();
null类型 is_null();
```

### 变量类型
标量类型 整型 浮点型 字符型 布尔型
复合类型 数组 对象
特殊类型 资源 null类型
```
is_scalar() 测试是否是标量
is_numeric 测试是否是数字 整型 浮点型
is_callable() 测试是否是函数，语言结构则会返回false
 ```
- 9.语言结构
```
echo()不是函数，虽然后面也可以加括号，和函数类似
print()也是语言结构，而printf是函数
array()也是语言结构
list()也是语言结构
```
- 10.类型转换
只针对标量
自动转换 和C语言类似
	整形 -> 字符串	3 -> "3"
		```$num = 123;```
		```echo $num."abc";```
	字符型 -> 整型	"3" ->3
		```$str = "123";```
		```echo $str + 1;```
	其他类型 -> 布尔类型 和empty()函数里面为空的情况一样
		```$num = 0;//会转成bool类型的假false```
		
```
if($num){
//如果为真则执行
} 
else{
}
```
强制类型转换 和C语言一样
(类型名)变量 例如```(float)$num```

- 11.删除变量
```unset($name);括号里面是变量名```

- 12.字符串的单双引号区别
  - 单引号的执行速度比双引号速度快，双引号在输出时还要检查走过的是否是变量
  - 变量在单引号中不能被解析，而在双引号中可以被解析
  - 如果字符串中没有变量就用单引号，如果字符串中有变量就用双引号

### php中的常量
- 13.常量的定义
和变量一样，但是一旦被定义就不能被修改，常量名最好大写便于区分，类似于C语言的宏定义
例如：
```
define("HOST","localhost"); 
define("USER","root");
define("PASS","123");
define("DBNAME","test");
```
数据库的配置文件就要用到常量，而且后面不能被修改
常量不能被放到双引号里面或者花括号里面，要用.来连接常量

- 14.预定义常量
```
PHP_OS	系统平台
PHP_VERSION	php当前的版本
__LINE__	该行代码在第几个行
__FILE__	文件的绝对路径
__FUNCTION__	函数的名字
M_PI	圆周率
```
- 15.运算符 (文件名不要出现中文，否则可能会有错误)
@运算符 屏蔽函数的错误信息，做调试时使用
数组运算符 =>

    - 索引数组
```
$arr = array("use1","use2","use3");
echo $arr[1];
```
-
    -  关联数组
```
$arr = array("name"=>"user","age"=>"30");
echo "<pre>";
print_r($arr);
echo "</pre>";
```
对象运算符 -> 对象使用 
逻辑运算符
开关功能 && 如果前面为真，才会判断后面的，和C语言一样，面试会考，但没有实际意义

- 16.date函数
```
date("Y-m-d");系统的年月日，Y，m，d的顺序是可以换的哦
date("w");系统的周几
```

- 17.表格的隔行换色代码
header头改编码格式 utf-8，gbk
```
header("content-type:text/html;charset = utf-8");
<?php
	$days = 31;
	echo "<table width = '700px', border = '1px'>";
	for($i = 1, $k = 1; $i <= $days;){
		//在行产生的时候判断行数来确定行的颜色
		if($k % 2 == 1){
			echo "<tr bgcolor = 'grey'>";
		}
		else{
			echo "<tr style = 'background:blue'>";
		}
		//七天为一个周期
		for($j = 0; $j < 7; $j++){
			if($i > $days){
				echo "<td> </td>";
			}
			else{
				echo "<td>{$i}</td>";
			}
			$i++;
		}
		echo "</tr>";
		$k++;
	}
	echo "</table>";
?>
```
- 18.终止脚本函数
```
exit();后面的()可以不用写
die();后面的()可以不用写，并且可以在里面写上内容
例如:die("脚本终止");
```
- 19.php的函数理解

    - php的函数可以传值，也可以是带有默认函数参数的，这点和C++一样

    - 函数的输出和返回，易错点，返回值的函数较常用，然后想输出的函数的结果时在输出，而不用一调用函数就会把函数的结果打印在屏幕上 

    - 可变参数的函数，C语言里也可以实现，php是自身带的函数来实现，func_num_args()可以获取当前传入函数的参数个数，func_get_args()得到传入的参数具体值，以数组的方式储存，再用array_sum(func_get_args())就可以得到传入的数据之和
变量函数，把函数的名字赋给一个变量，就可以用变量来使用函数的功能，例如：$a = "sum",sum是一个函数的名字，echo $a(1, 2);类似于C++的变量引用

    - 变量的作用域类似于C语言，global关键字可以把函数内的局部变量变成全局变量，但最好不要用，会干扰函数外的同名变量，如果想通过函数改变函数外部变量的值，可以使用函数参数的引用方式，这点和C++的方法一样，实质是先找到变量的地址，在修改地址里面的值

    - 回调函数，函数的参数是另一个函数，或者说是参数的一种

    - 静态变量和C语言的关键字一样，static

### 文件包含
include()和require()的区别
  - 1. include(),括号里面是另一个文件的名字，但还要注意文件的路径问题，当然同目录下的文件包含，路径问题不用管
- 2. require(),和include()的用法相同，但如果包含的文件找不到，就会终止php脚本，不会执行下面的代码（不管代码正确与否），而在include中仍会执行


- 20.php的执行过程
	加载页面，语法检测（加载函数），执行脚本

- 21.php数组
	数组是由多个元素组成，每个元素由key-value，value有八种数据类型
	索引数组和关联数组的混合使用,关联数组的使用不影响索引数组的下标
	例如：```$arr = array("name" => 1, 2, 3, 100 => 4, 5, "age" =>6)```，它们的下标打印为"name", 0, 1, 100, 101,"age"
	数组取值printf_r($arr);
	数组赋值，```$arr['age'] = 30```,数组赋值也可以定义数组，```$arr[] = 1; $arr[] = 2;```这些内容意义不大
#### 三种方法来进行数组遍历
for循环来进行带数字索引数组遍历，而foreach()可以遍历一切数组，foreach($arr as $key => $val),as就是从$arr中取值$val，然后再在循环里面打印数组的值，关联数组索引被称为键值对，list和each遍历数组，感觉麻烦，```while(list($key,$val) = each($arr)){echo "<h1>{$key}:{$val}</h1>";```
	多维数组，数组里面套数组，例如二维数组:```$arr = array(1, 2, array(4,5));```
	一张数据表其实就是一个二维数组，里面的每一行记录就是一个一维数组

22.超全局数组
```
$_SERVER 查看服务器信息，用print_r打印服务器相关信息，不要用echo
$_GET 获取get提交过来的数据，两个页面之间通讯，表单传值（get方式，post方式），
a标签传值（get传值），get传值的可以在地址上看到，不安全，
a标签推荐使用get提交数据，表单推荐使用post处传值方式，
而$_POST获取表单post传过来的数据，$_REQUEST获取a标签或者表单get或者post过来的数据
$_REQUEST 等于$_GET和$_POST
cookie和session前面不能有输出
$_COOKIE 同一个变量在多个页面获取到
$_SESSION 同一个变量在多个页面获取到
$_FILES 获取表单中的文件，并生成一个数组
$GLOBALS 里面包含页面内的全局变量，在函数里面改变一个外部变量的值，可以用$GLOBALS[$name] = "...";
```
- 23.数组函数
数组的键值操作函数
```
	array_keys();获取数组中的键，参数是数组名
	array_vals();获取数组中的值，参数是数组名
	array_key_exists();检查一个键是否在数组中	
	in_array();检查一个值是否在数组中，里面的参数是"值"和数组名
	array_flip();键和值对调，参数是数组名
	array_reverse();数组中的值反转，也就是函数返回一个和原来数组顺序相反的数组
```
统计数组的元素和唯一性
```
	count();统计数组元素个数
	array_count_values();统计数组中相同值的个数，并返回一个新的数组
	array_unique();删除数组中重复的值
```
使用回调函数处理数组的函数（过滤）
```
	array_filter();数组值过滤，筛选出符合条件的值，参数是数组名和条件
	array_map();将回调函数作用到给定数组的元素上，参数是函数和数组，返回一个新数组
```
数组的排序函数
```
	sort();升序排列，不保留key
	rsort();降序排列，不保留key
	asort();保留key升序排列
	arsort();保留key降序排列
	ksort();根据值对key升序排列，排列后key和value互换
	krsort();根据值对key降序排列，排列后key和value互换
	natsort();用自然排序算法升序排列，如果想降序排列，可以再使用array_reverse()函数倒转
	natcasesort();忽略大小写升序排列
	array_multisort();多个数组进行排序
```
拆分、合并、分解与结合函数

	explode();拆分函数，例如：explode("-",$arr);
	join();//implode();都是合并函数 
	array_slice();
参数是变量名，起始标量，终止标量，例如：``array($arr, 0, 3);```正数是从左到右截取，负数是从右到左截取，变成一个数组，而原来的数组不变

    array_splice();
用法和array_slice类似，不一样的是它不但产生一个新数组，还会把原数组变成截取后剩下的部分，更厉害的是它还可以在原数组裁剪的地方添加元素，例如```array_splice($arr, 0, 3, array("ff", "gg", "hh"));```所添加的元素会放到原数组的被裁剪的地方

	array_merge($a, $b);合并两个数组变成一个数组
	array_combine($a, $b);和并联个数组使前面的数组变成后面数组的下标	
	array_intersect();取两个数组的交集
	array_diff();前面的作为参照物，取后面数组中的和前面不一样的元素，求差集

 #### 数组与数据结构
	array_pop()弹出数组的最后一个元素，改变原数组，并返回一个新数组，和unset()不一样
	array_push();返回新数组的元素个数，改变原数组
	array_shift();在数组最前面弹出一个值，返回移出值，原数组下标重排
	array_unshift();在数组前面插入一个值，返回数组个数

其他有用的数组处理函数

	array_rand();随机取一个key
	shuffle();打乱一个数组
	array_sum;求数组所有值的和
	range();获取一个范围内的数组，两个参数，可以做验证码
	
- 24.字符串
	- echo和print的区别，echo可以连接多个字符串，例如：echo "aaa","bbb","cccc";而print不能
	- printf可以格式化输出，和C语言一样，有格式说明符%s，$d
	- sprintf也可以格式化，但不直接输出，而是返回一个字符串
	用点.来连接字符串
	
字符串函数
```
	去除空格和字符串填补的函数（解决用户名因多敲了空格而造成不能登录）
	ltrim();去除字符串左边的空格
	rtrim();去除字符串右边的空格
	trim();去除字符串左右两边的空格
	str_pad();填充字符串的长度，使用另一个字符串填充字符串为指定长度，
例如echo str_pad($str, "-", (STR_PAD_LEFT));最后一个参数可写可不写
	str_repeat(); 将一个字符串重复多少次，例如echo str_repeat("_",3),输出 ___
	strlen();获取字符串长度
```
字符串大小写转换函数
```
	strtoupper();
	strtolower();
	ucfirst();字符串的首字母大写
	ucwords();字符串的每一个单词的首字母都大写
```	

与html相关联的字符串函数（结合着数据库来看）
```
	nl2br();把\n转成br标签，例如echo nl2br("aaaa\nbbb\ccc");和其他语言交互的时候使用可以换行
	htmlspecialchars();转实体，转“、< > &”，对于恶意输入，原样输出
	strip_tags();去掉html标签，也可以保留一部分，在参数后面加上所要保留的标签，记着加双引号
	addslashes();转义' " \ ,在它们前面加上\,和C语言的转义字符一样，默认php开启
	stripslashes();去掉addslashes默认加的\
```
字符串比较函数（ASCII码）
```	
1.按字节进行字符串的比较
	strcmp($str1, $str2);比较字符串的每个字节
	strcasecmp();忽略大小写比较字符串的每个字节
2.按自然排序时字符串的比较
	strnatcmp();按自然排序比较字符串中的数字
	strnatcasecmp();按自然排序忽略比较字符串中的数字
```
字符串的分割与拼接（和前面数组部分一样）

	1.分割 explode();把字符串分割成数组
	2.拼接implode()（join()）;把字符串拼接成字符串
	
字符串的截取
```
substr();参数为变量名，起始位置坐标（从0开始），终止位置坐标。
 需要注意编码问题，可以统一字符编码，
例如：（这个是支持多字节的）echo mb_substr($str, 0, 6,"utf-8"),
不管是中文字符还是英文的字符，都会截取前6个
```	
查询字符串
```
	1.strstr();查找指定字符在字符串中的第一次出现，返回一个字符串，但不够精确
	2.strrchr();查找指定字符在字符串中的最后一次出现
	3.strpos();查找指定字符在字符串第一次出现的位置
	4.strrpos($str, 'w');w在$str中最后一次出现的位置
```
字符串替换
```	
1.str_replace();参数是两个字符串和一个字符串名字，后面的替换前面的，但是想替换多个不同的字符串，
可以把它们放在一个数组中，然后用数组作为第一个参数，就可以实现多个替换了，
并且还可以实现多个替换多个，就是把第二个参数也变成数组，两个数组对应替换
```
字符串拆分常用函数

	1.pathinfo($str);
	2.parse_url($str);专门针对url地址拆分
	3.parse_str($str['query'], $arr);拆分更具体
	4.preg_split('/ /',$srr['']);正则拆分

其他字符串函数
```
	strrev();字符串翻转
	strlen();字符串长度
	number_format();格式化数字字符串，每三个字符加一个（，），
    还可以选择保留几位小数，可以按照自己的要求格式化
	md5();md5加密后的字符串，加密后是一个32位字符串
	str_shuffle();随机输出字符串，和数组里面的shuffle一样
```
- 25.建议在数据插入数据库之前进行三道把控
```
	1.[b]aaaaaa[/b]  UBB代码，使用strip_tags过滤一部分html标签
	2.addslashes()
	' " \ 前加上\，防止对数据库造成破坏
	3.htmlspecialchars()
	将' " < > & 转成实体，防止对数据库造成破坏
```
- 26.正则表达式
正则表达式是用于描述字符排列和匹配模式的一种语法规则，它主要用于字符串的模式分割，匹配，查找及替换操作，在PHP中正则表达式一般是由正规字符和一些特殊字符联合构成的一个文本模式的程序性描述，本次采用perl兼容的正则表达式
	- 1.原子
		.（点） 代表任意一个字符，但不能匹配换行符\n
		\w 代表任意一个字母、数字、下划线
		\W 除了字母、数字、下划线以外的任意一个字符
		\d 代表任意一个数字
		\D 代表任意一个非数字
		\s 匹配空白字符、空格、tab
		\S 除了空白字符、空格、tab
		\. 转义字符，代表一个普通字符 .（点）
		[abc] 它里面的任意一个字符
		[^abc] 它里面除了abc的任意一个
		() 单元  
		
	- 2.元子符
		*（星号） 0个、一个、多个 修饰前面任意多个原子
		+（加号） 一个、多个,至少有一个 
		？（问号） 0个、多个，可有可无的，一般和单元结合使用
		| 或
		^ 以什么开头的
		$ 以什么结尾的
		\b 词边缘
		\B 非词边缘
		{2} 2个原子
		{2,} 2个前面的原子
		{2,5} 2-5个前面的原子 
	- 3.模式修正符
		/正则表达式/U
		i, m, s, U, e
		i 忽略大小写
		m 视为多行
		s 视为一行
		U 贪婪模式，最大化细致匹配模式
		e 替换的时候用的，可以用函数加工向后
	- 4.例子
	$str = "LINUX and php are lamp or linux is good";
	$ptn = '/linux/i';//匹配出字符串中的linux
	preg_match_all($ptn, $str, $arr);
	echo "<pre>";
	print_r($arr);//可以匹配出LINUX和linux
	echo "</pre>";
	
	eval让字符串表达式可以执行

- 27.正则表达式函数
```
	字符串的匹配与查找
	1.preg_match();正则表达式匹配
	2.preg_match_all();正则表达式全匹配
	3.preg_grep();可以做搜索
	字符串替换
	4.preg_replace();正则表达式替换
	字符串分割
	5.preg_split();	正则表达式分割
```
- 28.数学函数
```
	注：  参数有两种，1.多个数字，2.多个数字组成的数组
	1.max(10,5,8);取最大值
	2.min();取最小值
	3.mt_rand();随机去一个值，参数是一个范围
	4.ceil();距该数的上一个整数，不是四舍五入
	5.floor();距该数的下一个整数
	6.pi();取圆周率函数
	7.round();四舍五入函数
```
- 29.日期函数
  - 1.time();当前的时间戳，也即离1970年1月1日0点0分0秒C语言和unix诞生的起源时间，32位机器在2038年会出现时间溢出
  - 2.date("Y-m-d H:i:s");	
参数有Y（2016年）y（16）m（09月）n(9月，不带零) d（05日）j（5日，不带零）
H:24小时制的 h:12小时制的 i:00-59分 s:00-59秒
w（0-6 周日-周六）t（31）一个月多少天 L是否为闰年
A:AM或PM a:am或pm
年月日，时分秒,小y代表年的后两位，有时区规则
  - 3.strtotime();将字符串日期转成时间戳，可以是未来的时间也可以是过去的时间，如2016/9/11或者2015-3-4
  - 4.microtime(); 1秒=1000毫秒=1000000微秒
要想将两个时间进行数学运算，需要传给microtime里面的参数为1
  - 5.sleep(1)耽搁一秒钟

与时区有关的函数
```
1.date_default_timezone_set("时区名字缩写");中国的缩写是PRC
2.date_default_timezone_get();默认的时区，这个和PHP的配置文件有关，
在php.ini里面可以修改默认的时区，这样就不用设置默认的时间了
3.date_timezone_set();
4.date_timezone_get();
```

- 30.php的错误处理
  - 1.关闭和开启报错
display_errors = On
display_errors = Off（不建议使用）

   - 2.报错级别
E_ALL 所有以下错误
E_NOTICE 提示错误，脚本不终止
E_WARNING 警告错误，脚本不终止
E_ERROR 严重错误，脚本终止
E_PARSE 语法错误，脚本终止
  - 3.控制报错级别
error_reporting = E_ALL
E_ALL & ~E_NOTICE  报所有错误，但是除了提示错误
	
  - 4.报错地方
display_errors = Off//是否从浏览器输出错误
log_errors = On//是否把错误输出到一个自定义的日志文件中
修改存放文件的路径
error_log = e:\phplogs\php.log

- 31.GD库画图步骤
	1.准备画布
	2.准备涂料
	3.画画
	4.输出图片
	5.保存图片
	6.关闭画布

- 32.php图片处理函数
	- 1.适用场景
	验证码，缩放，裁剪，水印
	- 2.图片格式
	jpeg（jpg）是一种普及率最高的图片类型，它使用的是有损压缩格式
	png是网络上最常用的图片类型，它使用的时候无损压缩格式
	gif是网站上最常用的图片类型，它可以支持动态图片，它使用无损压缩格式
	- 3.创建图像的五个步骤
	   - 1.准备画布资源（资源创建用完后要被释放）
	$im = imagecreatetruecolor();参数是画布的大小
	  - 2.准备涂料
	$white = imagecolorallocate($im, 255, 255, 255);
	$black = imagecolorallocate($im, 0, 0, 0);参数是画布和颜色（十进制和十六进制）
	  - 3.在画布上画图像或文字（画布默认填充就是黑色）
	imagesetpixel();设置像素点，再加上for循环做验证码的干扰素
	iamgeline();画一条线，参数是坐标起点的x，y，终点的x，y
	imagerectangle();画一个矩形，参数是坐标起点的x，y，终点的x，y
	imagefilledrectangle();画一个矩形并填充
	imagepolygon();画一个多边形，参数有数组，个数，颜色
	imagefilledpolygon();画一个多边形并填充
	imagearc();画一个圆弧
	imagestring();水平画一行字符串
	imagestringup();垂直地画一列字符串
	imagechar();水平画一个字符
	imagecharup();垂直地画一个字符
	imagettftext();用truetype字符向图像画一个字符串
	imagefill($im, 0, 0, $white);填充画布，从什么位置以及颜色
	imageellipse($im, 0, 0, 100,100);画圆和椭圆,参数是画布，画图位置，椭圆的宽高
	- 4.输出或保存最终图像
	header("content-type:image/png");
	imagepng($im);以png格式输出，如果想保存图片，需加一个参数，里面写上图片的名称，注意格式
	- 5.释放画布资源
	imagedestroy($im);

- 33.php跳转设计
	1.页面跳转
	php方式的跳转（header前面不能有输出，不建议使用这种方式）
	header("location:index.php");
	js方式的跳转（优先使用此方式）
	echo "<script>location = '地址'</script>";
	echo "<script>location = 'index.php'</script>";

- 34.图片相关函数
	- 1.获取图片的宽高
	getimagesize();参数是文件名
	imagesx();得到图片的宽	
	imagesy();得到图片的高

	- 2.已经存在形成的画布资源
	imagecreatefromjpeg();打印出图片

	- 3.图片缩放函数（等比例问题）
	imagecopyresampled();

	- 4.图片裁剪函数
	imagecopyresampled();

	- 5.图片水印函数
	imagecopy();	

- 35.文件处理
	- 1.文件测试函数
	filetype();文件目录，参数是文件名，返回dir（文件夹）或者file（文件）
	is_dir();判断是否是目录，返回布尔值
	is_file();判断是否是文件，返回布尔值
	file_exists();文件或目录是否存在，返回布尔值
	filesize();文件大小或目录大小（4kb）
	- 2.文件操作函数
	fopen($filename,"w");参数是文件名和读或写方式，和C语言文件操作一样
	文件打开模式
	r 只读方式打开，将文件指针指向文件头
	r+ 读写方式打开，将文件指针指向文件头，擦除以后写
	w 写入方式打开，将文件指针指向文件头并将文件大小截为零，如果文件不存在则尝试先创建
	w+ 读写方式打开，将文件指针指向文件头并将文件大小截为零，且清空文件原有的内容，如果文件不存在则尝试先创建新文件
	a 写入文件打开，将文件指针指向文件结尾，如果文件不存在则尝试创建文件
	a+ 读写方式打开，将文件指针指向文件结尾，如果文件不存在则尝试创建文件
	fclose();关闭文件
	unlink();文件删除
	copy();文件复制改变，参数是文件和新的文件路径，并且可以修改文件名，剪切就是先复制后删除原文件
	rename();重命名文件名，参数是文件和新的文件名
	fread();文件读取，参数是文件资源和读取长度
	file();不需要资源，把文件以数组返回
	readfile();不需要资源
	file_get_contents();读取文件内容不需要资源，可以以后再打印
	写入文件内容
	file_put_contents();写入文件内容不需要资源
	移动文件指针
	ftell();告诉当前位置
	fseek();指针到指定位置
	rewind();指针返回文件开头
	feof();判断文件指针是否到了文件结束的位置
	- 3.目录操作
	mkdir();创建目录（文件夹）
	rmdir();删除目录，如果目录里面有内容则不能删除

	- 4.与路径有关的函数
	basename();取路径中最后的文件名
	dirname();取路径中除了最后文件名之外的路径
	realpath();里面的参数是.或者是..  .代表当前目录，..代表上一目录，每个文件夹都有两个隐藏的文件.和..，可以把相对地址转绝对地址
	__FILE__当前文件地址超全局变量
	DIRECTORY_SEPARATOR;目录分割符，windows是\,linux是/
	PATH_SEPARATOR;命令分割符,windows是;  linux是
	pathinfo();文件的路径信息，并将其存放在一个数组里面
	parse_url();浏览器地址的路径信息，并将其存放在一个数组里面
	parse_str();
	
	5.目录大小
	opendir(); readdir();打开读取文件夹
	closedir();删除目录资源，然后在删除目录本身rmdir();
	
- 36.文件上传
	- 1.客户端上传设置
	post方法
	enctype = 'multipart/form-data'
	max_file_size最大文件大小，在php.ini里面可以修改
	
	- 2.在服务器端通过php处理上传
	upload_max_filesize
	input type = file 上传框中文件的最大值
	post_max_size
	form表单的总大小，肯定要大小文件类型上传框的大小
	$_FILE['myfile']['error']
	0 表示没有发生任何错误
	1 表示上传文件的大小超过了约定值，约定值是upload的值,但不能超过post的值
	2 表示上传文件大小超过表单限制，是在表单<input type = "hidden" name = "MAX_FILE_SIZE" value = "10000">来设置
	3 表示文件只被部分上传
	4 表示没有上传任何文件

	- 3.多文件上传
	使用for循环

    - 4.文件下载（下载文件前要知道文件类型）
	1.header("content-type:image/png");
	2.header("content-disposition:attachment;filename=a.txt);
	3.header("content-length:30KB");
	4.readfile('a.txt');

- 37.mysql数据库
	- 数据表三部分组成
	1.表结构 列信息
	2.表字段（数据） 行信息
	3.表记录 （索引） 把列中的行加到索引中（一般情况下一个表一定要把id这一列的所有数据都加到主键索引中）
	
	- mysql基本命令
	 1.net stop mysql 关闭mysql
  	 2.net start mysql 开启mysql
	3.mysql -uroot -p123456
	4.show databases;
	5.use test切换test数据库
	6.show tables;查看所有表
	7.select * from t1;查看t1表中所有数据
	8.desc t1;查看t1表列结构
	9.exit 退出mysql客户端
	10.desc user 查看表记录
	
	- 数据库操作
	  - 1.创建数据库
	sql命令不区分大小写，每一个数据库会多一个文件夹，在window下数据库名不区分大小写，在linux下相反
	  - 2.查看数据库
	show databases；
	  - 3.删除数据库
	drop database t62;
	  - 4.切换数据库
	use test；

	- 表操作
	1.查看表
	show tables;
	2.创建表
	mysql> create table user(
	-> id int,
	-> name varchar(30),
	-> pass varchar(30)
	-> );
	3.修改表
	rename table t1 to user;
	4.删除表
	5.查看表字段
	desc user;
	6.查看表数据
	select * from t1;//来自哪的表格
	select id  from t1;
	7.查看表结构
	desc user;

- 38.数据库设计
	- 1.数值
	//显示和大小，这个显示是没有意义，除非你在左侧一定要补0，本列得有zerofill属性，而你这一列变成无符号
	int    //int（3）与长度无关，不够3位时前面补0，默认是看不见的
	float	
	- 2.字符串（显示和个数）
	//名字：char varchar 标题：varchar(30) 内容：text类型
	char(n)速度更快，n长度最大值255，占用n个字节
	varchar(n)节省空间，存多少字节占多少字节，储存l+1
	text 65535字节
	longtext longtext 42亿字节 
	- 3.日期和时间
	data
	time
	datatime
	year
	timestamp
	//用整形的时间戳来替代日期类型，在php中把time()时间戳存到mysql中的int列中，建议日期存整型
	- 4.数据字段属性
	unsigned 无符号，无负数
	zerofill 零填充，int(3)，不够3位补零
	auto_increment  自增，id使用
	null 默认属性，这一列允许为null
	not null  和下面的配合使用，改变属性
	default

- 39.查看服务器端的基本信息用\s查看
	四种字符集：
	服务器字符集
	Server characterset: utf8
	数据库字符集
	Db characterset：utf8
	客户端字符集
	Client characterset: utf8
	客户端的连接字符集
	Conn. characterset: utf8
	
	show create database  t1；查看数据库t1被创建时的相关信息
	
	查看数据库字符集
	show create database test;
	查看表字符集
	show create table user;
	
- 40.数据库的增删改查
//在mysql中没有==，只有=，即包含赋值，又包含比较
	增 insert
	insert into t1(usename) valuse('f');
	改 update
	update t1 set username='g' where id=6;
	一次性更改多个值，中间用逗号，隔开
	update t1 set username='gg',id=66 where id=6;
	删 delete
	//必须加where条件，如果不加where全部删除，这个时候应该用
	delete from t1 where id=3;
	delete from t1 where id>=3 and id<=5;
	delete from t1 where id between 3 and 5;
	delete from t1 where id=1 or id=3 or id=5;
	delete from t1 where id in(1,3,5);
