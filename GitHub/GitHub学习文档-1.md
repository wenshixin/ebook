![初识GitHub.png](http://upload-images.jianshu.io/upload_images/5763525-025a4d0e62bca3c3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 目录
- 1.[GitHub 是什么](#1-GitHub是什么) 
- 2.[GitHub 可以做什么](#2-GitHub可以做什么)
- 3.[注册 GitHub 账号](#3-注册GitHub账号)
	- 3.1[Step 1](#31-step1)
	- 3.2[Step 2](#32-step2)
	- 3.3[Step 3](#33-step3)
- 4.[GitHub 网站的介绍](#4-GitHub网站的介绍)
	- 4.1[用户信息的修改](#41-用户信息的修改)
	- 4.2[网站上专有名词的说明](#42-网站上专有名词的说明)

## 1 GitHub 是什么
---
[GitHub的百度百科](http://baike.baidu.com/link?url=WDFA0PF6QxWuccDtdEZGIbtDZx5KN45Ei-MFKc_qjkFDP58IpJUW5r8P7hbISWPF7Me4tzsIjsBTflj4u55KfK) 

首先 GitHub 是一个平台级的软件，在 PC端，网页端，手机端都有相应的应用，下面都是根据 GitHub 的网页端介绍的。GitHub 可以看做是一个面向开源和私有项目的托管平台，它具有版本控制和协作代码管理的功能。有了账号后，你就可以在平台上建立自己的仓库，GitHub 上主要是代码仓库，当然还有其他的应用。

## 2 GitHub 可以做什么
---
上面说到 GitHub 具有版本控制的功能，版本控制就是一个项目在逐渐的完善过程中，会产生不同的版本，比如手机QQ，更新之后，软件版本号肯定是不一样。如果每个版本都要保存一份，想想需要保存多少的版本，并且还要多处备份避免丢失。但是你的项目放到了 GitHub 上，那么项目版本的控制将会变得简单，每次你的项目有变更，你都可以在 GitHub 上添加一次记录，并还可以对比各个版本之间的差异，并且如果你想回退到之前的版本也很简单。

另外 GitHub 还有协作代码管理的功能，在团队协作开发项目的时代，如何更好的合作来完成项目就显得异常重要。GitHub 把一个项目分支，主分支是 master，一个团队的每个人就是一个小分支，大家先在自己的分支上工作，为主分支提供自己的贡献，团队的负责人再把每个人的小分支合并到主分支上，项目最终的发布以主分支为准。

GitHub 还是一个社区，大家可以在这里贡献自己的聪明才智，去帮助其他的人，比如说分享自己的代码、资料等等。另一方面你也可以免费得到他人开放分享的资源，但是要遵守项目中的协议约束。如果你发现别人的项目存在问题，你可以向项目的所有者提交并附带解决方法。作为项目的非直接参与者，你可以克隆别人的项目，并做出改进提交给项目所有者，这样你也算是这个项目的参与者了。社区中有一些人发布开源合作项目，如果你有能力或者建议可以与他们合作，真正参与到开发中，这可提升自己的能力和人脉。GitHub 是全球性的平台，有很多外国公司的开源项目，你可以在这里认识到外国朋友，甚至可以收到一份国外的工作邀请。

关于 GitHub 的使用，曾经看到过一个人用它来写小说，还有一个妹子把自己找男票的条件放在上面，但她好像并没有找到男票😃。GitHub 网站一直也在增加新的功能，本文写作之际，并没有对这些新功能做过研究，只是作为 GitHub 的入坑文章😏。

## 3 注册GitHub账号
---
GitHub 好处多多，你是否也想开启你的 GitHub 之旅呢？

- 登录 GitHub 官网 :[GitHub官网](https://github.com/)

![GitHub官网主页.png](http://upload-images.jianshu.io/upload_images/5763525-5f2fa7a749640b7f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- 创建 GitHub 账号 [加入GitHub](https://github.com/join?source=login) 

- ### 3.1 Step1
按照网站上的要求，填写姓名，注册邮箱，密码信息就可以注册账号啦！注意要进行验证，在用邮箱注册后 GitHub 会给你发一份邮箱验证邮件，需要点击验证链接 Verify email address 

![加入GitHub.png](http://upload-images.jianshu.io/upload_images/5763525-271a939c24a6e790.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- ### 3.2 Step2
注册成功后，进入欢迎界面，选择个人的计划，对于大部分人来说我们都是选择免费的开源仓库类型，之后如果涉及到机密的仓库也可以创建成付费的私人仓库的，所以选择第一个就好了。

![GitHub欢迎界面.png](http://upload-images.jianshu.io/upload_images/5763525-92b0234138e30e17.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


- ### 3.3 Step3
填写一些信息来简单的介绍自己，比如像图片上的，自己的编程经验如何，自己使用 GitHub 的计划，自己的身份，自己感兴趣的内容等等。当然你也可以直接跳过这一步，其实Step 1之后你就已经注册完成 GitHub 账号了。

![个人的简单介绍.png](http://upload-images.jianshu.io/upload_images/5763525-b92ad0044e083c58.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


最后是 GitHub 的初次使用向导，由于 GitHub 网站是英文的，所以下面会详细介（fan）绍（yi）的。

![学习GitHub.png](http://upload-images.jianshu.io/upload_images/5763525-f461fe23aba99dea.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 4 GitHub 网站的介绍
---
我们先打开 GitHub 的个人主页，这个是我刚注册的 GitHub 账号主页，地址：https://GitHub.com/wenshixin ，直接在 https://github.com/ 后面加上用户名即可，下面借助页面来逐一介绍页面上的用户个人信息修改以及页面上专有名词的含义。


![GitHub个人主页介绍1.png](http://upload-images.jianshu.io/upload_images/5763525-2518472198e43f5e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



![GitHub个人主页介绍2.png](http://upload-images.jianshu.io/upload_images/5763525-e0bb1d726dffa33d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

下面是我当前正在在使用的这个账号的 GitHub 主页展示，因为你刚有 GitHub 账号，所以页面上的信息没有我的这样丰富。自己已有一个粉丝还是外国朋友，也是有些小激动。

![当前账号的个人主页面信息.png](http://upload-images.jianshu.io/upload_images/5763525-04fc1cf8cbd68674.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


- ### 4.1 用户信息的修改
下面就来随我一起更改自己的个人信息吧！~~点击上面介绍的编辑信息按钮就可以进入编辑信息界面~~，直接点击图片就可以修改个人的信息。


![GitHub个人信息完善.png](http://upload-images.jianshu.io/upload_images/5763525-2a4e5b45cfa6218e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


- ### 4.2 网站上专有名词的说明

![网站专有名词的说明.png](http://upload-images.jianshu.io/upload_images/5763525-745da77208045060.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**Repository:**  
在 GitHub 上，Repository 是仓库的意思，仓库里就是你的项目，一切项目的开始都是先建仓库。

**Star:**  
Star 是给项目点赞的意思，其实也有收藏的含义，你 Star 过的项目会出现在你的个人主页的 Star 里，这样方便你之后对这个项目的查找。

**Fork**  
从Fork前面的图标以及词义上，我们就可以大致猜出他有分支的意思，如果你 Fork 了一个别人的项目，就相当于是在别人的项目上新建了一个分支，你可以随心所欲的改动这个项目，而不会对原有的项目代码结构产生影响，这个 Fork 操作在之后的 Pull requests 操作里还会提到。关于分支的概念，在《相守GitHub》中会详细说明的。

**Watch:**  
Watch 一个项目就是关注了一个项目，那么这个项目的最新动态都会及时的发送通知提醒给你。

**Gist:**  
如果你没有项目开源，只想分享一些代码片段，就可以用到 Gist 功能，但是这个好像直接打不开，需要翻墙才能使用。

**Code:**  
显示当前的项目中的目录结构以及文件内容等

**Issues:**  
项目中的问题或者 Bug，如果一个项目的 Issues 数量较多，不一定就说明这个项目错误很多，这其实恰恰说明这个项目受到很多人的重视，问题不可怕，可怕的是怕发现问题，怕去解决问题。如果你发现了问题，可以点击 Issues 选项，然后提出自己的问题，项目的问题解决了，问题的状态就是 Close 掉了，否则这个问题的状态就是 Open。如下图所示。

![Issues说明.png](http://upload-images.jianshu.io/upload_images/5763525-1ebbc58b5a6c719f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**Pull requests:**  
 GitHub 上的开源项目，任何人都是可以参与其中的，大家一起参与开发，一起来完善一个项目，这要用 Pull requests 来完成，需要先 Fork 一下别人的项目，然后自己可以把项目 clone 到本地进行修改完善，做好后再提交到自己的 Fork 的这个 GitHub 仓库中，最后就是在这个点击这个仓库的 Pull requests 选项，把自己的贡献提交个项目人，自己实际是建了一个分支，项目人查看你的修改，并决定是否接受你的 Pull requests。如果你的 Pull requests 被接受了，那么你也算是这个项目的贡献者之一了。

![Pullrequests说明.png](http://upload-images.jianshu.io/upload_images/5763525-5d1c98897547e393.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**Projects:**  
可以在本仓库中新增一个项目，这个功能基本没人用到，新增一个项目我会选择新建一个仓库的，所以这个功能了解一下就好了。

![Projects说明.png](http://upload-images.jianshu.io/upload_images/5763525-a8827d96a673d439.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**Wiki:**  
这个功能是关于项目说明文档的，一般每个项目 GitHub 都推荐建立一个 README.md 文档来做些项目说明，如果你的项目中有详细的项目文档，就可以使用Wiki功能来把项目说明文档做的更详细，Wiki通过建立词条来说明内容。

**Pulse:**  
pulse是这个项目活跃的汇总，包括该仓库的 Pull requests 数和 Issues 数，还有项目的参与程度，提交次数等等，这些都是反映一个项目的受关注度和参与度的指标。

![Pulse说明.png](http://upload-images.jianshu.io/upload_images/5763525-3bcecd0c806bb85f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**Graphs:**  
Graphs 从字面可以看出是图表，其实就是上面说的 Pulse 内容的另一种展示方式，

![Graphs说明.png](http://upload-images.jianshu.io/upload_images/5763525-daaad90428cf1a59.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**Settings:**  
如果这个项目是你自己的仓库的，那么你就拥有这个项目所有设置权，如果你是参看者，那么是没有这个 Settings 选项的。这个选项是对该项目信息的设置，比如项目的重命名，删除项目，关闭项目的 Wiki 和 Issues 功能等等，一般情况下我们采用项目的默认设置即可。

![Settings说明.png](http://upload-images.jianshu.io/upload_images/5763525-0e7f4c2d4ca3aba7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
