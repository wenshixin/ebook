---
style: candy
---
![牵手GitHub.jpg](http://upload-images.jianshu.io/upload_images/5763525-5e128f2e144c4f48.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 目录
- 1.[GitHub 和 Git 的前世今缘](#1-GitHub和git的前世今缘)
- 2.[Git 的下载安装](#2-git的下载安装)
	- 2.1[Git 下载](#21-git下载)
	- 2.2[Git 安装](#22-git安装)
- 3.[Git 的初始配置](#3-git的初始配置)
	- 3.1[查看安装的 Git 的版本](#31-查看安装的git的版本)
	- 3.2[查看 Git 的初始环境变量](#32-查看git的初始环境变量)
	- 3.3[配置 Git 的工作环境变量](#33-配置git的工作环境变量)
- 4.[Git 的工作流程](#4-git工作流程)
- 5.[Git 牵手 GitHub](#5-git牵手GitHub)

## 1 GitHub 和 Git 的前世今缘
---
[Git百度百科](http://baike.baidu.com/link?url=UAmMJcx4ZWc9X8Lz2x71e8twXzL--CFO21V716reS38M7HBohE15YeH_5yKZb-hHNad5KMcG34bRiUjCso7psq)

你可能会问学习 GitHub 和 Git 有什么关系吗？首先 GitHub 的功能本身就是基于 Git 来实现。现在的问题是 Git 是什么？Git 是一个开源的分布式版本控制系统，简单的来说就是一个强大的版本管理工具，GitHub 因为集成了 Git，所以具有版本管理的功能，在加上自身的社区功能，如订阅，讨论，关注，分享代码片段等。Git 是 Linux 之父—— Linus 两周周写成的（在此默默崇拜大牛5秒钟），最初是为了帮助管理 Linux 内核开发而设计的版本控制软件，这是一款开源的软件。（*在这里说一下，开源的意思就是开放源代码，用户可以在遵守项目协议的基础上使用和修改源代码。*）版本控制工具可不止 Git，还有 SVN 集中式的版本管理。但与 SVN 相比，Git 的优势是分布式，SVN 是把项目托管到一个中央服务器上，而 Git 则是分布在每个使用者的电脑上的，每个使用者都有项目的所有信息。使用 Git 可以在本地操作 GitHub 上的项目，增删改，操作完了，在推送到 GitHub 上保存。

## 2 Git 的下载安装
---
以 windows 环境下安装为例，Linux 和 Mac 系统请自行查找 Git 安装资料

- ### 2.1 Git 下载
[Git的官方下载地址](https://git-scm.com/downloads)

![下载Git1.png](http://upload-images.jianshu.io/upload_images/5763525-f28003872fc881ad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


![下载Git2.png](http://upload-images.jianshu.io/upload_images/5763525-74877c8e65d1c1f7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


如果你的下载速度较慢，也可以选择去软件应用中心下载，这里也推荐一个百度软件下载中心，但里面的软件不一定是最新的，还是建议在官网上下载的。[Git百度软件中心下载](http://rj.baidu.com/search/index/?kw=git)

- ### 2.2 Git 安装
在 windows 下安装 Git 和安装普通软件一样，你可以更改安装目录，其他的可以按照默认的安装选项，直接点下一步直到安装完成即可，安装完成后鼠标的右键功能表中会增加两个新的选项。  

![Git安装好后.png](http://upload-images.jianshu.io/upload_images/5763525-633fc41483c5543b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 3 Git 的初始配置
---
- ### 3.1 查看安装的 Git 的版本  
可以在运行窗口中输入 cmd 进入命令提示符窗口，然后输入 **```git --version```** 回车即可显示所安装的 git 版本，也可以进入鼠标右键的 Git Bash Here 选项进入 Git 命令行窗口，输入 **```git --version```** 命令来查看版本信息。
- ### 3.2 查看 Git 的初始环境变量  
在 Git Bash 窗口中输入 **```git config --global -l //分条查看初始配置信息```**

![Git初始配置信息.png](http://upload-images.jianshu.io/upload_images/5763525-a7063b6c3c5ed2c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


- ### 3.3 配置 Git 的工作环境变量 
下面这些环境变量的设置决定了 Git 在各个环节具体的工作方式和行为。比如说配置使用 Git 的用户信息，当然不配置也是可以的。
在 Git Bash 窗口中输入 **```git config --global user.name "wenshixin" //配置用户名```**  **```git config --global user.email "wenshixin23.@163.com" //配置邮箱名```**，再次查看Git的配置信息。

![Git配置后的环境信息.png](http://upload-images.jianshu.io/upload_images/5763525-bb23e4655a73ab07.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

另外还可以配置差异检测工具和 Git 默认的文本编辑器，一般 Git 默认的是 Vi 或者 Vim 编辑器，这个配置不常用，这里就不多演示，还有一个地方是 Git 命令行窗口输入的配置，一个是输入光标的设置，一个是在 Git 里中文显示不正常的设置，鼠标右键点击窗体标题栏。

![Git的其他配置.gif](http://upload-images.jianshu.io/upload_images/5763525-5c75b1f2aa173dff.gif?imageMogr2/auto-orient/strip)

**中文乱码配置问题**  
如下图所示，在 Git 里中文是用八进制编码来显示的。

![Git中文乱码.png](http://upload-images.jianshu.io/upload_images/5763525-a5f0f14d9cc15371.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

通过在 Git 命令行中输入 **```git config --global core.quotepath false```** ，来修改 Git 的默认文件编码，使中文文件名正常显示，下图为修改过的中文显示。

![修正Git中文编码问题.png](http://upload-images.jianshu.io/upload_images/5763525-195764fe8d7f831f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 4 Git 工作流程
---
![Git工作流程.png](http://upload-images.jianshu.io/upload_images/5763525-c98acfa44c6099b6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- ### 4.1 三个区域

工作区，暂存区，本地版本库。  

工作区（workspace）：就是你实际写项目的地方，比如可见的文件夹以及文件  

暂存区（stage/index）：工作区和本地版本库之间的区域，暂存区承担着工作区和版本库之间的“通信”任务，这样的设计为版本控制带来更大的灵活性，工作区的内容先放到暂存区，暂存区会生成索引，或者称为是目录树来记录工作区的修改。由工作区到暂存区只是把工作区的内容做了“跟踪监视”，并没有为其建立版本控制的内容。 

本地版本库（local repository）：暂存区的内容提交到本地的版本库中，版本库有一个 ./git 的隐藏文件夹来管理这个项目的每次提交，至此，你的项目才是真正的被“管理”起来了，而不只是之前的“监控”了。  
关于 Git 的各种命令，在《亲吻GitHub》中会详细说到。

- ### 4.2 四种状态 

未跟踪状态（untracked）：文件还只放在工作区，还没有放到暂存区进行跟踪和监视。  
未修改状态（unmodified）：没有进行编辑的空白文件  
修改状态（modified）：对空白文件进行了编辑  
暂存状态（staged）：文件到暂存区的后即为暂存状态

- ### 4.3 Git 的各个区之间的工作流程
这三个区域构成一个循环的工作流程，工作区（workspace）的内容添加（add）到暂存区（stage/index）,暂存区的内容再提交到本地版本控制仓库（local repository），工作区又可以切换（checkout）到版本的某一分支。

## 5 Git 牵手 GitHub

![Git结合GitHub.png](http://upload-images.jianshu.io/upload_images/5763525-6d4fa8c675e2a0c9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


之前说到可以在 GitHub 上创建仓库，这个仓库我们称为是远程仓库（remote repository），这和我们刚才说到的本地仓库又有什么关系呢？从上面的图中我们可以看到这个远程仓库有和本地仓库和工作区有联系。

我们通过创建本地仓库对我们的项目施行了管理控制，但这个仓库毕竟是我们硬盘上的文件，还是会有被意外删除和丢失的可能，所以放到远程仓库就很有必要，放在 GitHub 上安全一些，但是有时候在国内访问会有些慢。说明一点，远程仓库也不止 GitHub 一家，国内也有 **码云**，**CSDN**，但这些在名气上都比不上 GitHub，毕竟 GitHub 是全球性的，这些代码托管平台和 GitHub 在使用上其实相差不大，学会 GitHub，其他平台的使用都是小儿科，笔者也使用了码云这个托管平台，因为有时候 GitHub 真的挺慢。另外远程仓库也提高了我们项目的可携带性，只要在一个连上网的环境，我们就可以从远程仓库拷贝到本地计算机硬盘上，这样也便于分享自己的开源项目，团队合作会在《相守GitHub》中说到的。

知道了这些，我们再来说说 Git 和 GitHub 是如何“牵手”的。本地仓库中的内容可以推（push）到远程仓库，当然这里需要先而让本地仓库和远程仓库建立联系，具体的操作，在《亲吻GitHub》中会说。在本地你可以把远程仓库的内容直接拉（pull）到本地工作区，或者是获取（fetch）和克隆（clone）远程仓库中的内容到本地仓库中，这两种方式的区别，在《相守GitHub》中会说。
