今天阅读《编写高质量代码：改善Java程序的151个建议》，其中一个是自增的陷阱，才感受到自己对自增的理解还不够，看看下面这些代码的结果是什么。

##### java中
```java
public static void main(String[] args) {
		int count1 = 0;
		int count2 = 0;
		for(int i = 0; i < 10; i++)
		{
			count1 = count1++;
			count2++;
		}
		System.out.println("循环后count1="+count1);
		System.out.println("循环后count2="+count2);
	}
```
在myeclipse中执行结果如下

![java执行结果.png](http://upload-images.jianshu.io/upload_images/5763525-89b608f7975a26b7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
##### PHP中
```php
<?php 
	$count1 = 0;
	$count2 = 0;
	for($i = 0; $i < 10; $i++)
	{
		$count1 = $count1++;
		$count2++;
	}
	echo "循环后count1=".$count1."<br>";
	echo "循环后count2=".$count2;
?>
```
在浏览器中显示结果如下
![PHP执行结果.png](http://upload-images.jianshu.io/upload_images/5763525-78d1fd9e739cd43d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### C语言

```C
int _tmain(int argc, _TCHAR* argv[])
{
	int count1 = 0;
	int count2 = 0;
	for (int i = 0; i < 10; i++)
	{
		count1 = count1++;
		count2++;
	}
	printf("循环后count1=%d\n", count1);
	printf("循环后count2=%d", count2);
	getchar();
	return 0;
}
```
在VS2013中执行结果如下
![C语言执行结果.png](http://upload-images.jianshu.io/upload_images/5763525-1929850e07b6de73.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### C++

```C++
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int count1 = 0;
	int count2 = 0;
	for (int i = 0; i < 10; i++)
	{
		count1 = count1++;
		count2++;
	}
	cout << "循环后count1=" << count1 << endl;
	cout << "循环后count2=" << count2 << endl;
	system("pause");
	return 0;
}
```
在VS2013中执行结果如下
![C++执行结果.png](http://upload-images.jianshu.io/upload_images/5763525-c4aa1653190260d8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

你已经发现了在java和php中，count = count++;这句话不回使count的值加1，而在C/C++中却可以使count的值加1，这确实通过执行结果得到的直接的解释。

count++是一个表达式，不同语言对于i++自增的表达式的处理机制可能是不一样的，首先说下在Java中的处理方式的语言描述
```java
int temp = count; //先把count的值拷贝到临时变量区
count++; //count的值加1
count = temp; //将temp的值返回给count
```
所以，在java语言中，不管循环多少次，count的值始终为0，保持初态，在java中不要在单个的表达式中对相同的变量赋值超过一次。

而在C语言中
```C
count = count++; //等价于count++，因为C语言对它们的处理是一样的
```
规避这个自增的陷阱简单的方法就是把`count = count++`直接写成`count++`，而不要在前面在赋值给其本身。
有人会说单纯谈论语法是没有意义的，但是能用不同语言解释清楚count = count++这句话的人绝对不简单，这里涉及到不同语言的设计原理。在这里我不是自夸哈，我把这个拿出来和大家分享，因为之前我对这里的理解也有些简单，认为i++就是先赋值后运算，++i就是先运算后赋值，这大多是我受C语言和C++的影响，而在Java中却不是这样的。
