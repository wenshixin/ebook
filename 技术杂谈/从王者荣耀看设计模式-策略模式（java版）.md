![王者荣耀](http://upload-images.jianshu.io/upload_images/5763525-78fc7dae712a7b88.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

曾经我也是一名玩农药的大学生，但现在已经弃了，别问我为什么（被坑爹队友坑的哭。。。）。而今天我们来把让农药变荣耀，我们来从王者荣耀中看设计模式的策略模式。

![射手](http://upload-images.jianshu.io/upload_images/5763525-5a118c265cf0a790.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

#### 00.分析
我们先来思考，王者荣耀现在已经发展到60多个英雄了，他们大致分为坦克，战士，刺客，射手，法师，辅助六种角色。因为我喜欢玩射手，下面就拿射手来说明吧。射手相同的特征是射击，但每个射手都有不同的射击状态，可以佩戴不同的召唤师技能等等。

我们找一找射手这类英雄的相同点，相同点都是射击攻击，不同点是射击的方式是不同的，后羿拿弓箭射，鲁班是机枪扫射，狄仁杰是拿令牌扔，还有每个召唤师佩戴的技能也是可以不一样的。

找出这些不同点，如何使用面向对象来解决呢？面向对象的三大特性：封装，继承，多态。很容易想到的是，我们可以设计一个射手的抽象类，里面是一个射手应有的各种方法，比如说普通攻击，召唤师技能，技能攻击等等，然后在其子中类继承这种方法，然后使用多态就可以用父类来调用子类方法了。这貌似是可行的，可是大家知道王者荣耀是在一直更新的，以后会不断有新英雄，新装备，新皮肤，新技能出现，它们每个新的出现，我们都要覆写一遍父类中的方法，只用继承来做，不具有灵活性，对王者荣耀未来的更新支持很差。

我们再把思考的范围缩小一点，就拿召唤师技能来说吧。每个英雄是可以佩戴相同的召唤师技能的，但召唤师技能也是有多种的，这里你不能使用继承来实现英雄有召唤师技能了吧。因为英雄和召唤师技能之间是Has-a，而不是Is-a的关系，机智的你想到java中的接口interface，没错，我们可以定义各种召唤师的接口来使英雄获得不同的召唤师技能，但是我们再想一想，我们是可以在对战之前选择召唤师技能的，而不是每个英雄固定的，只用接口的话，召唤师的技能在英雄对象生成的时候就固定了，或者说，每个英雄是有具有召唤师技能的能力，但是我们并不能确定这个技能具体是什么。你可能还会想，我们可以加一些判断来给英雄不同的召唤师技能，但如果这样，我们每个英雄又会有多种不同的英雄（每个召唤师技能的接口不同），但想一想这种方式肯定是不可取的，工作量太TM大了，聪明的程序员肯定不会这样做。

![召唤师技能](http://upload-images.jianshu.io/upload_images/5763525-d82b98e840b1326a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

其实我们是想有一种可以动态加载召唤师技能接口的方法，我们整理一下思路， 召唤师的技能是在改变的，但每个英雄都可以佩戴召唤师技能，从而拥有召唤师技能的行为，我们可以把召唤师技能抽象出来作为射手抽象类的一个数据成员，然后每个召唤师技能再具体实现这个接口，这样英雄就很容易具有各种召唤师技能的行为了，如果王者荣耀更新新的召唤师技能，我们仍可以将这个技能实现自这个召唤师接口，我们称这个接口为策略接口，也就是设计模式中的策略模式的体现，这个接口不会变，但接口具体的实现类（各种召唤师技能）是可以变的，这就是我们通常所说的以不变应万变的宗则。这样王者荣耀这款游戏的寿命就延长了，王者荣耀的可（keng）玩（ren）性也大大增加了，你爸爸再也不用担心你的学习了（⊙﹏⊙）。

#### 01.代码实现
下面是我用Java实现的王者荣耀的策略模式的代码（环境：eclipse）

- 目录结构
英语不好，英雄名和召唤师技能都是用的拼音。。。

![image.png](http://upload-images.jianshu.io/upload_images/5763525-42bb29955cb81f15.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

下面是UML类图

![UML图](http://upload-images.jianshu.io/upload_images/5763525-b88777d7e5b90dd7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 英雄包
抽象类射手类，子类只写了后羿，鲁班七号，狄仁杰，你还可以加其他射手
1. 射手类代码
```
package character;

import strategy.SkillStrategy;

/**
 * 射手类（抽象类）
 * @author 一只写程序的猿
 *
 */
public abstract class Shooter {
	
	//英雄名字
	private  String heroName;
	public void setHeroName(String heroName) {
		this.heroName = heroName;
	}
	public String getHeroName() {
		return heroName;
	}

	//显示射手函数，由子类覆写实现具体的展示
	public abstract void display();
	
	//普通攻击函数，每个射手都可以射击
	public abstract void normalAttack();
	
	//技能策略成员
	private SkillStrategy skillStrategy;
	public void setSkillStrategy(SkillStrategy skillStrategy) {
		this.skillStrategy = skillStrategy;
	}

	//召唤师技能函数
	public void skill()
	{
		skillStrategy.useSkill();
	}
}
```
2.后羿子类代码
```
package character;

/**
 * 英雄：后羿（射手的子类）
 * @author 一只写程序的猿
 *
 */

public class HouYi extends Shooter {
	
	public HouYi() {
		super.setHeroName("后羿");
	}
	
	@Override
	public void display() {
		System.out.println("觉醒吧，猎杀时刻！");
	}

	@Override
	public void normalAttack() {
		System.out.println("xiuxiuxiu~，被动：被动迟缓之箭");
	}

}
```
3.鲁班七号子类代码
```
package character;

/**
 * 英雄：鲁班七号（射手的子类）
 * @author 一只写程序的猿
 *
 */

public class LuBanQiHao extends Shooter{
	
	public LuBanQiHao() {
		super.setHeroName("鲁班七号");
	}
	
	@Override
	public void display() {
		System.out.println("鲁班七号，智商二百五，鲁班，鲁班，顶级鲁班!");
	}

	@Override
	public void normalAttack() {
		System.out.println("dadada~，被动：火力压制");
	}
	
}
```
4.狄仁杰子类代码
```
package character;

/**
 * 英雄：狄仁杰（射手的子类）
 * @author 一只写程序的猿
 *
 */

public class DiRenJie extends Shooter {

	public DiRenJie() {
		super.setHeroName("狄仁杰");
	}
	
	@Override
	public void display() {
		System.out.println("代表法律制裁你！");
	}

	@Override
	public void normalAttack() {
		System.out.println("shuashuashua~，被动：迅捷");
	}

}

```
- 策略接口

1.召唤师技能策略接口
```
package strategy;
/**
 * 策略接口，使用射手的技能
 * @author 一只写程序的猿
 *
 */
public interface SkillStrategy {
	void useSkill();
}
```
- 召唤师技能包

1.惩戒召唤师技能
```
package skill;

import strategy.SkillStrategy;
/**
 * 技能策略-惩戒技能
 * @author 一只写程序的猿
 *
 */

public class ChengJie implements SkillStrategy{

	@Override
	public void useSkill() {
		System.out.println("惩戒：30秒CD，对身边的野怪和小兵造成真实伤害并眩晕1秒");
	}
}
```
2.疾跑召唤师技能
```
package skill;

import strategy.SkillStrategy;
/**
 * 技能策略-疾跑技能
 * @author 一只写程序的猿
 *
 */

public class JiPao implements SkillStrategy {

	@Override
	public void useSkill() {
		System.out.println("疾跑：60秒CD，增加攻击速度60%，并增加物理攻击力10%持续5秒");
	}

}
```
3.狂暴召唤师技能
```
package skill;

import strategy.SkillStrategy;
/**
 * 技能策略-狂暴技能
 * @author 一只写程序的猿
 *
 */

public class KuangBao implements SkillStrategy {

	@Override
	public void useSkill() {
		System.out.println("狂暴：60秒CD，增加攻击速度60%，并增加物理攻击力10%持续5秒");
	}

}
```

- 射手测试类
可以想（yi）象（yin）成王者荣耀的客户端，就是你玩的手机
```
package character;

import skill.ChengJie;
import skill.JiPao;
import skill.KuangBao;
import skill.ZhanSha;

public class ShooterTest {
	public static void main(String[] args) {
		System.out.println("（音乐响起。。。）欢迎来到王者农药，敌军还有30秒到达战场，请做好准备，全军出击！");
		Shooter shooter = null;
		//使用父类对象指向不同的子类对象，来切换英雄
		shooter = new HouYi();
		//shooter = new LuBanQiHao();
		//shooter = new DiRenJie();
		System.out.print("英雄"+shooter.getHeroName()+"登场，开场白：");
		shooter.display();
		System.out.print("普通攻击：");
		shooter.normalAttack();
		//动态加载召唤师技能
		shooter.setSkillStrategy(new JiPao());
		//shooter.setSkillStrategy(new KuangBao());
		//shooter.setSkillStrategy(new ZhanSha());
		//shooter.setSkillStrategy(new ChengJie());
		System.out.print("召唤师技能：");
		shooter.skill();
	}
}
```
执行结果截图

![后羿](http://upload-images.jianshu.io/upload_images/5763525-2e2d4558024b9735.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![狄仁杰](http://upload-images.jianshu.io/upload_images/5763525-457990bc555b82f0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![鲁班七号](http://upload-images.jianshu.io/upload_images/5763525-1d40339aff7c24e3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

#### 02.总结
关于策略模式，官方给出的定义如下。
>策略模式将可变的部分从程序中抽象出来分离成算法接口，在该接口下分别封装一系列算法实现。

这其中的设计原则有：
1.找出应用中需要变化的部分，把他们独立出来做成算法接口。（我们是把每个英雄的召唤师技能做成了算法接口，局内道具也是可以这样做的）
2.面向接口编程，而不是面向实现。（我们在使用召唤师策略接口时，并不知道它以后还会有什么样的召唤师技能）
3.多用组合，少用继承。（一个完整的英雄，是用具体的接口对象组合而成的，或者说各种接口小对象组合成了一个英雄对象）

策略模式的优点
- 使用了组合，而不单单是继承，使得架构更灵活。
- 富有弹性，可以较好的应对未来的变化。（开-闭原则）
- 更好的代码复用性。（相对于继承）
- 消除了大量的条件语句。

策略模式的缺点
- 客户端代码需要了解每个策略算法有那些（玩家需要知道有什么样的召唤师技能，服务器端的代码更新后，客户端也要增加相应的更新）
- 增加了对象的数目（每个接口都做成了一个对象，对象数目是不是增多了）

当然上面所有分析，都是个人的思考，不代表王者荣耀游戏就是这样做的哈。
