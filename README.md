# ebook
学习之路上的笔记

![觉得这只猫很可爱](http://upload-images.jianshu.io/upload_images/5763525-ad99496677a2a564.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 写作初衷
---

最开始学习时，只学习，而很少做笔记，随着学习的东西越来越多，基本就是学着忘着的状态，渐渐知道学习做笔记的重要性，就开始写写总结，但并没有形成文章。

## 写作开始
---

正真开始写作之旅是在2017年暑假，得知一个写作的网站——[简书](http://www.jianshu.com/)，从此便开始走上了写作之路，代码不可能写一辈子的，想着到了一定的年龄开始写书。

## 一波“广告”
---

写作的好处就不多说了，码云上的更新可能没有我的简书上更新的快，你可以关注我的简书主页——[一只写程序的猿](http://www.jianshu.com/u/76a6e8741afd)，欢迎你对我的文章提出建议，我将表示衷心感谢😘！

## 笔记目录
---

有（👌）的表示已完成，打叉（❌）的表示以后会加上的。

### GitHub
GitHub 的学习文章，之前写给她的，感叹字在人不在。

- [GitHub学习文档-1](https://gitee.com/wenshixin/ebook/blob/master/GitHub/GitHub%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3-1.md) 👌
- [GitHub学习文档-2](https://gitee.com/wenshixin/ebook/blob/master/GitHub/Github%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3-2.md) 👌
- [GitHub学习文档-3](https://gitee.com/wenshixin/ebook/blob/master/GitHub/Github%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3-3.md) 👌
- GitHub学习文档-4 ❌

### Java
Java 中 SSH 框架的学习笔记(下面的排列的顺序即是写作时的先后顺序，后面的文章会默认以前面的文章为基础)，需要有一定的 JavaEE 基础，里面贴了很多代码，都是自己亲手写的。

- [SSH框架之旅-Hibernate（1）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-hibernate%EF%BC%881%EF%BC%89.md) 👌
- [SSH框架之旅-Hibernate（2）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-hibernate%EF%BC%882%EF%BC%89.md) 👌
- [SSH框架之旅-Hibernate（3）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-hibernate%EF%BC%883%EF%BC%89.md) 👌
- [SSH框架之旅-Hibernate（4）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-hibernate%EF%BC%884%EF%BC%89.md) 👌
- [SSH框架之旅-Struts2（1）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-struts2%EF%BC%881%EF%BC%89.md) 👌
- [SSH框架之旅-Struts2（2）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-struts2%EF%BC%882%EF%BC%89.md) 👌
- [SSH框架之旅-Struts2（3）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-struts2%EF%BC%883%EF%BC%89.md) 👌
- [SSH框架之旅-Struts2（4）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-struts2%EF%BC%884%EF%BC%89.md) 👌
- [SSH框架之旅-Spring（1）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-spring%EF%BC%881%EF%BC%89.md) 👌
- [SSH框架之旅-Spring（2）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-spring%EF%BC%882%EF%BC%89.md) 👌
- [SSH框架之旅-Spring（3）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-spring%EF%BC%883%EF%BC%89.md) 👌
- [SSH框架之旅-Spring（4）](https://gitee.com/wenshixin/ebook/blob/master/Java/SSH%E6%A1%86%E6%9E%B6%E4%B9%8B%E6%97%85-spring%EF%BC%884%EF%BC%89.md) 👌

### PHP
之前学习 PHP 时的零碎笔记整理了一下，PHP 有段时间没在搞了。

- [php 入门笔记](https://gitee.com/wenshixin/ebook/blob/master/PHP/%E6%83%B3%E5%AD%A6%E4%B9%A0php%E7%9A%84%EF%BC%8C%E4%B8%8D%E5%A6%82%E6%9D%A5%E8%BF%99%E9%87%8C%E7%9C%8B%E7%9C%8B.md) 👌
- [Win10下配置PHP独立环境](https://gitee.com/wenshixin/ebook/blob/master/PHP/win10-%E4%B8%8B%E9%85%8D%E7%BD%AE-PHP-%E7%8B%AC%E7%AB%8B%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%80%BB%E7%BB%93.md) 👌

### 读书笔记
书喻药也，善读则可治愚！一句话，多读书多看报，少吃零食，多睡觉。

- 站在数据结构上思考Java-集合类 ❌

### 技术杂谈
个人扯淡的文章，不喜勿喷。

- [i-=-i++真的很简单？C-C++，java，php探秘](https://gitee.com/wenshixin/ebook/blob/master/%E6%8A%80%E6%9C%AF%E6%9D%82%E8%B0%88/i-=-i++%E7%9C%9F%E7%9A%84%E5%BE%88%E7%AE%80%E5%8D%95%EF%BC%9FC-C++%EF%BC%8Cjava%EF%BC%8Cphp%E6%8E%A2%E7%A7%98.md) 👌
- [从王者荣耀看设计模式-策略模式（java版）](https://gitee.com/wenshixin/ebook/blob/master/%E6%8A%80%E6%9C%AF%E6%9D%82%E8%B0%88/%E4%BB%8E%E7%8E%8B%E8%80%85%E8%8D%A3%E8%80%80%E7%9C%8B%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F-%E7%AD%96%E7%95%A5%E6%A8%A1%E5%BC%8F%EF%BC%88java%E7%89%88%EF%BC%89.md) 👌
- [我谈Markdown](https://gitee.com/wenshixin/ebook/blob/master/%E6%8A%80%E6%9C%AF%E6%9D%82%E8%B0%88/%E6%88%91%E8%B0%88-Markdown.md) 👌

### 前端
不想当将军的士兵不是好士兵，不想当全栈的工程师不是好工程师。

- [Bootstrap学习文档（一）](https://gitee.com/wenshixin/ebook/blob/master/%E5%89%8D%E7%AB%AF/Bootstrap%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3%EF%BC%88%E4%B8%80%EF%BC%89.md) 👌
- [Bootstrap学习文档（二）](https://gitee.com/wenshixin/ebook/blob/master/%E5%89%8D%E7%AB%AF/Bootstrap%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3%EF%BC%88%E4%BA%8C%EF%BC%89.md) 👌
- [Bootstrap学习文档（三）](https://gitee.com/wenshixin/ebook/blob/master/%E5%89%8D%E7%AB%AF/Bootstrap%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3%EF%BC%88%E4%B8%89%EF%BC%89.md) 👌
- [Bootstrap学习文档（四）](https://gitee.com/wenshixin/ebook/blob/master/%E5%89%8D%E7%AB%AF/Bootstrap%E5%AD%A6%E4%B9%A0%E6%96%87%E6%A1%A3%EF%BC%88%E5%9B%9B%EF%BC%89.md) 👌

### 实验花园
自己随手写的零碎小片段，形不成一篇文章。

- [java-JUnit测试工具的使用和对增强版for循环的理解](https://gitee.com/wenshixin/ebook/blob/master/%E5%AE%9E%E9%AA%8C%E8%8A%B1%E5%9B%AD/java-JUnit%E6%B5%8B%E8%AF%95%E5%B7%A5%E5%85%B7%E7%9A%84%E4%BD%BF%E7%94%A8%E5%92%8C%E5%AF%B9%E5%A2%9E%E5%BC%BA%E7%89%88for%E5%BE%AA%E7%8E%AF%E7%9A%84%E7%90%86%E8%A7%A3.md) 👌
- [java-可变参数](https://gitee.com/wenshixin/ebook/blob/master/%E5%AE%9E%E9%AA%8C%E8%8A%B1%E5%9B%AD/java-%E5%8F%AF%E5%8F%98%E5%8F%82%E6%95%B0.md) 👌
- [Java-数组](https://gitee.com/wenshixin/ebook/blob/master/%E5%AE%9E%E9%AA%8C%E8%8A%B1%E5%9B%AD/Java-%E6%95%B0%E7%BB%84.md) 👌
- [Eclipse快捷键](https://gitee.com/wenshixin/ebook/blob/master/%E5%AE%9E%E9%AA%8C%E8%8A%B1%E5%9B%AD/Myeclipse%E5%BF%AB%E6%8D%B7%E9%94%AE.md) 👌

### 数据库
数据库相关的文章，比较基础。

- [MySQL入门指南（一）](https://gitee.com/wenshixin/ebook/blob/master/%E6%95%B0%E6%8D%AE%E5%BA%93/%E5%86%99%E7%BB%99%E6%96%B0%E6%89%8B%E7%9A%84Mysql%E5%85%A5%E9%97%A8%E6%8C%87%E5%8D%97%EF%BC%88%E4%B8%80%EF%BC%89.md) 👌
- [MySQL入门指南（二）](https://gitee.com/wenshixin/ebook/blob/master/%E6%95%B0%E6%8D%AE%E5%BA%93/%E5%86%99%E7%BB%99%E6%96%B0%E6%89%8B%E7%9A%84Mysql%E5%85%A5%E9%97%A8%E6%8C%87%E5%8D%97%EF%BC%88%E4%BA%8C%EF%BC%89.md) 👌