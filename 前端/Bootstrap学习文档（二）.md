![Bootstrap](http://upload-images.jianshu.io/upload_images/5763525-ce9461d49270d339.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 标签和样式
Bootstrap 中把一些标签的样式重置了，也即是为一些标签设置了带有 Bootstrap 风格的样式，如 h 标签，p 标签等等，这其中包含了 HTML5 标签。

示例代码如下：
你可以把 Bootstrap 的 css 的样式文件注释后刷新浏览器，看看两种情况下的标签的默认样式。
```html
<h1>爱秋的艳</h1>
<h2>写给女朋友系列</h2>
<h3>Bootstrap学习文档</h3>
<h4>一只写程序的猿</h4>
<h5>html</h5>
<h6>css</h6>
<p>http://www.jianshu.com</p>
<p><kbd>ctrl+i</kbd></p>
<pre><p>这里是一段文字，不会被解析</p><br /><div>kaivon</div></pre>
<code>Hello, word!</code>
<code><span></code>
```

如果你想用Bootstrap h 标签的六级标题样式，还可以直接在标签中加上 h1~h6 的类名，如： ```<span class="h1">标题样式</span>``` 。

## 表格
Bootstrap 也重置了表格这个标签，加入了表格常用的样式，比如隔行换色，加边框等，下面是 Bootstrap 的表格类名，它们可以组合使用。
- table 会修改一些简单的样式
- table-striped 隔行换色
- table-bordered 给表格添加边框
- table-hover 给每一行添加一个hover状态
- table-condensed 让表格更加紧凑
			
注意：将表格包在一个class为table-responsive的div里，当屏幕尺寸小于768的时候会出现滚动条

在表格中，Bootstrap 还封装了一些状态类，通过这些状态类可以为行或单元格设置颜色。
- active 鼠标悬停在行或单元格上时所设置的颜色
- success	标识成功或积极的动作
- info 标识普通的提示信息或动作
- warning	标识警告或需要用户注意
- danger 标识危险或潜在的带来负面影响的动作

示例代码如下：
```html
<div class="container">
  <div class="row">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-condensed">
      <thead>
        <tr>
          <th>星期一</th>
          <th>星期二</th>
          <th>星期三</th>
          <th>星期四</th>
          <th>星期五</th>
        </tr>
      </thead>
      <tbody>
        <tr class="active">
          <td>语文  </td>
          <td>数学</td>
          <td>英文</td>
          <td>生物</td>
          <td>化学</td>
        </tr>
        <tr class="success">
          <td>语文  </td>
          <td>数学</td>
          <td>英文</td>
          <td>生物</td>
          <td>化学</td>
        </tr>
        <tr class="warning">
          <td>语文  </td>
          <td>数学</td>
          <td>英文</td>
          <td>生物</td>
          <td>化学</td>
        </tr>
        <tr class="danger">
          <td>语文  </td>
          <td>数学</td>
          <td>英文</td>
          <td>生物</td>
          <td>化学</td>
        </tr>
        <tr class="info">
          <td>语文  </td>
          <td>数学</td>
          <td>英文</td>
          <td>生物</td>
          <td>化学</td>
        </tr>
        <tr>
          <td class="active">语文 </td>
          <td class="success">数学</td>
          <td class="warning">英文</td>
          <td class="danger">生物</td>
          <td class="info">化学</td>
        </tr>
      </tbody>
    </table>
    </div>
  </div>
</div>
```

## 表单

1.基本样式
- form-control 让表单的宽度为100%，并且还添加了一些其它的样式 ，如表单圆角效果和聚焦发光
- form-group 给表单纵向之间添加了一些距离
- form-inline 让表单在一行中显示，此时可以不要form-group，但还是建议加上
示例代码如下：
```html
<div class="container">
  <div class="row">
    <form action="#" class="form-inline">
      <!--<div class="form-group">-->
        <label for="userName">用户名</label>
        <input type="text" id="userName" class="form-control" />
      <!--</div>-->
      
      <!--<div class="form-group">-->
        <label for="password">密码</label>
        <input type="password" id="password" class="form-control" />
      <!--</div>-->
    </form>
  </div>
</div>
```
2.样式进阶
- form-horizontal 让表单在一行中显示，并且能够改变form-group的样式
- control-label 让label里的文字水平垂直居中对齐


示例代码如下：
```html
<div class="container">
  <div class="row">
    <form action="#" class="form-horizontal">
      <div class="form-group">
        <label for="userName" class="col-lg-1 control-label">用户名</label>
        <div class="col-lg-11">
          <input type="text" id="userName" class="form-control" />
        </div>
      </div>
      
      <div class="form-group">
        <label for="password" class="col-lg-1 control-label">密码</label>
        <div class="col-lg-11">
          <input type="password" id="password" class="form-control" />
        </div>
      </div>
      
      <div class="form-group">
        <div class="col-lg-1 col-lg-offset-1">
          <input type="button" value="登录" class="btn" />
        </div>
      </div>
    </form>
  </div>
</div>
```

3.特殊样式
单选框，复选框，文本域框的样式
- checkbox checkbox的样式设置
- checkbox-inline 让checkbox在一行中显示
- radio radio的样式设置
- radio-inline 让radio在一行中显示

表单的校验状态类
- has-warning 表单填写警告
- has-error 表单填写错误
- has-success 表单填写成功

示例代码如下：
```html
<div class="form-group">
  <div class="col-lg-1 col-lg-offset-1">
    <div class="checkbox disabled">
      <label><input type="checkbox" disabled /> 记住我</label>
    </div>
    
  </div>
</div>

<div class="form-group">
  <div class="col-lg-1 col-lg-offset-1">
    <div class="checkbox">
      <label><input type="checkbox" /> 忘记我</label>
    </div>
    
  </div>
</div>

<!--checkbox在一行中显示-->
<div class="form-group">
  <div class="col-lg-11 col-lg-offset-1">
    <div class="checkbox">
      <label class="checkbox-inline"><input type="checkbox" /> 忘记我</label>
      <label class="checkbox-inline"><input type="checkbox" /> 忘记我</label>
    </div>
    
  </div>
</div>


<!--radio在一行中显示-->
<div class="form-group">
  <div class="col-lg-11 col-lg-offset-1">
    <div class="radio">
      <label class="radio-inline"><input type="radio" /> 忘记我</label>
      <label class="radio-inline"><input type="radio" /> 忘记我</label>
    </div>
  </div>
</div>

<!--select下拉框-->
<div class="form-group">
  <div class="col-lg-11 col-lg-offset-1">
    <select multiple class="form-control">
      <option value="">1</option>
      <option value="">2</option>
      <option value="">3</option>
      <option value="">4</option>
      <option value="">5</option>
    </select>
  </div>
</div>

<!--文本输入框-->
<div class="form-group">
  <div class="col-lg-11 col-lg-offset-1">
    <textarea class="form-control"></textarea>
  </div>
</div>
```

## 按钮
Bootstrap 为我们提供了丰富的按钮样式
- 按钮的背景色
btn-default 默认的按钮样式
btn-link 链接样式的按钮
btn-primary 首选项颜色的按钮
btn-success 成功颜色的按钮
btn-info 一般信息颜色的按钮
btn-warning 警告颜色的按钮
btn-danger 危险颜色的按钮

- 按钮的尺寸
btn-lg btn-md btn-sm btn-xs

- btn-block
让按钮从内联块级元素变为块级元素，可以撑满整个父级元素，也即是把按钮的宽度置为100%；

- 按钮的状态
active（激活状态） disabled（禁用状态）

示例代码如下：
```html
<div class="container">
  <div class="row">
    <input type="button" value="按钮" class="btn" />
    <button type="button" class="btn btn-default">kaivon</button>
    <button type="button" class="btn btn-primary">kaivon</button>
    <button type="button" class="btn btn-success">kaivon</button>
    <button type="button" class="btn btn-info">kaivon</button>
    <button type="button" class="btn btn-warning">kaivon</button>
    <button type="button" class="btn btn-danger">kaivon</button>
    <button type="button" class="btn btn-link">kaivon</button>
  </div>
  
  <!--按钮的尺寸-->
  <div class="row" style="margin-top: 10px;">
    <button type="button" class="btn btn-default btn-lg">kaivon</button>
    <button type="button" class="btn btn-primary btn-md">kaivon</button>
    <button type="button" class="btn btn-success btn-sm">kaivon</button>
    <button type="button" class="btn btn-info btn-xs">kaivon</button>
  </div>
  
  <!--btn-block 让按钮的宽度变为100%，并且成了块级元素-->
  <div class="row" style="margin-top: 10px;">
    <button type="button" class="btn btn-primary btn-block">kaivon</button>
  </div>
  
  <!--激活状态-->
  <div class="row" style="margin-top: 10px;">
    <button type="button" class="btn btn-primary active">kaivon</button>
    <a href="#" class="btn btn-danger active">kaivon</a>
  </div>
  
  <!--禁用状态-->
  <div class="row" style="margin-top: 10px;">
    <button type="button" class="btn btn-primary active" disabled>kaivon</button>
    <button type="button" class="btn btn-info disabled">kaivon</button>
    <a href="#" class="btn btn-danger active disabled">kaivon</a>
  </div>
</div>
```

## 图片
直接在img标签里面放置这些类，但不要乱用哦
- img-responsive
响应式图片，图片的大小随着父级容器的改变而改变，最大为图片的真实尺寸
- 图片的形状
img-rounded 带圆角效果的图片
img-circle 圆形的图片
img-thumbnail 带边框的图片

示例代码如下：
```html
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      ![](logo.jpg)
    </div>
  </div>
  
  <!--图片的形状-->
  <div class="row" style="margin-top: 10px;">
    <div class="col-lg-3">
      ![](logo.jpg)
    </div>
    <div class="col-lg-3">
      ![](logo.jpg)
    </div>
    <div class="col-lg-3">
      ![](logo.jpg)
    </div>
  </div>
</div>
```

## 辅助类
1.字体的颜色
text-muted text-primary text-success text-info text-warning text-danger
可以发现和前面的按钮背景色的后面一部分有相同的地方，Bootstrap 的命名是很有规律的，对于不同的使用场景，使用不同的前缀。

示例代码如下：
```html
<div class="row">
  <p class="text-muted">爱秋的艳</p>
  <p class="text-primary">爱秋的艳</p>
  <p class="text-success">爱秋的艳</p>
  <p class="text-info">爱秋的艳</p>
  <p class="text-warning">爱秋的艳</p>
  <p class="text-danger">爱秋的艳</p>
</div>
```

2.文本的背景
bg-primary bg-success bg-info bg-warning bg-danger

示例代码如下：
```html
<div class="row">
  <p class="bg-primary">爱秋的艳</p>
  <p class="bg-success">爱秋的艳</p>
  <p class="bg-info">爱秋的艳</p>
  <p class="bg-warning">爱秋的艳</p>
  <p class="bg-danger">爱秋的艳</p>
</div>
```

3.三角符号和按钮
Bootstrap 提供了常用的三角符号和按钮图标，使用起来很方便。
在span标签里面加上caret的类名，就可以变成一个下三角的符号。在button里面加上close的类名，并在button中加上×的转义符号就可以出现一个关闭的按钮。

示例代码如下：
```html
<div class="row">
  <span class="caret"></span>
  <button class="close">×</button>
</div>
```

4.浮动
pull-left 左浮动
pull-right 右浮动
注意，直接将浮动的部分放在row中，是可以撑开容器的，因为row是带清楚浮动样式的，如果不放直接放在row中可以在浮动部分外面加上clearfix的类名清楚内部的浮动。

示例代码如下：
```html
<div class="row">
  <span class="caret"></span>
  <button class="close">×</button>
</div>
<div class="row" style="border: 1px solid #f00;">
  <div class="pull-left">程序员</div>
  <div class="pull-right">程序媛</div>
</div>
```

5. 隐藏与显示
hidden 不在占去文档流的位置
show 正常的显示
 invisible 不可见，但仍会占有位置

示例代码如下：
```html
<style>
.row div{
    height: 100px;
    background: green;
    border: 1px solid #000;
    color: #fff;
  }
</style>

<div class="row">
  <div class="col-lg-3 show">第1列</div>
  <div class="col-lg-3 hidden">第2列</div>
  <div class="col-lg-3">第3列</div>
  <div class="col-lg-2 invisible">第4列</div>
  <div class="col-lg-1">第5列</div>
</div>
```

6.居中显示
center-block 是一个块级元素居中，原理其实很简单，就是我们经常写的 `margin: 0 auto; ` 另外还加入了 `display:block;` 来使元素变为块级的。

示例代码如下：
```html
<div class="row" >
  <div style="width: 100px; height: 100px; border: 1px solid #f00;" class="center-block"></div>
</div>
```

## 响应式工具
针对不同的屏幕宽度，显示和隐藏相关的内容
- 满足条件则显示
visible-*-* 第一个*的内容是 lg md sm  xs，第一个 * 的内容是 block inline inline-block 。

- 满足条件则隐藏
hidden-* * 的内容为 lg md sm xs 。

示例代码如下：
```html
<div class="container">
  <div class="row">
    <div class="col-lg-4 visible-lg-block">第1行第1列</div>
    <div class="col-lg-4 visible-lg-inline">第1行第2列</div>
    <div class="col-lg-4 visible-lg-inline-block">第1行第3列</div>
  </div>
  
  <div class="row">
    <div class="col-lg-4 visible-md-block">第2行第1列</div>
    <div class="col-lg-4 visible-md-inline">第2行第2列</div>
    <div class="col-lg-4 visible-md-inline-block">第2行第3列</div>
  </div>
  
  <div class="row">
    <div class="col-lg-4 hidden-lg">第3行第1列</div>
  </div>
</div>
```

## 打印类
打印的功能（浏览器快捷键Ctrl + P 或者右键功能选项）我们平时很少使用，Bootstrap 为了更加全面，加入了打印的样式类。
- 打印的时候显示
visible-print-* * 的内容有 block  inline inline-block 
- 打印的时候隐藏（不显示）
hidden-print

示例代码如下：
```html
<div class="container">
  <div class="row">
    <div class="col-lg-4 visible-print-block">第1行第1列</div>
    <div class="col-lg-4 visible-print-inline">第1行第2列</div>
    <div class="col-lg-4 visible-print-inline-block">第1行第3列</div>
  </div>
  
  <div class="row">
    <div class="col-lg-4 hidden-print">第2行第1列</div>
  </div>
</div>
```


Bootstrap 系列：

[Bootstrap学习文档（一）](http://www.jianshu.com/p/9cc8176ad0ce)
Bootstrap学习文档（二）
[Bootstrap学习文档（三）](http://www.jianshu.com/p/8419ceb4be55)
[Bootstrap学习文档（四）](http://www.jianshu.com/p/3f90fd868b6a)
