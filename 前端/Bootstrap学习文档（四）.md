![Bootstrap](http://upload-images.jianshu.io/upload_images/5763525-ce9461d49270d339.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 模态框

注意：
1、弹出层必需放在body里
2、弹出层里面不能再嵌套别的层
3、弹出层出来以后，页面的滚动条会被覆盖

- modal 弹出层父级
- modal-dialog        弹出层
- modal-content     弹出层的内容区域
- modal-header        弹出层的头部区域
- modal-body        弹出层的主体区域
- modal-footer        弹出层的底部区域
- fade            让弹出层有一个运动的效果，加给弹出层父级

示例代码如下：

模态框又有大小，modal-lg 大模态框，modal-sm 小模态框，默认是中等模态框。

```html
<div class="container">
  <div class="row">
    <button class="btn btn-primary" data-toggle="modal" data-target=".myModal1">弹出一个小层</button>
    <button class="btn btn-primary" data-toggle="modal" data-target=".myModal2">弹出一个大层</button>
    <button class="btn btn-primary" data-toggle="modal" data-target=".myModal3">弹出一个小小的层</button>
  </div>
</div>

<div class="modal fade myModal1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h4>爱秋的艳</h4>
      </div>
      <div class="modal-body">
        <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
        <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>


<div class="modal myModal2">
  <div class="modal-dialog  modal-lg"><!--modal-lg是用来设置弹出层的尺寸，它必需给modal-dialog的层-->
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h4>爱秋的艳</h4>
      </div>
      <div class="modal-body">
        <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p><p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>

<div class="modal myModal3">
  <div class="modal-dialog  modal-sm"><!--modal-lg是用来设置弹出层的尺寸，它必需给modal-dialog的层-->
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h4>爱秋的艳</h4>
      </div>
      <div class="modal-body">
        <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p><p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>
```

## 滚动监听

步骤：
1、谁要滚动就要给它添加 data-spy="scroll"，并且需要添加一个 data-target，这个值要与导航的父级相关联
2、给导航的父级添加一个 id 或者 class，要与要滚动的元素的 data-target 的值一致，并且要添加一个 navbar-collapse 的 class
3、导航里面的每个 a 标签都需要添加上一个锚链接，这样的话，才能与滚动对上号
4、滚动区域里的内容的标题要添加上相应的 id，用于与导航的锚链接对应

示例代码如下：

下面的代码取自 Bootstrap 官网，另外需要给滚动监听加上样式，不然还是没有效果的，使用起来不太简单。

```html
<style>
      #subNav{
        position: fixed;
        top: 0;
      }
      
      .nav a{
        color: #666;
        
      }
      .nav ul{
        display: none;
      }
      .nav .active ul{
        display: block;
        padding-left: 20px;
      }
      .nav > li > a {
          padding: 5px 15px;
      }
      .nav .active > a{
        color: #f00;
        border-left: 1px solid #f00;
      }
      
      .nav .active .bold{
        font-weight: bold;
        border-left: 3px solid #f00;
        font-size: 20px;
      }
    </style>

<div class="container">
  <div class="row">
    <!-- 这里给个padding值是为了让下面的内容上去，可以试试去掉的效果 -->
    <div class="col-lg-9" style="padding-bottom: 500px;">
      <div class="section">
        <h2 id="gaishu">概览</h2>
        <h3 id="dange">单个还是全部引入</h3>
        <p>JavaScript 插件可以单个引入（使用 Bootstrap 提供的单个 <code>*.js</code> 文件），或者一次性全部引入（使用 <code>bootstrap.js</code> 或压缩版的 <code>bootstrap.min.js</code>）。</p>
        <p>建议使用压缩版的 JavaScript 文件bootstrap.js 和 bootstrap.min.js 都包含了所有插件，你在使用时，只需选择一个引入页面就可以了。</p>
        <p>插件之间的依赖关系某些插件和 CSS 组件依赖于其它插件。如果你是单个引入每个插件的，请确保在文档中检查插件之间的依赖关系。注意，所有插件都依赖 jQuery （也就是说，jQuery必须在所有插件之前引入页面）。 bower.json 文件中列出了 Bootstrap 所支持的 jQuery 版本。</p>
        <h3 id="datashuxing">data 属性</h3>
        <p>你可以仅仅通过 data 属性 API 就能使用所有的 Bootstrap 插件，无需写一行 JavaScript 代码。这是 Bootstrap 中的一等 API，也应该是你的首选方式。</p>
        <p>话又说回来，在某些情况下可能需要将此功能关闭。因此，我们还提供了关闭 data 属性 API 的方法，即解除以 data-api 为命名空间并绑定在文档上的事件。就像下面这样：</p>
        <p>
          <pre>$(document).off('.data-api')</pre>
        </p>
        <p>另外，如果是针对某个特定的插件，只需在 data-api 前面添加那个插件的名称作为命名空间，如下：</p>
        <p>
          <pre>$(document).off('.alert.data-api')</pre>
        </p>
        <h3 id="biancheng">编程方式的 API</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="bimian">避免命名空间冲突</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="shijian">事件</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="weidui">未对禁用 JavaScript 的浏览器提供补救措施</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
      </div>
      
      <div class="section">
        <h2 id="guodu">过渡效果</h2>
        <h3 id="guanyu">关于过渡效果</h3>
        <p>JavaScript 插件可以单个引入（使用 Bootstrap 提供的单个 <code>*.js</code> 文件），或者一次性全部引入（使用 <code>bootstrap.js</code> 或压缩版的 <code>bootstrap.min.js</code>）。</p>
        <p>建议使用压缩版的 JavaScript 文件bootstrap.js 和 bootstrap.min.js 都包含了所有插件，你在使用时，只需选择一个引入页面就可以了。</p>
        <p>插件之间的依赖关系某些插件和 CSS 组件依赖于其它插件。如果你是单个引入每个插件的，请确保在文档中检查插件之间的依赖关系。注意，所有插件都依赖 jQuery （也就是说，jQuery必须在所有插件之前引入页面）。 bower.json 文件中列出了 Bootstrap 所支持的 jQuery 版本。</p>
        <h3 id="baohan">包含的内容</h3>
        <p>你可以仅仅通过 data 属性 API 就能使用所有的 Bootstrap 插件，无需写一行 JavaScript 代码。这是 Bootstrap 中的一等 API，也应该是你的首选方式。</p>
        <p>话又说回来，在某些情况下可能需要将此功能关闭。因此，我们还提供了关闭 data 属性 API 的方法，即解除以 data-api 为命名空间并绑定在文档上的事件。就像下面这样：</p>
        <p>
          <pre>$(document).off('.data-api')</pre>
        </p>
        <p>另外，如果是针对某个特定的插件，只需在 data-api 前面添加那个插件的名称作为命名空间，如下：</p>
        <p>
          <pre>$(document).off('.alert.data-api')</pre>
        </p>
      </div>
      
      <div class="section">
        <h2 id="shili">实例</h2>
        <h3 id="jingtai">静态实例</h3>
        <p>JavaScript 插件可以单个引入（使用 Bootstrap 提供的单个 <code>*.js</code> 文件），或者一次性全部引入（使用 <code>bootstrap.js</code> 或压缩版的 <code>bootstrap.min.js</code>）。</p>
        <p>建议使用压缩版的 JavaScript 文件bootstrap.js 和 bootstrap.min.js 都包含了所有插件，你在使用时，只需选择一个引入页面就可以了。</p>
        <p>插件之间的依赖关系某些插件和 CSS 组件依赖于其它插件。如果你是单个引入每个插件的，请确保在文档中检查插件之间的依赖关系。注意，所有插件都依赖 jQuery （也就是说，jQuery必须在所有插件之前引入页面）。 bower.json 文件中列出了 Bootstrap 所支持的 jQuery 版本。</p>
        <h3 id="dongtai">动态实例</h3>
        <p>你可以仅仅通过 data 属性 API 就能使用所有的 Bootstrap 插件，无需写一行 JavaScript 代码。这是 Bootstrap 中的一等 API，也应该是你的首选方式。</p>
        <p>话又说回来，在某些情况下可能需要将此功能关闭。因此，我们还提供了关闭 data 属性 API 的方法，即解除以 data-api 为命名空间并绑定在文档上的事件。就像下面这样：</p>
        <p>
          <pre>$(document).off('.data-api')</pre>
        </p>
        <p>另外，如果是针对某个特定的插件，只需在 data-api 前面添加那个插件的名称作为命名空间，如下：</p>
        <p>
          <pre>$(document).off('.alert.data-api')</pre>
        </p>
        <h3 id="kexuan">可选尺寸</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="jinzhi">禁止动画效果</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="yongfa">用法</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="tongguo1">通过 data 属性</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="tongguo">通过 JavaScript 调用</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="canshu">参数</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="fangfa">方法</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
        <h3 id="shijian2">事件</h3>
        <p>我们为所有 Bootstrap 插件提供了纯 JavaScript 方式的 API。所有公开的 API 都是支持单独或链式调用方式，并且返回其所操作的元素集合（注：和jQuery的调用形式一致）。</p>
        <p>$('.btn.danger').button('toggle').addClass('fat')</p>
        <p>所有方法都可以接受一个可选的 option 对象作为参数，或者一个代表特定方法的字符串，或者什么也不提供（在这种情况下，插件将会以默认值初始化）：</p>
      </div>
    </div>
    <div class="col-lg-3">
      <div id="subNav" class="navbar-collapse">
        <ul class="nav">
          <li>
            <a href="#gaishu" class="bold">概览</a>
            <ul class="nav">
              <li><a href="#dange">单个还是全部引入</a></li>
              <li><a href="#datashuxing">data 属性</a></li>
              <li><a href="#biancheng">编程方式的 API</a></li>
              <li><a href="#bimian">避免命名空间冲突</a></li>
              <li><a href="#shijian">事件</a></li>
              <li><a href="#weidui">未对禁用 JavaScript 的浏览器提供补救措施</a></li>
            </ul>
          </li>
          <li>
            <a href="#guodu" class="bold">过渡效果</a>
            <ul class="nav">
              <li><a href="#guanyu">关于过渡效果</a></li>
              <li><a href="#baohan">包含的内容</a></li>
            </ul>
          </li>
          <li>
            <a href="#shili" class="bold">实例</a>
            <ul class="nav">
              <li><a href="#jingtai">静态实例</a></li>
              <li><a href="#dongtai">动态实例</a></li>
              <li><a href="#kexuan">可选尺寸</a></li>
              <li><a href="#jinzhi">禁止动画效果</a></li>
              <li><a href="#yongfa">用法</a></li>
              <li><a href="#tongguo1">通过 data 属性</a></li>
              <li><a href="#tongguo">通过 JavaScript 调用</a></li>
              <li><a href="#canshu">参数</a></li>
              <li><a href="#fangfa">方法</a></li>
              <li><a href="#shijian2">事件</a></li>
            </ul>
          </li>
        </ul>
      </div>  
    </div>
  </div>
</div>
```

## 提示标签

在 a 标签上，可以加上提示，需要加上 title 属性，里面放上提示语。但在 Bootstrap 中也有更多的效果，可以变显示方向，但需要js的代码。

- data-placment="top（bottom）（left）（right）" 决定标签弹出来的方向

示例代码如下：

```html
<style>
  p{
    margin: 200px 0;
  }
</style>

<div class="container">
  <div class="row">
    <p><a href="#" class="tool" data-toggle="tooltip" data-placement="top" title="我爱你">爱秋的艳</a>程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛<a href="#" class="tool" data-toggle="tooltip" data-placement="right" title="我爱你">爱秋的艳</a>程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛<a href="#" class="tool" data-toggle="tooltip" data-placement="bottom" title="我爱你">爱秋的艳</a>程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛<a  href="#" class="tool" data-toggle="tooltip" data-placement="left" title=我爱你">爱秋的艳</a>程序猿爱程序媛</p>
  </div>
  
  <div class="row">
    <button class="btn btn-primary tool" data-toggle="tooltip" title="code" data-placement="top">code</button><br /><br />
    <button class="btn btn-primary tool" data-toggle="tooltip" title="code" data-placement="right">code</button><br /><br />
    <button class="btn btn-primary tool" data-toggle="tooltip" title="code" data-placement="bottom">code</button><br /><br />
    <button class="btn btn-primary tool btnShow" data-toggle="tooltip" title="code" data-placement="left">code</button>
  </div>
</div>

<!--注意放在 Bootstrap.js 文件的下面-->
<script>
  $('.tool').tooltip();
  $('.btnShow').tooltip('show');  //让某一个标签一上来就显示
</script>
```

## 提示框

提示框可以比提示标签提示更多的内容，它弹出来的是个小的对话框，用法和提示标签差不多。

示例代码如下

```html
<style>
  .row{
    padding-top: 200px;
  }
</style>

<div class="container">
  <div class="row">
    <button class="btn btn-danger" data-toggle="popover" data-placement="top" title="这是弹框的标题" data-content="这是弹框的内容，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长">点击我给你弹出一个框</button><br /><br />
    <button class="btn btn-danger" data-toggle="popover" data-placement="right" title="这是弹框的标题" data-content="这是弹框的内容，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长">点击我给你弹出一个框</button><br /><br />
    <button class="btn btn-danger" data-toggle="popover" data-placement="bottom" title="这是弹框的标题" data-content="这是弹框的内容，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长">点击我给你弹出一个框</button><br /><br />
    <button class="btn btn-danger btnShow" data-toggle="popover" data-placement="left" title="这是弹框的标题" data-content="这是弹框的内容，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长，它很长很长">点击我给你弹出一个框</button>
  </div>
</div>

<!--注意放在 Bootstrap.js 文件的下面-->
<script>
      $('.btn').popover();
      $('.btnShow').popover('show');  //让某一个提示框一上来就显示
</script>
```


## 折叠菜单

### 1.单个折叠菜单

制作步骤：
1、给要点击的那个元素添加一个 data-toggle="collapse"，并且需要给它添加一个data-target，让它的值与对应的内容区域的id或者class相同
2、给对应的内容区域添加一个id或者class，与点击的元素相对应
			
注意：
- 1、内容区域不能有padding值

```html
<div class="container">
  <div class="row">
    <div class="col-lg-5">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a href="#" data-toggle="collapse" data-target="#con">程序媛</a>
          </h3>
        </div>
        <!--添上 in 的 class， 使得折叠菜单折叠后不在展开-->
        <div id="con" class="in">
          <div class="panel-body">
            程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛程序猿爱程序媛
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
```

### 2.一组折叠菜单

- 1.把所有的面板都放到一个父级里，并且给父级添加一个panel-group，还有一个id
- 2.给要点击的元素添加一个data-parent属性，并让他的值与父级的id一样-
- 3.需要给内容区域添加一个class为collapse
示例代码如下：

```html
<div class="container">
  <div class="row">
    <div class="col-lg-5">
      <!--1、把所有的面板都放到一个父级里，并且给父级添加一个panel-group，还有一个id-->
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <!--2、给要点击的元素添加一个data-parent属性，并让他的值与父级的id一样-->
            <h3 class="panel-title" data-parent="#accordion" data-toggle="collapse" data-target="#con1">爱秋的艳</h3>
          </div>
          <!--3、需要给内容区域添加一个class为collapse-->
          <div id="con1" class="collapse in">
            <div class="panel-body">
              程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿
            </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title" data-parent="#accordion" data-toggle="collapse" data-target="#con2">爱秋的艳</h3>
          </div>
          <div id="con2" class="collapse">
            <div class="panel-body">
              程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿 程序媛爱程序猿
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
```

## 轮播图

- 1、写一个父级，放一个 id，和一个 carousel 的 class
slide	 给图片添加运动的效果
data-interval="500" 间隔时间，单位为毫秒，1秒是等于1000毫秒的，不要低于400，否则容易出现问题
data-ride="carousel" 页面一加载后就开始播放
- 2、小圆点的内容要放在一个 class 为 carousel-indicators 的层里
- 3、图片内容区域要放在一个 class 为 carousel-inner 的层里，每一项内容都需要放到一个叫 item 的层里，这个里面也可以放文字，那需要来一个父级，父级的 class 为 carousel-caption
- 4、左右按钮按以下的格式写，href 里面的值要与父级的 id 一样
<a href="#pic" class="carousel-control left" data-slide="prev">‹</a>
<a href="#pic" class="carousel-control right" data-slide="next">›</a>

示例代码如下：

```html
<div class="container">
  <div id="pic" class="carousel slide" data-interval="2000" data-ride="carousel">
    <!--小圆点-->
    <ol class="carousel-indicators">
      <li class="active"></li>
      <li></li>
      <li></li>
      <li></li>
    </ol>
    <!--轮播图的图片-->
    <div class="carousel-inner">
      <div class="item active">
        ![](images/1.jpg)
        <div class="carousel-caption">
          <h3>这里是标题1</h3>
          <p>这里是内容，非常多非常多</p>
        </div>
      </div>
      <div class="item">
        ![](images/2.jpg)
        <div class="carousel-caption">
          <h3>这里是标题2</h3>
          <p>这里是内容，非常多非常多</p>
        </div>
      </div>
      <div class="item">
        ![](images/3.jpg)
        <div class="carousel-caption">
          <h3>这里是标题3</h3>
          <p>这里是内容，非常多非常多</p>
        </div>
      </div>
      <div class="item">
        ![](images/4.jpg)
        <div class="carousel-caption">
          <h3>这里是标题4</h3>
          <p>这里是内容，非常多非常多</p>
        </div>
      </div>
    </div>
    <!--左右按钮-->
    <a href="#pic" class="carousel-control left" data-slide="prev">‹</a>
    <a href="#pic" class="carousel-control right" data-slide="next">›</a>
  </div>
</div>
```

解决轮播图的高度问题：
1.使轮播图里面的图片自适应，添加 img-responsive 是没用的。

css代码如下：
```css
#carousel-img.carousel-inner .item{ width: 100%;height: 0;padding-bottom: 50%;overflow: hidden;}
#carousel-img .carousel-inner .item img{width: 100%;}
```

2.轮播图的图片大小一致，但想改变轮播图的高度，改变父级 div 是无法改变图片的高度或者宽度。

css代码如下：

高度值要根据轮播图的图片定，这里是示例。
```css
*解决轮播图的高度问题*/
.carousel, .carousel .item{
  height: 400px;
}
.carousel .item img{
  width: 100%;
}

@media only screen and (max-width:768px ) {
	.carousel, .carousel .item{
    height: 150px;
  }
  .carousel .item img{
    width: 100%;
  }
}
```


Bootstrap 系列：

[Bootstrap学习文档（一）](http://www.jianshu.com/p/9cc8176ad0ce)
[Bootstrap学习文档（二）](http://www.jianshu.com/p/8d69f5b97bf3)
[Bootstrap学习文档（三）](http://www.jianshu.com/p/8419ceb4be55)
Bootstrap学习文档（四）
