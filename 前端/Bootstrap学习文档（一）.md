![Boostrap中中文网](http://upload-images.jianshu.io/upload_images/5763525-edc235541b60a96a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 1. Bootstrap 是什么

Bootstrap 是最受欢迎的 HTML、CSS 和 JS 框架，用于开发响应式布局、移动设备优先的 WEB 项目，使用这个框架可以简单高效的开发出适配各种屏幕的网站应用，即是编写一套代码，适用多重平台（PC，平板，手机等）。Bootstrap 相比其他框架，自由度更高，它提供了基本的样式和基本的组件，而不会在创造上约束开发者的思维。

简单的用面向对象的来说，Bootstrap 为我而们封装了一些常用的类（class的名字）和接口（js的插件），这些类就是这个模版自定义的 css 样式，比如文本居中，css 代码是 `text-align: center;` 经过 Bootstrap封装后的类（class）名为 `text-center`，这样我们就可以直接使用text-center来使一个 p 标签或者 div 标签里面的内容居中了，从而实现我们软件工程中所说的代码重用。Bootstrap 中的js插件，不需要我们写 js 代码就能帮我们实现要用 js 来实现的效果，而是通过使用 Bootstrap 自定义的属性。

学习的时候直接到这里查 Bootstrap 的文档 [Bootstrap中文网](http://v3.bootcss.com)。

## 2. Bootstrap 常用的基本模版

相比官网的基本模版，增加了一些常用的设置。
```html
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <!-- 使IE浏览器用最新的edge引擎渲染页面 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 让页面的最大宽度等于设备的宽度，页面初始化为不缩放状态，当然这样还是可以缩放的 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <!-- 使360浏览器渲染页面时默认使用极速模式，考虑到国内360的大量用户 -->
    <meta name="renderer" content="webkit">
    <title>Bootstrap常用的模版</title>

    <!-- 项目导入的Bootstrap的css压缩文件 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- 一个浏览器默认css样式重置文件，不用在手动重置了 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

    <!-- 下面两个文件是让IE9以下的IE浏览器兼容新增的HTML5标签和CSS3样式 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>写给女朋友的Bootstrap学习文档</h1>

    <!-- 为Bootstrap中封装的JavaScript插件提供支持，并且要在Bootstrap.js之前引入 -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 项目导入的Bootstrap的js压缩文件 -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
```

## 3. 栅格系统

- #### Bootstrap 的布局容器
  - 1.container-fluid 自适应宽度100%
  - 2. container 适应屏幕的固定宽度，要比container占的宽度小一些
    - 屏幕宽度 >= 1200px   固定宽度为1270px
    - 992px <= 屏幕宽度 <= 1200px 固定宽度为970px
    - 768px <= 宽度 <= 992px 固定宽度为750px
    - 宽度< 768px 固定宽度auto（自适应）

注意：这两个class不能放在一起，可以是兄弟关系，但不能是嵌套的关系。如果不想让宽度随着屏幕而变化，可以给它一个!important来提升优先级，这样的话在所有尺寸下都是一个定值

示例代码如下：
```html
<div class="container-fluid">
  <div style="background: red;">自适应宽度100%</div>
</div>
		
<div class="container" style="background: green; width: 1000px!important;">适应屏幕的固定宽度</div>

下面是错误写法
<div class="container-fluid">
  <div class="container" style="background: red;">错误写法</div>
</div>
```

- #### Bootstrap 的栅格系统
在 Bootstrap 中一行分为 12列，也即是屏幕的宽度被分为了 12份，一份就是十二分之一的屏幕宽度，源码是通过宽度为百分比以及浮动实现的。
  **1.** row 代表一行
  **2.** col-*-* 代表列，第一个 * 和屏幕尺寸有关，第二个 * 是列数
    如果列的和超过了12，那就会换行，如果有一列，这个数值超过了12，那就会按12去显示
    lg 宽度>1200px
      表现形式：
        屏幕的宽度大于1200，一行显示n(结构里有几个div)列
	屏幕的宽度小于1200，一行显示1列

     md 992px <= 宽度 <= 1200px
      表现形式：
        屏幕的宽度大于992并且小于1200，一行显示n(结构里有几个div)列
	屏幕的宽度小于992，一行显示1列

     sm 768px <= 宽度 <= 992px
      表现形式：
        屏幕的宽度大于768并且小于992，一行显示n(结构里有几个div)列
	屏幕的宽度小于768，一行显示1列

      xs 宽度 <= 768px
      表现形式：
        屏幕的宽度小于768，一行永远显示n(结构里有几个div)列

记忆口诀：LG（手机品牌）（lg），妈的（md），什么（sm），想死（xs）。（不是自己独创的，参考的网上的笑话。）

示例代码如下：
缩小浏览器宽度改变div的宽度，观察列数的变化。
```html
<style>
  .row div{
    background: green;
    color: #fff;
    height: 100px;
    border: 1px solid #000;
  }
</style>

<div class="container">
  <div class="row">
      <div class="col-lg-13">超过12列啦啦啦</div>
  </div>
  <div class="row">
    <div class="col-lg-1">第1行第1列</div>
    <div class="col-lg-11">第1行第2列</div>
  </div>
  <div class="row">
    <div class="col-md-4">第2行第1列</div>
    <div class="col-md-4">第2行第2列</div>
    <div class="col-md-4">第2行第3列</div>
  </div>
  <div class="row">
    <div class="col-sm-4">第3行第1列</div>
    <div class="col-sm-4">第3行第2列</div>
    <div class="col-sm-4">第3行第3列</div>
  </div>
  <div class="row">
    <div class="col-xs-4">第4行第1列</div>
    <div class="col-xs-4">第4行第2列</div>
    <div class="col-xs-4">第4行第3列</div>
  </div>
</div>
```

**3.** 组合使用
示例代码如下：
缩小浏览器宽度改变div的大小，仔细观察，四列，变三列，再变两列，最后变成一列的效果
```html
<style>
  .row div{
    background: green;
    color: #fff;
    height: 100px;
    border: 1px solid #000;
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-6">第1行第1列</div>
    <div class="col-lg-3 col-md-4 col-sm-6">第1行第2列</div>
    <div class="col-lg-3 col-md-4 col-sm-6">第1行第3列</div>
    <div class="col-lg-3 col-md-4 col-sm-6">第1行第4列</div>
  </div>
  <div class="row">
    <div class="col-lg-3">第1行第1列</div>
  </div>
</div>
```

  **4.** 列偏移
col-*-offset-* 向右偏移，第一个 * 是和屏幕尺寸有关，第二个 * 是偏移的列数，如果偏移的数量大于12则会不起作用。

示例代码如下：
```html
<style>
  .row div{
    background: green;
    color: #fff;
    height: 100px;
    border: 1px solid #000;
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-lg-4">第1行第1列</div>
    <div class="col-lg-4">第1行第2列</div>
    <div class="col-lg-4">第1行第3列</div>
  </div>
  <div class="row">
     <!--可以放开注释试着玩玩，看看效果-->
    <!--<div class="col-lg-4 col-lg-offset-4">第2行第1列</div>-->
    <!--<div class="col-lg-4 col-lg-offset-5">第2行第1列</div>-->
    <!--<div class="col-lg-4 col-lg-offset-12">第2行第1列</div>-->
    <div class="col-lg-4 col-lg-offset-13">第2行第1列</div>
  </div>
</div>
```

  **5.** 列排序
col-*-push（pull）-* 第一个 * 是和屏幕尺寸有关，第二个 * 是往右或者往左的列数，不能超过12，否则就不起作用，push是往右推，pull是往左拉。

示例代码如下：
```html
<style>
  .row div{
    background: green;
    color: #fff;
    height: 100px;
    border: 1px solid #000;
  }
</style>

<div class="container" style="border: 1px solid #f00;">
  <div class="row">
    <div class="col-lg-2 col-lg-push-5">第1行第1列</div>
    <div class="col-lg-2 col-lg-push-13">第1行第2列</div>
  </div>
  
  <div class="row">
    <div class="col-lg-2 col-lg-pull-1">第2行第1列</div>
    <div class="col-lg-2 col-lg-pull-13">第2行第2列</div>
  </div>
  
  <!--调换位置效果-->
  <div class="row">
    <div class="col-lg-2 col-lg-push-10">第3行第1列</div>
    <div class="col-lg-10 col-lg-pull-2">第3行第2列</div>
  </div>
</div>
```

**6.** 列偏移和列排序的区别
  - 1. 列偏移只能往右走，而列排序（pull、push）既可以往右边走，也可以往左边走
  - 2. 如果一行中有多列，offset偏移如果大的话，会换行再偏移，而push不会有这个问题，可以溢出父级的容器。

示例代码如下：
```html
<style>
  .row div{
    background: green;
    color: #fff;
    height: 100px;
    border: 1px solid #000;
  }
</style>

<div class="container" style="border: 1px solid #f00;">
  <!--第一个区别-->
  <div class="row">
    <div class="col-lg-2 col-lg-offset-10">第1行第1列</div>
  </div>
  <div class="row">
    <div class="col-lg-2 col-lg-push-10">第2行第1列</div>
  </div>
  <div class="row">
    <div class="col-lg-2 col-lg-pull-2">第3行第1列</div>
  </div>
  
  <!--第二个区别-->
  <!-- 一行有多列总份数超过12则会出现折行-->
  <div class="row">
    <div class="col-lg-4">第4行第1列</div>
    <div class="col-lg-4 col-lg-offset-4">第4行第2列</div>
  </div>
  
  <div class="row">
    <div class="col-lg-4">第5行第1列</div>
    <div class="col-lg-4 col-lg-push-5">第5行第2列</div>
  </div>
</div>
```
**7.** 嵌套
每一列里面都可以在嵌套一行和n（不能超过12）列，那嵌套里面的元素就会以父级的宽度为基准，再分12个列。

示例代码如下：
```html
<style>
  .row div{
    background: green;
    color: #fff;
    height: 100px;
    border: 1px solid #000;
  }
</style>

<div class="container" style="border: 1px solid #f00;">
  <div class="row">
    <div class="col-lg-6">
      第1行第1列
      <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4"></div>
        <div class="col-lg-4"></div>
      </div>
    </div>
    <div class="col-lg-6">第1行第2列</div>
  </div>
</div>
```

## 本篇结束，写在最后
>栅格系统用来网页布局，这是网页设计的第一步，另外Bootstrap的中文网上（官网的翻译版）里面的布局方式也值得我们学习，可以仔细观察一下。

Bootstrap 系列：

Bootstrap学习文档（一）
[Bootstrap学习文档（二）](http://www.jianshu.com/p/8d69f5b97bf3)
[Bootstrap学习文档（三）](http://www.jianshu.com/p/8419ceb4be55)
[Bootstrap学习文档（四）](http://www.jianshu.com/p/3f90fd868b6a)
