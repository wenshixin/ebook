![Bootstrap](http://upload-images.jianshu.io/upload_images/5763525-ce9461d49270d339.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

>注意下面的组件，很多是需要用到 js 的，所以要引入 Bootstrap 的 js 文件和
 jquery 文件在示例代码中，我只是没有写，注意加上哦。

## 字体图标

Bootstrap 中还带有免费的图标供我们使用，这些图标使用起来很简单，就像使用字体一样，改变图标的大小就是改变字体的 `font-size`，改变图标的颜色就是改变字体的颜色 `color`。有了这些图标，再也不用切图啦。另外还有第三方的图标我们可以使用，比如说 [Font Awesome](http://www.bootcss.com/p/font-awesome/) ，也是这种字体图标。在Bootstrap的下载包中，记得引入fonts文件，这样图标才能显示出来。

示例代码如下：

```html
<div class="container">
  <div class="row">
    <span class="glyphicon glyphicon-star" style="font-size: 40px; color: red;"></span>
  </div>
  
  <button class="btn btn-danger">
    <span class="glyphicon glyphicon-camera" style="color: #000;"></span> kaivon
  </button>
</div>
```

## 按钮组
- btn-group	按钮组
- btn-group-* 改变按钮的尺寸，* 的内容为 lg md sm xs
- btn-group-vertical 按钮组竖着排
- btn-group-justified 让按钮两端对齐，三种实现方式，推荐使用 a 标签
  - 如果说是 button 标签，那需给他们添加一个父级，并且给父级添加一个 btn-group 的 class
  - 如果说是 input 标签，那需给他们添加一个父级，并且给父级添加一个 btn-group 的 class
  - 如果说是 a 标签，则不需要给他们添加一个父级，写起来起来比较简单



示例代码如下：

```html
<div class="container">
  <!--单个按钮-->
  <div class="row">
    <button class="btn btn-primary">kaivon</button>
    <button class="btn btn-success">kaivon</button>
    <button class="btn btn-info">kaivon</button>
  </div>
  
  <!--按钮组-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-success">kaivon</button>
      <button class="btn btn-info">kaivon</button>
    </div>
  </div>
  
  <!--按钮组-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group btn-group-lg">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-success">kaivon</button>
      <button class="btn btn-info">kaivon</button>
    </div>
    <div class="btn-group btn-group-md">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-success">kaivon</button>
      <button class="btn btn-info">kaivon</button>
    </div>
    <div class="btn-group btn-group-sm">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-success">kaivon</button>
      <button class="btn btn-info">kaivon</button>
    </div>
    <div class="btn-group btn-group-xs">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-success">kaivon</button>
      <button class="btn btn-info">kaivon</button>
    </div>
  </div>
  
  <!--按钮组,竖着排-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group-vertical">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-success">kaivon</button>
      <button class="btn btn-info">kaivon</button>
    </div>
  </div>
  
  <!--btn两端对齐，如果说是button，那需给他们添加一个父级，并且给父级添加一个btn-group的class-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group btn-group-justified">
      <div class="btn-group">
        <button class="btn btn-primary">kaivon</button>
      </div>
      <div class="btn-group">
        <button class="btn btn-success">kaivon</button>
      </div>
      <div class="btn-group">
        <button class="btn btn-info">kaivon</button>
      </div>
    </div>
  </div>
  
  <!--btn两端对齐，如果说是input，那需给他们添加一个父级，并且给父级添加一个btn-group的class-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group btn-group-justified">
      <div class="btn-group">
        <input type="button" class="btn btn-primary" value="kaivon" />
      </div>
      <div class="btn-group">
        <input type="button" class="btn btn-success" value="kaivon" />
      </div>
      <div class="btn-group">
        <input type="button" class="btn btn-info" value="kaivon" />
      </div>
    </div>
  </div>
  
  <!--btn两端对齐，如果说是 a 标签，则不需要给他们添加一个父级，写起来起来比较简单-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group btn-group-justified">
      <a href="#" class="btn btn-primary">kaivon</a>
      <a href="#" class="btn btn-success">kaivon</a>
      <a href="#" class="btn btn-info">kaivon</a>
    </div>
  </div>
</div>
```

- 按钮组图标
按钮组中可以添加图标，比如说常见的下拉按钮图标,，同时添加的方式也有不同，下面还是给出了三种不同的标签添加，input标签是不能这样使用的。

示例代码如下：

```html
<!--给按钮添加三角形-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group">
      <button class="btn btn-primary">kaivon <span class="caret"></span></button>
      <a href="#" class="btn btn-success">kaivon <span class="caret"></span></a>
      <input type="button" class="btn btn-info" value="kaivon" /> <span class="caret"></span>
    </div>
  </div>
  
  <!--能过组的试给按钮添加下三角形-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-primary"><span class="caret"></span></button>
    </div>
  </div>
  
  <!--能过组的试给按钮添加上三角形-->
  <div class="row" style="margin-top: 10px;">
    <div class="btn-group dropup">
      <button class="btn btn-primary">kaivon</button>
      <button class="btn btn-primary"><span class="caret"></span></button>
    </div>
  </div>
```

## 下拉菜单

用 Bootstrap 做一个下拉菜单只需要三步，Bootstrap 官网上写的有些复杂，这是因为 Bootstrap 考虑到了屏幕阅读器这类用户的体验，我们则不需要这个。
- 1.给父级div添加一个 dropdown的class，或者给父级添加定位属性
- 2.给button按钮添加一个data-toggle="dropdown"的属性，最好再加一个dropdown-toggle的class，这样可以消除按钮上的光环
- 3.ul需要添加一个dropdown-menu的class

示例代码如下：

```html
<div class="container">
  <div class="row">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">前端 <span class="caret"></span></button>
      
      <ul class="dropdown-menu">
        <li><a href="#">html</a></li>
        <li><a href="#">css</a></li>
        <li><a href="#">javascript</a></li>
        <li><a href="#">less</a></li>
        <li><a href="#">bootstrap</a></li>
      </ul>
    </div>
  </div>
</div>
```

下拉菜单的样式
下拉菜单除了上面默认的样式还有其他不同的样式，比如说下拉菜单右对齐（默认是左对齐），下拉菜单里面也有样式可以选择。

- open 默认菜单是展开的，给 dropdown 添加
- dropup 让菜单在上面显示，将默认的 dropdown 换为dropup
- dropdown-menu-right 下拉菜单右对齐，注意button按钮的位置，要撑满整个父级，可以用按钮的 btn-block 类名
- dropdown-header 菜单里有标题，给 li 添加
- text-center 让内容居中，对段落 p 标签也是适用的哦
- divider 分隔线，给li添加

示例代码如下：

```html
<div class="container">
  <div class="row">
    <div class="col-lg-4">
      <div class="dropdown open">
        <button class="btn btn-default dropdown-toggle btn-block" data-toggle="dropdown">默认打开 <span class="caret"></span></button>
        
        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#">html</a></li>
          <li><a href="#">css</a></li>
          <li><a href="#">javascript</a></li>
          <li><a href="#">less</a></li>
          <li><a href="#">bootstrap</a></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-4" style="margin-top: 150px;">
      <div class="dropup">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">向上展开 <span class="caret"></span></button>
        
        <ul class="dropdown-menu">
          <li><a href="#">html</a></li>
          <li><a href="#">css</a></li>
          <li><a href="#">javascript</a></li>
          <li><a href="#">less</a></li>
          <li><a href="#">bootstrap</a></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">带有标题和分割线 <span class="caret"></span></button>
        
        <ul class="dropdown-menu">
          <li class="dropdown-header text-center"><a href="#">前端</a></li>
          <li><a href="#">html</a></li>
          <li><a href="#">css</a></li>
          <li><a href="#">javascript</a></li>
          <li><a href="#">less</a></li>
          <li><a href="#">bootstrap</a></li>
          <li class="divider"></li>
          <li class="dropdown-header text-center"><a href="#">后端</a></li>
          <li><a href="#">php</a></li>
          <li><a href="#">java</a></li>
          <li class="disabled"><a href="#">.net</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
```

## 输入框

- input-group 输入框组，只能针对输入框，是我们前面说到的 form-group 的一部分
- input-group-addon 给输入框前后添加的额外元素
- input-group-*	修改输入框组的尺寸，与之前不同的是，这里 * 的内容只有 lg md sm 三种尺寸。
- input-group-btn 添加的额外元素是按钮，一般的按钮的话用 span 标签包裹起来，下拉菜单按钮，外面需要 div 包裹起来，注意哦~。

示例代码如下：

```html
<div class="container">
      <div class="row">
        <div class="input-group">
          <span class="input-group-addon">@</span>
          <input type="text" class="form-control" />
        </div>
      </div>
      
      <!--输入框组的尺寸-->
      <div class="row" style="margin-top: 10px;">
        <div class="input-group input-group-lg">
          <span class="input-group-addon">@</span>
          <input type="text" class="form-control" />
        </div>
        
        <div class="input-group input-group-md">
          <span class="input-group-addon">@</span>
          <input type="text" class="form-control" />
        </div>
        
        <div class="input-group input-group-sm">
          <span class="input-group-addon">@</span>
          <input type="text" class="form-control" />
        </div>

        <!--没有这样的尺寸-->
        <!--<div class="input-group input-group-xs">
          <span class="input-group-addon">@</span>
          <input type="text" class="form-control" />
        </div>-->
      </div>
      
      
      <!--额外内容里放的是checkbox-->
      <div class="row" style="margin-top: 10px;">
        <div class="input-group">
          <span class="input-group-addon">
            <input type="checkbox" />
          </span>
          <input type="text" class="form-control" />
        </div>
      </div>
      
      <!--额外内容里放的是radio-->
      <div class="row" style="margin-top: 10px;">
        <div class="input-group">
          <span class="input-group-addon">
            <input type="radio" />
          </span>
          <input type="text" class="form-control" />
        </div>
      </div>
      
      <!--额外内容里放的是按钮-->
      <div class="row" style="margin-top: 10px;">
        <div class="input-group">
          <input type="text" class="form-control" />
          <span class="input-group-btn">
            <button type="button" class="btn btn-default">搜索</button>
          </span>
        </div>
      </div>
      
      <!--额外内容里放的是按钮-->
      <div class="row" style="margin-top: 10px;">
        <div class="input-group">
          <div class="input-group-btn">
              <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Bootstrap课程 <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">html</a></li>
                <li><a href="#">css</a></li>
                <li><a href="#">javascript</a></li>
                <li><a href="#">less</a></li>
                <li><a href="#">bootstrap</a></li>
              </ul>
          </div>
          <input type="text" class="form-control" />
        </div>
      </div>
    </div>
```

## 导航

Bootstrap 中的导航分为标签页式导航，面包屑式导航，两端对齐式导航，可以有需要的选择使用，导航里面又可以放下拉菜单。

示例代码如下：
- nav 导航的基本样式
- nav-tabs 标签式导航
- nav-pills 胶囊式导航
- nav-stacked 导航竖着排
- nav-stacked 两端对齐式导航

```html
<div class="container">
  <div class="row">
    <ul class="nav nav-tabs">
      <li><a href="#">程序员</a></li>
      <li class="active"><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </div>
  
  <div class="row" style="margin-top: 10px;">
    <ul class="nav nav-pills">
      <li><a href="#">程序员</a></li>
      <li class="active"><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </div>
  
  <div class="row" style="margin-top: 10px;">
    <ul class="nav nav-pills nav-stacked">
      <li><a href="#">程序员</a></li>
      <li class="active"><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </div>
  
  <div class="row" style="margin-top: 10px;">
    <ul class="nav nav-pills nav-justified">
      <li><a href="#">程序员</a></li>
      <li class="active"><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </div>
  
  
  <!--导航中放下拉菜单 -->
  <div class="row" style="margin-top: 10px;">
    <ul class="nav nav-tabs">
      <li><a href="#">程序媛</a></li>
      <li class="active"><a href="#">程序猿</a></li>
      <li><a href="#">kaivon</a></li>
      <li>
        <a href="#" data-toggle="dropdown">课程 <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">html</a></li>
          <li><a href="#">css</a></li>
          <li><a href="#">javascript</a></li>
          <li><a href="#">less</a></li>
          <li><a href="#">bootstrap</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
```
选项卡

上面的导航可以变成选项卡式的，这样对于内容较多的页面就可以进行有效的分离。

制作选项卡的步骤：
- 1.导航对应的所有内容需要放到一个class为tab-content的层里面
- 2.一个内容块都要加上一个名为tab-pane的class，并且给对应的默认显示的内容添加一个active的class
- 3.给每一个导航的a标签添加一个data-toggle="tab"的自定义属性表示选项卡，还记得下拉菜单的data-toggle的值吗？没错，是dropdown。
- 4.给每一个内容部分添加一个id
- 5.给每一个导航里的a标签添加一个锚点

示例代码如下：

```html
<style>
  .tab1{
    border: 1px solid #ddd;
    border-top: none;
    padding: 100px;
    border-radius: 0 0 5px 5px;
  }
</style>

<div class="container">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#content1" data-toggle="tab">程序员</a></li>
      <li><a href="#content2" data-toggle="tab">程序媛</a></li>
      <li><a href="#content3" data-toggle="tab">code</a></li>
    </ul>
    <div class="tab-content">
      <div id="content1" class="tab-pane active">html</div>
      <div id="content2" class="tab-pane">css</div>
      <div id="content3" class="tab-pane">js</div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-lg-4">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#content4" data-toggle="tab">程序员</a></li>
        <li><a href="#content5" data-toggle="tab">程序媛</a></li>
        <li><a href="#content6" data-toggle="tab">code</a></li>
      </ul>
      <div class="tab-content tab1">
        <div id="content4" class="tab-pane active">html</div>
        <div id="content5" class="tab-pane">css</div>
        <div id="content6" class="tab-pane">js</div>
      </div>
    </div>
  </div>
</div>  
```

## 导航条

1. 导航条的基本样式
- navbar 导航条的基础样式
- nav navbar-nav 导航条里菜单的固定样式组合class
- navbar-default 导航条的默认样式
- navbar-inverse 导航条的样式为黑色
- navbar-static-top 导航条为直角，改变导航条默认的圆角效果
- navbar-fixed-top 导航条固定在最上边，需要给body加一个padding或者margin，使得固定的导航条不会覆盖下面的内容
-navbar-fixed-bottom 导航条固定在最下边，不会随滚动的滚动而变动

示例代码如下：

```html
<body style="padding: 50px 0; height: 2000px;">
<div class="container">
  <!--默认样式的导航条-->
  <nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">学院</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </nav>
  
  <!--黑色的导航条-->
  <nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">学院</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </nav>
  
  <!--黑色的导航条-->
  <nav class="navbar navbar-inverse navbar-static-top">
    <ul class="nav navbar-nav">
     <li class="active"><a href="#">学院</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </nav>
  
  <!--固定在上边的导航条-->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">学院</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </nav>
  
  <!--固定在上边的导航条-->
  <nav class="navbar navbar-default navbar-fixed-bottom">
    <ul class="nav navbar-nav">
     <li class="active"><a href="#">学院</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </nav>
</div>
</body>
```

2.导航条的进阶

- navbar-header 导航的头部，一般情况下来放logo
- navbar-brand 用来放logo的，需要配合navbar-header

示例代码如下：
```html
<!--一般情况下都会把nav标签包在container的外面，解决 active 项溢出的问题-->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">logo</a>
    </div>
    
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">程序员</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
  </div>
</nav>
```
```html
<!--对齐方式的导航条-->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">logo</a>
    </div>
    <!--navbar-left表示内容以左边对齐-->
    <ul class="nav navbar-nav navbar-left">
      <li class="active"><a href="#">程序员</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
    <!--navbar-left表示内容以右边对齐-->
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">登录</a></li>
      <li><a href="#">注册</a></li>
    </ul>
  </div>
</nav>
```
```html
<!--有链接与文字的导航条-->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">logo</a>
    </div>
    <!--navbar-left表示内容以左边对齐-->
    <ul class="nav navbar-nav navbar-left">
      <li class="active"><a href="#">程序员</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
    <!--如果导航里有文字需要给文字加上navbar-text，如果有链接需要给链接加上navbar-text-->
    <a href="#" class="navbar-link navbar-text">链接</a>
    <p class="navbar-text">这里是一段文字</p>
    <!--navbar-left表示内容以右边对齐-->
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">登录</a></li>
      <li><a href="#">注册</a></li>
    </ul>
  </div>
</nav>
```
```html
<!--有表单的导航条-->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">logo</a>
    </div>
    <!--navbar-left表示内容以左边对齐-->
    <ul class="nav navbar-nav navbar-left">
      <li class="active"><a href="#">程序员</a></li>
      <li><a href="#">程序媛</a></li>
      <li><a href="#">code</a></li>
    </ul>
    <!--如果导航里只有一个按钮，那给它加一个navbar-btn-->
    <button class="btn btn-default navbar-btn">搜索</button>
    <!--如果导航里有form，那就需要给form添加一个navbar-form，想让它们在一行显示，就要添加一个navbar-left-->
    <form action="http://www.apeclass.com/" class="navbar-form navbar-left">
      <input type="text" class="form-control" />
      <button class="btn btn-default">搜索</button>
    </form>
    <!--navbar-left表示内容以右边对齐-->
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">登录</a></li>
      <li><a href="#">注册</a></li>
    </ul>
  </div>
</nav>
```

## 路径导航

在比较复杂点页面中，有时候我们需要让用户知道自己所处的位置，我们就可以使用路径导航组件，使用起来也很简单的。

示例代码如下：
```html
<div class="container">
  <div class="row">
    <ul class="breadcrumb">
      <li><a href="#">bootstrap</a></li>
      <li><a href="#">dist</a></li>
      <li class="active">css</li>
    </ul>
  </div>
</div>
```

## 分页

对一个数量比较多的列表内容，我们需要使用分页功能来显示一部分内容，Bootstrap 为我们提供了分页的组件。

示例代码如下：

```html
<div class="container">
  <div class="row">
    <ul class="pagination">
      <li class="disabled"><a href="#"><<</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li class="active"><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">>></a></li>
    </ul>
  </div>
  
  <div class="row">
    <ul class="pagination pagination-lg">
      <li class="disabled"><a href="#"><<</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li class="active"><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">>></a></li>
    </ul>
  </div>
  
  <div class="row">
    <ul class="pagination pagination-md">
      <li class="disabled"><a href="#"><<</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li class="active"><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">>></a></li>
    </ul>
  </div>
  
  <div class="row">
    <ul class="pagination pagination-sm">
      <li class="disabled"><a href="#"><<</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li class="active"><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">>></a></li>
    </ul>
  </div>
```
翻页
可以分居两边，只需要加上 previous 和 next 的class 类名
```html
  <ul class="pager">
    <li><a href="#">←上一页</a></li>
    <li><a href="#">下一页→</a></li>
  </ul>
  <ul class="pager">
    <li class="previous disabled"><a href="#">←上一页</a></li>
    <li class="next"><a href="#">下一页→</a></li>
  </ul>
</div>
```

## 标记

有时候想显示消息的数目，就像QQ或者微信那样，我们可以实用标记类加在 span 标签上。

示例代码如下：
```html
<div class="container">
  <div class="row">
    <a href="#">未读消息<span class="badge">12</span></a>
    <button class="btn btn-primary">未读消息<span class="badge">12</span></button>
  </div>
</div>
```

## 巨幕

巨幕可以用在全屏显示或者铺满整个页面，还以用在页头，挺实用的类

示例代码如下：

```html
<div class="container">
  <div class="jumbotron">
    <h1>程序员</h1>
    <p>程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员</p>
    <p><a href="#" class="btn btn-primary">程序媛</a></p>
  </div>
</div>


<div class="jumbotron">
  <div class="container">
    <h1>程序员</h1>
    <p>程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员程序员</p>
    <p><a href="#" class="btn btn-primary">程序媛</a></p>
  </div>
</div>
```

## 缩略图

图片配上文字的首选，只需要给父级添加一个 thumbnail 类名就可以变成一个缩略图，而给文字加上 caption 类名会使文字与间距增大一点。

示例代码如下：

```html
<div class="container">
  <div class="row">
    <div class="col-lg-3">
      <a href="http://www.baidu.com" class="thumbnail">
        ![](...)
      </a>
    </div>
    <div class="col-lg-3">
      <a href="http://www.baidu.com" class="thumbnail">
        ![](...)
      </a>
    </div>
    <div class="col-lg-3">
      <a href="http://www.baidu.com" class="thumbnail">
        ![](...)
      </a>
    </div>
    <div class="col-lg-3">
      <a href="http://www.baidu.com" class="thumbnail">
       ![](...)
      </a>
    </div>
  </div>
  
  <!--还可以把thumbnail给父级，会有不同的样式-->
  <div class="row">
    <div class="col-lg-4">
      <div class="thumbnail">
        <a href="http://www.baidu.com">
         ![](...)
        </a>
        <div>
          <h3>程序员</h3>
          <p>程序员爱程序媛程序员爱程序媛程序员爱程序媛程序员爱程序媛</p>
          <p><a href="#" class="btn btn-primary">程序媛</a> <a href="#" class="btn btn-danger">code</a></p>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="thumbnail">
        <a href="http://www.apeclass.com">
         ![](...)
        </a>
        <div class="caption">
          <h3>程序员</h3>
          <p>程序员爱程序媛程序员爱程序媛程序员爱程序媛程序员爱程序媛</p>
          <p><a href="#" class="btn btn-primary">程序媛</a> <a href="#" class="btn btn-danger">code</a></p>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="thumbnail">
        <a href="http://www.apeclass.com">
          ![](...)
        </a>
        <div class="caption">
         <h3>程序员</h3>
          <p>程序员爱程序媛程序员爱程序媛程序员爱程序媛程序员爱程序媛</p>
          <p><a href="#" class="btn btn-primary">程序媛</a> <a href="#" class="btn btn-danger">code</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
```

## 警告框

有时候用户在填写表单信息时，我们可以在输入框下面填上一些信息，并带有关闭的小按钮，Bootstrap 也为我们提供了这些组件，而不用我们自己在写 js 的代码，还是通过自定义属性实现的，`data-dismiss="alert"`，在使用时只需要加上 alert 的类名即可，其实现在可以感受到，学习 Bootstrap 就是记住 class ，不用我们在使用是想样式。

示例代码如下：

```html
<div class="container">
  <div class="row">
    <p class="alert alert-success">这里是一个警告框！！！！<button class="close" data-dismiss="alert">×</button></p>
    <p class="alert alert-info">这里是一个警告框！！！！</p>
    <p class="alert alert-warning">这里是一个警告框！！！！</p>
    <p class="alert alert-danger">这里是一个警告框！！！！</p>
  </div>
</div>
```


## 进度条

可以用来显示一个事件的进度，Bootstrap 也做了很多样式，比如颜色，条纹，并且还可以使进度条动起来，但更多的效果功能还是要配合 js来实现。

示例代码如下：

```html
<div class="container">
  <div class="row">
    <div class="progress">
      <div class="progress-bar" style="width: 60%;">60%</div>
    </div>
    
    <div class="progress">
      <div class="progress-bar progress-bar-info" style="width: 36%;">36%</div>
    </div>
    
    <div class="progress">
      <div class="progress-bar progress-bar-warning" style="width: 70%;">70%</div>
    </div>
    
    <div class="progress">
      <div class="progress-bar progress-bar-danger" style="width: 20%;">20%</div>
    </div>
    
    <!--带条纹的进度条-->
    <div class="progress">
      <div class="progress-bar progress-bar-danger progress-bar-striped active" style="width: 20%;">20%</div>
    </div>
    
    <!--带堆叠效果的进度条-->
    <div class="progress">
      <div class="progress-bar" style="width: 10%;">10%</div>
      <div class="progress-bar progress-bar-info" style="width: 20%;">36%</div>
      <div class="progress-bar progress-bar-warning" style="width: 30%;">70%</div>
      <div class="progress-bar progress-bar-danger" style="width: 20%;">20%</div>
      <div class="progress-bar progress-bar-danger progress-bar-striped active" style="width: 10%;">20%</div>
    </div>
  </div>
</div>
```

## 图文编排

图文编排也是网页上经常使用的组件，例如在留言或者评论区域，或者文字说明，当然需要结合其他样式使用，才能是图文搭配的更好看。

- media 图文混排，默认图片靠上对齐
- media-left 图片的区域，在左边显示
- media-right 图片的区域，在右边显示
- media-middle 图片居中对齐
- media-bottom 图片靠下对齐
- media-body 内容区域
- media-heading 内容区域里的标题

示例代码如下

```html
<div class="container">
  <div class="row">
    <div class="media">
      <a href="http://www.baidu.com" class="media-left">
       ![](...)
      </a>
      <div class="media-body">
        <h4 class="media-heading">程序员</h4>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
      </div>
    </div>
  </div>
  
  <!--图片在右边显示-->
  <div class="row">
    <div class="media">
      <div class="media-body">
        <h4 class="media-heading">程序员</h4>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
      </div>
      <a href="http://www.baidu.com" class="media-right">
        ![](...)
      </a>
    </div>
  </div>
```

图文混排复杂的使用方式
```html
  <!--左右都有图片-->
  <div class="row">
    <div class="media">
      <a href="http://www.baidu.com" class="media-left">
      ![](...)
      </a>
      <div class="media-body">
        <h4 class="media-heading">程序员</h4>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
      </div>
      <a href="http://www.baidu.com" class="media-right">
       ![](...)
      </a>
    </div>
  </div>
  
  <!--图片在居中对齐和靠下对齐-->
  <div class="row">
    <div class="media">
       <a href="http://www.baidu.com" class="media-left media-middle">
        ![](...)
      </a>
      <div class="media-body">
        <h4 class="media-heading">程序员</h4>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
      </div>
      <a href="http://www.baidu.com" class="media-right media-bottom">
       ![](...)
      </a>
    </div>
  </div>

  <!--右侧内容又嵌套了一个media-->
  <div class="row">
    <div class="media">
      <a href="http://www.baidu.com" class="media-left">
       ![](...)
      </a>
      <div class="media-body">
         <h4 class="media-heading">程序员</h4>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
        <div class="media">
          <a href="http://www.baidu.com" class="media-left">
           ![](...)
          </a>
          <div class="media-body">
             <h4 class="media-heading">程序员</h4>
            <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
            <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
            <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
            <p>程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员程序媛爱程序员</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
```

## 列表组

可以用来用作侧栏导航或者信息列表的展示。
- list-group 列表组,给 ul 添加
- list-group-item 列表项,给 li 添加

示例代码如下：

```html
<div class="row">
  <ul class="list-group">
    <li class="list-group-item">html</li>
    <li class="list-group-item">css</li>
    <li class="list-group-item">javascript</li>
    <li class="list-group-item">html5/css3</li>
    <li class="list-group-item">less/sass</li>
    <li class="list-group-item">bootstrap</li>
  </ul>
</div>
```

还可以结合前面的标记组件来使用，并且标记会自动跑到右边显示。

示例代码如下：
```html
<!--给每个列表项添加了一个标记，那这个标记就会自动跑到右侧-->
<div class="row">
  <ul class="list-group">
    <li class="list-group-item">html<span class="badge">12</span></li>
    <li class="list-group-item">css<span class="badge">34</span></li>
    <li class="list-group-item">javascript<span class="badge">52</span></li>
    <li class="list-group-item">html5/css3<span class="badge">35</span></li>
    <li class="list-group-item">less/sass<span class="badge">27</span></li>
    <li class="list-group-item">bootstrap<span class="badge">7</span></li>
  </ul>
</div>
```

列表组不仅可以用 ul 和 li 来做，还可以使用 div 加 a 标签来做哦。

示例代码如下：

```html
<!--用a标签做的列表组，可以添加激活状态，禁用状态，背景色-->
<div class="row">
  <div class="list-group">
    <a href="#" class="list-group-item active">html<span class="badge">12</span></a>
    <a href="#" class="list-group-item disabled">css<span class="badge">34</span></a>
    <a href="#" class="list-group-item list-group-item-info">javascript<span class="badge">52</span></a>
    <a href="#" class="list-group-item list-group-item-warning">html5/css3<span class="badge">35</span></a>
    <a href="#" class="list-group-item list-group-item-danger">less/sass<span class="badge">27</span></a>
    <a href="#" class="list-group-item list-group-item-success">bootstrap<span class="badge">7</span></a>
  </div>
</div>
```

列表组里面还可以是标题加段落，可以用来做文章列表。

- list-group-item-heading 列表组标题
- list-group-item-text 列表组内容

示例代码如下：

```html
<!--添加标题与段落的列表组,列表组标题：list-group-item-heading，列表组内容：list-group-item-text-->
<div class="row">
  <ul class="list-group">
    <li class="list-group-item active">
      <h4 class="list-group-item-heading">程序媛</h4>
      <p class="list-group-item-text">程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿</p>
    </li>
    <li class="list-group-item">
      <h4 class="list-group-item-heading">程序媛</h4>
      <p class="list-group-item-text">程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿</p>
    </li><li class="list-group-item">
      <h4 class="list-group-item-heading">程序媛</h4>
      <p class="list-group-item-text">程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿</p>
    </li><li class="list-group-item">
      <h4 class="list-group-item-heading">程序媛</h4>
      <p class="list-group-item-text">程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿</p>
    </li>
  </ul>
</div>
```

## 面板
有标题和内容的部分就可以用面板来做。

- panel 面板
- panel-default	面板默认样式
- panel-heading 面板的头部区域，不只是标题，还有副标题等等
- panel-title 面板的头部区域里标题的样式
- panel-body 面板的内容区域

示例代码如下：
```html
<div class="container">
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">爱秋的艳</h3>
    </div>
    <div class="panel-body">
      <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
    </div>
  </div>
</div>

<div class="row">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">爱秋的艳</h3>
    </div>
    <div class="panel-body">
      <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
    </div>
  </div>
</div>

<div class="row">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">爱秋的艳</h3>
    </div>
    <div class="panel-body">
      <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
    </div>
  </div>
</div>


<div class="row">
  <div class="panel panel-info">
    <div class="panel-heading">
      <h3 class="panel-title">爱秋的艳</h3>
    </div>
    <div class="panel-body">
      <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
    </div>
  </div>
</div>

<div class="row">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h3 class="panel-title">爱秋的艳</h3>
    </div>
    <div class="panel-body">
      <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
    </div>
  </div>
</div>

<div class="row">
  <div class="panel panel-danger">
    <div class="panel-heading">
      <h3 class="panel-title">爱秋的艳</h3>
    </div>
    <div class="panel-body">
      <p>爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳爱秋的艳</p>
    </div>
  </div>
</div>
```

面板还可以结合列表来使用，注意列表组没有放在panel-body中哦。

示例代码如下：

```html
<!--面板与列表组结合起来-->
      <div class="row">
        <div class="panel panel-danger">
          <div class="panel-heading">
            <h3 class="panel-title">学院</h3>
          </div>
          <div class="panel-body">
            <p>程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿程序媛爱程序猿</p>
          </div>
          <ul class="list-group">
          <li class="list-group-item">html</li>
          <li class="list-group-item">css</li>
          <li class="list-group-item">javascript</li>
          <li class="list-group-item">html5/css3</li>
          <li class="list-group-item">less/sass</li>
          <li class="list-group-item">bootstrap</li>
        </ul>  
        </div>
      </div>
```

>以上呢就是 Bootstrap 组件的全部内容啦，Bootstrap 的设计是很有规律的呢，多看多用，自然就可以记住啦。

Bootstrap 系列：

[Bootstrap学习文档（一）](http://www.jianshu.com/p/9cc8176ad0ce)
[Bootstrap学习文档（二）](http://www.jianshu.com/p/8d69f5b97bf3)
Bootstrap学习文档（三）
[Bootstrap学习文档（四）](http://www.jianshu.com/p/3f90fd868b6a)
