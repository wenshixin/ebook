---
style: plain
---
团队文档及编码规范
===

> 黄金定律：不管有多少人共同参与同一项目，一定要确保每一行代码都像是同一个人编写的。

## 一、文档
---

对于团队文档的编写，排版要简洁清晰，输写要规范统一，编辑工具为 Markdown 和 Word 。正式的需要打印上交的文档使
用 Word 编辑，不太正式的文档推荐使用 Markdown 输写较好。一个项目的上线的官方文档必须使用 Markdown，Markdown 文档更适合在互联网上传播，Markdown 的学习可以参考 [我谈 Markdown](http://www.jianshu.com/p/01694193400b) 这篇文档。

### （一）排版

1.【强制】标题：一篇文档最多可使用六个等级的标题，文档的标题为一级标题，接下来是二级标题，三级标题，标题的使用要连续。

比如：本文的标题为 **团队文档及编码规范**，这就是一级标题，**一、文档** 和 **二、前端** 等都是二级标题，**（一）排版** 和 **（二）输写** 为二级标题 **一、文档** 下的小标题，为三级标题。

2.【强制】图片：没有特殊要求的，一般为水平居中。

3.【强制】表格：没有特殊要求的，表格中的文字水平和垂直方向都要居中。

4.【强制】标点符号：中文文档，标点符号使用中文半角的，但并不排除使用英文符号，使用英文符号，要和前后的字符留一个空格。

正例：今天的天气不错！我爱学习，学习爱我。

反例：今天的天气不错!我爱学习,学习爱我.

5.【推荐】空格：中文文档中出现英文单词，英文的前后要和中文之间留出一个空格，英文和中文标点之间则不需要空格。有些名词是中英混合的，可以在英文和不想关的中文之间留有空格，也可以不留空格。

说明：在合适的位置增加空格，能够使文档的内容更容易阅读。

正例：我们一起来学习 C++ 和 C语言吧。

正例：我喜欢 Java。

反例：我们一起来使用C++和C语言吧。

反例：我不喜欢Java 。

6.【推荐】 表情：在文档中如果要加入一些小表情，如输入法的表情，建议和使用英文一样，表情前后留有空格。

正例：礼物 🎁 送给你！

反例：礼物🎁送给你！

### （二）输写

1.【强制】大小写：英文专业术语，该大写的要大写，该小写的小写，除特别情况，不要全小写或者全大写。

正例：GitHub / MySQL / 

反例：github / Mysql / 

2. 【推荐】数字和单位：数字和单位之间，推荐不留空格，但也有些人把数字和后面的单位不看成整体，两者之间也留一个空格，网上也没有明确的写法。

### （三）其他


## 二、前端
---

### （一）HTML

1.【强制】语法：HTML5 的文档类型。

说明：为每个页面的每一行添加标准模式的文档声明，这样确保在每个浏览器中拥有一致的展现。

2.【强制】用两个空格来代表来代替制表符缩进。

说明：Bootstrap 编码规范上规定使用两个空格来代替制表符（tab），但在阿里巴巴 Java 开发手册规定使用四个空格缩进，使用空格可以保证在所有开发环境下，代码获得一致展现的方法。

3.【强制】属性的定义使用双引号，绝对不要使用单引号。

4.【强制】不可省略结束标签，例如，`<li>`，`</body>`。

5.【强制】字符编码：使用国际化的字符编码 UTF-8。

说明：避免在 HTML 中使用实体标记，使用国际化的 UTF-8 的编码也可防止页面出现乱码，GBK 的页面编码不适合国际化。

6.【强制】IE 兼容模式：确定绘制当前页面所采用的 IE 的版本为最新版本的。

7.【推荐】建议为 html 标签元素指定 lang 属性，从而为文档设置正确的语言。

说明：这将有助于语音合成工具确定其所应该采用的发音，有助于翻译工具确定其翻译时所应遵守的规则等。

8.【推荐】浏览器内核：设置浏览器的内核为 webkit 的。

说明：考虑到国内 360 浏览器的用户，而 360 浏览器的默认内核为 IE，设置 webkit 内核使 360 浏览器渲染页面时使用极速模式来达到更好的页面渲染效果。

正例：综合上面几点。

```html
<!DOCTYPE html>
<html lang="zh-cn">
  <head>
	<meta charset="UTF-8">
	<!-- 使IE浏览器用最新的edge引擎渲染页面 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- 使360浏览器渲染页面时默认使用极速模式 -->
    <meta name="renderer" content="webkit">
    <title>Page title</title>
  </head>
  <body>
    <img src="images/company-logo.png" alt="Company">
    <h1 class="hello-world">Hello, world!</h1>
  </body>
</html>
```

9.【推荐】JS 文件的加载优化：js文件的引入位置放在 body 标签结束之前。

说明：JS 的优化的重点在于JS的下载时间，下面的写法更符合现代的浏览器。

- 使用DNS预解析，非常简单，效果也是立杆见影。

- preload 属性使浏览器在遇到 link 标签时，立即下载 JS 文件（不阻塞的解析），并放在内存中，但并不执行 JS 文件中的语句，只有当遇到 script 标签时采用将预先下载的 JS 文件执行掉。

- prefetch 属性使浏览器在空闲的时候下载 JS 文件，并缓存到硬盘中，只有当页面使用的时候才从硬盘中读取，如果 JS 文件还没下载完，浏览器发现 script 标签也引用了相同的资源，浏览器会再次向服务器发出请求的，导致该 JS 文件被加载了两次，所以在页面马上就要使用的资源加 prefetch 属性，要用 preload 属性。

- defer 和 async 是 script 标签的两个属性，用于在不阻塞的页面文档的解析中，控制脚本的下载和执行，defer 执行脚本的时间是在页面元素解析完成之后，JS 的 DOM 事件触发之前；async 执行脚本的时间是在 JS 脚本下载完成之后，带有 async 属性的 script 的执行顺序是不固定的，所以 async 只能用于加载一些独立无依赖的代码，比如 Google Analysis之类。

- 提前告知浏览器，网站马上要使用的 JS 文件，以后可能要用到的 JS 文件，浏览器才能更快的渲染页面，浏览器的解析器在遇到 head 中的 preload 时开始下载 JS 文件，读到 script 标签时，就已经下载完了。浏览器是自上而下按顺序解析 HTML 文档的，这样写就是非阻塞的下载 JS 文件了。

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Faster</title>
  <!-- 预先解析DNS -->
  <link rel="dns-prefetch" href="//cdn.com/">
  <link rel="preload" href="//js.cdn.com/currentPage-part1.js" as="script">
  <link rel="preload" href="//js.cdn.com/currentPage-part2.js" as="script">
  <link rel="preload" href="//js.cdn.com/currentPage-part3.js" as="script">
  <link rel="prefetch" href="//js.cdn.com/prefetch.js">
</head>
<body>
  <script type="text/javascript" src="//js.cdn.com/currentPage-part1.js" defer></script>
  <script type="text/javascript" src="//js.cdn.com/currentPage-part2.js" defer></script>
  <script type="text/javascript" src="//js.cdn.com/currentPage-part3.js" defer></script>
</body>
</html>
```
10.【推荐】标签属性的顺序：按照下面的顺序依次排列，确保代码的易读性。

说明：class 用于标识高度可复用组件，因此应该排在首位。id 用于标识具体组件，应当谨慎使用（例如，页面内的书签），因此排在第二位。

- class
- id，name
- data-*
- src，for，type，href，value
- title，alt
- role，aria-*

11.【强制】不使用 JS 生成 HTML 标签。

### （二）CSS

1.【强制】class 名称中，只能出现小写字符和破折号，这里不要使用下划线和高级语言中的驼峰命名法。

说明：破折号相当于 class 命名空间的作用，例如按钮的样式 `.btn` `.btn-danger`，这点在 BootStrap 框架中很明显，而且这样命名具有规律性，更容易写。

2.【强制】一个 css 样式文件中的 css 样式可能包含很多页面的公共样式和单独样式，在每一大块样式前加上必要的注释，说明这块样式是哪个页面的样式。

正例：
```csss
  /* 全局样式 */
  * {
    margin: 0;
	padding: 0;
	...
  }
  ...

  /* 用户登录页面的样式 */
  login {
	...
  }
  ...
```

1.【强制】使用两个空格来代替制表符。

说明：这是唯一可以保证所有环境下获得一致展现的方法。

2.【强制】为选择器分组时，将单独的选择器单独放在一行。

3.【强制】为了代码的易读性，为每个声明块的左花括号前添加一个空格，声明块的右花括号单独成行。

4.【强制】每条声明属性语句 `：` 后添加一个空格，并且每条属性声明都单独占一行。

5.【推荐】对于属性值或者颜色参数值，省略小于 1 的小数前面的 0。

正例：.5 代替 0.5，-.5px 代替 -0.5px

6.【强制】媒体查询的位置：将媒体查询放在尽可能相关规则的附近，不要将他们打包放在单一样式文件中或者放在文档的底部。

说明：在使用 BootStrap 框架时，尽量使用 BootStrap 框架的媒体查询的 class，少写自己的媒体查询样式。

正例：
```css
.element { ... }
.element-avatar { ... }
.element-selected { ... }

@media (min-width: 480px) {
  .element { ...}
  .element-avatar { ... }
  .element-selected { ... }
}
```

7.【推荐】相关的属性声明应当归为一组，按照下面的顺序排列。

说明：由于定位属性（position）可以从正常的文档流中移除元素，并且还能覆盖盒模型（box model）相关的样式，因此排在样式的首位。盒模型排在第二位，因为它决定了组件的尺寸和位置。其他属性只是影响了组件内部的样式，排在后面。

- position（位置）
- box model（盒模型）
- typographic（印刷）
- visual（视觉）


正例：
```css
.declaration-order {
  /* Positioning */
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;

  /* Box-model */
  display: block;
  float: right;
  width: 100px;
  height: 100px;

  /* Typography */
  font: normal 13px "Helvetica Neue", sans-serif;
  line-height: 1.5;
  color: #333;
  text-align: center;

  /* Visual */
  background-color: #f5f5f5;
  border: 1px solid #e5e5e5;
  border-radius: 3px;

  /* Misc */
  opacity: 1;
}
```
8.【推荐】最好不要使用 `@import`

说明：与 `<link>` 标签相比，`@import` 指令要慢的多，不光增加了额外的请求次数，还会导致不可预料的问题。但在使用 BootStrap 框架时，有时候修改样式时需要的选择器很多，可以打开浏览器的调试窗口，检查元素来解决，尽量不要直接在相应的 HTML 标签上直接写 CSS 样式。

正例：
```css
/* Good CSS */
.selector,
.selector-secondary,
.selector[type="text"] {
  padding: 15px;
  margin-bottom: 15px;
  background-color: rgba(0,0,0,.5);
  box-shadow: 0 1px 2px #ccc, inset 0 1px 0 #fff;
}
```
反例：
```css
/* Bad CSS */
.selector, .selector-secondary, .selector[type=text] {
  padding:15px;
  margin:0px 0px 15px;
  background-color:rgba(0, 0, 0, 0.5);
  box-shadow:0px 1px 2px #CCC,inset 0 1px 0 #FFFFFF
}
```
9.【强制】带前缀的属性，通过缩进的方式，是每个属性的值在垂直方向对齐，这样也方便多行编辑器。

正例：
```css
/* Prefixed properties */
.selector {
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
          box-shadow: 0 1px 2px rgba(0,0,0,.15);
}
```

## 三、后端
---

### （一）命名风格

1.【强制】代码中的命名均不能以下划线或美元符号开始，也不能以下划线或美元符号结束。

反例： _name / __name / /$Object / name_ / name/$ / Object$

2.【强制】类名使用 UpperCamelCase 风格，必须遵从驼峰形式。

正例：Student / AdminUser

反例：student / adminUser

4. 【强制】方法名、参数名、成员变量、局部变量都统一使用 lowerCamelCase 风格，必须遵从
驼峰形式。

正例： localValue / getHttpMessage() / inputUserId

5. 【强制】常量命名全部大写，单词间用下划线隔开，力求语义表达完整清楚，不要嫌名字长。
正例： MAX _ STOCK _ COUNT
反例： MAX _ COUNT

### （二）代码格式

### （三）注释

### （四）其他


## 四、MySQL 数据库
---

### （一）命名风格

1.【强制】表名、字段名：都必须使用小写字母，尽量不使用数字，禁止数字开头，使用下划线作为表名和字段名中单词的分割。

说明：MySQL 在 Windows 下是不区分大小写的，但在 Linux 下是区分大小写的，因此数据库名、表名、字段名都不允许出现大写字母，影响 MySQL数据库的跨平台性。

正例：student_system（数据库名）/ stu_info（表名） / student_name（字段名）

反例：studentSystem (数据库名) / stu-info（表名） / studentName（字段名）

2.【强制】表名：表名不使用复数名词

说明：表名仅仅表示一个表的内容，而不是实体的数量，所以不需要使用复数名词。

反例：students（学生表）

3.【强制】禁止使用保留字，如 desc，range，match，delayed 等 MySQL 官方的保留字。

4.【推荐】数据库名最好和应用的名称一致。

5.【推荐】表的命名：最好使用 **业务名称_表的作用** 来命名

正例：teacher_info（教师信息表） / teacher_position（教师职位表）

### （二）类型

1.【强制】使用合适的字段类型，创建一个字段的类型时，要考虑到字段实际的长度，不能随便给字段设置数据类型长度，尽量不要使用数据类型的默认长度。

说明：合适的字段类型及长度，不但可以节约数据库表的空间，节约索引存储，还可以提高检索的速度。

正例：如下表所示

| 对象 | 年龄区间 | 类型 | 字节数 | 表示的范围 |
|:---:|:---:|:---:|:---:|:---:|
| 人 | 150岁之间 | unsigned tinyint | 1 | 无符号值：0 ~ 255 |
| 龟 | 数百岁 | unsigned smallint | 2 | 无符号值：0 ~ 65535 |
| 恐龙化石 | 数千万年 | unsigned int | 4 | 无符号值：0 ~ 42.9亿 |
| 太阳 | 约 50亿年 | unsigned smallint | 2 | 无符号值：0 ~ 约 10 的 19次方 |

2.【强制】字符串：如果存储的字段字符串的长度固定或者几乎相等，使用 char 定长字符串类型，如果是长度是可变的，使用 varchar 可变长的字符串类型。

说明：使用定长字符串查询效率是比较高的，注意 char 和 varchar 的长度指的是字符的个数，不考虑编码对字节的影响，也不考虑字符是英文还是中文。

正例：student_id（9位学号） char(9) / name varchar(5)

反例：student_id（9位学号） varchar(9) / name varchar（250）

3.【强制】字段名：表达是否概的字段，使用 is_xxx 的方式命名，数据类型使用 unsigned tinyint 类型，1 代表是，0 代表否。

说明：任何字段如果为非负数，必须是 unsigned。

正例：表示逻辑删除的字段名：is_delete，1 代表删除，0 表示未删除。

4.【强制】小数：数据类型使用 decimal，并手动设置小数的长度以及小数点后的位数，禁止使用 float 和 double 类型。

说明：float 和 double 在 存储小数的时候，会存在精度丟失的问题，进行值比较的时候，得不到正确的结果。如果存储数据的范围超过 decimal 的范围，建议将数据的整数部分和小数部分分开存储。

正例：grade（绩点）decimal(5,2) 数据 3.6988

反例：grade（绩点）float 数据 3.6988

### （三）ORM 映射

### （四）其他

1.【推荐】SQL 语句：MySQL 的关键字、保留字、聚合函数都建议大写。

说明：SQL 语句是不区分大小写，但是在 SQL 语句执行的时候，是先转化为大写字母，如果大写就可以节省一个转化的时间，同时也便于和表名，字段名区分，出错时也容易查找。

正例：`SELECT COUNT(*) FROM student`

参考文档：

- BootStrap 编码规范

- 阿里巴巴 Java 开发手册