![hibernate](http://upload-images.jianshu.io/upload_images/5763525-8815aec4c3409034.gif?imageMogr2/auto-orient/strip)

## 1.什么是框架
---

什么是框架呢？个人觉得在软件设计中，框架可以看作是架构组件。如果把整个程序看作是一个人的话，那么框架可以看出是一个人的骨架，我们要做的的就是在这个框架的基础上进行开发，完成整个程序，让这个只有骨架的“人”活起来。而一些代码库可以看成是封装了某些功能的组件，而框架就是由很多库组成的。


## 2.SSH 框架介绍
---

SSH 是 spring struts hibernate 的缩写。从名字的缩写就可以看出，SSH 是一个集成框架，它是现在比较流行的 Web 应用程序的开源框架。个人的学习路线是 hibernate -> struts -> spring。hibernate 是操作数据库的框架，属于 Web 开发的 DAO 数据库操作层，底层是通过 jdbc 实现的，所以只要有jdbc基础的，都可以先从 hibernate 学起，hibernate也可以用在一般的 java项目的开发的。

## 3.认识 hibernate
---

hibernate 使用了 orm 的设计思想，orm 是 object relationship mapping，对象映射关系的缩写。即是实体类和数据库表是一一的映射（对应）关系。具体来说，实体类中的属性和数据表的字段是一一对应的。这样做有什么好处呢？这会大大简化我们对数据库的操作，我们操作数据库的方式，从原来的要写很多 sql 语句，改为通过数据库表对应的实体对象来操作数据库。


---

下面我们通过一个 java项目来学习 hibernate的基础使用。

---


![项目的目录结构](http://upload-images.jianshu.io/upload_images/5763525-a72189e3a52775be.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 4.搭建 hibernate框架环境
---

### 4.1 准备 hibernate包

heinate 框架是由一些 jar包组成的，我们首先要去官网下载 hibernate 的 zip压缩包。这里需要注意的是 hibernate 的版本，不同版本的 hibernate 所需的 jdk的版本也是不同的，2017年，hibernate 的版本是5.2。所需的 jdk 支持要是 jdk1.8。低于 jdk1.8的 jdk版本无法使用 hibernate 5.2。个人测试过 hibernate 5.0，需要 jdk1.7以上的支持。不知道为什么，Oracle 的官网，现在无法直接下载 jdk1.7的版本了，需要登陆，可能是 Oracle官网想提倡开发者使用最新的 jdk版本来开发吧，毕竟 jdk1.8，也就是 java8，里面还是提供了很多新功能的，也肯定在之前的版本上做了优化。个人看法是，当前的开发，肯定是使用最新的 jdk版本来开发，jdk的老版本是做测试用的。

### 4.2 导入 hibernate所需文件。

从 hibernate官网下载完压缩包后，找到目录下 lib文件夹下的 required文件夹，这里面的文件，就是 hibernate框架所需的所有文件啦，但由于 hibernate的底层还是 jdbc，所以我们还需要对应数据库的驱动文件。个人建议把这些 jar包单独存放在一个自定义的文件下，也便于使用。

### 4.3 配置路径

在 java项目下新建一个lib文件夹来存放我们的 hibernate 的 jar包和数据库连接的 jar包，并把这些包全部加到配置路径中。
![hibernate 配置](http://upload-images.jianshu.io/upload_images/5763525-d6e2a679fa41e979.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 5.使用 hibernate框架
---

### 5.1 创建实体类

用 private 访问限制词修饰，用 get，set 方法来设置属性的值，可以省略默认的构造方法。

```java
package cc.wenshixin.entity;

public class Student {
	private int id;// 序号
	private String name;// 姓名
	private String sex;// 性别
	private String grade;// 班级
	
	public Student(String name, String sex, String grade) {
		this.name = name;
		this.sex = sex;
		this.grade = grade;
	}

	//这里省略了get和set方法
}

```
### 5.2 创建实体类映射文件

建议映射文件和实体类文件在同一个包下，映射文件命名为：实体类名称.hbm.xml。dtd 为 xml 的文件约束，下面的写法使需要在联网状态下才会有 xml 的代码提示的，关于离线dtd的配置，可自行上网搜索。所谓文件约束就是规定了文件内容的书写格式，具体就是标签的包裹，标签的属性等等。映射文件可以使用 jboss-tool插件生成，可自行根据自己使用的 IDE 工具使用。

下面说一下映射关系文件的配置，具体见下面代码所示。
1.首先是 `hibernate-mapping` 标签，包裹整个映射关系。
2.然后是 `class` 标签，里面 `name` 属性的值为实体类的全路径，`table` 为自动创建的数据表的名称。
3.`id` 标签配置数据表的主键，`name` 属性为实体类中作为唯一值的属性名称，`column` 属性作为生成的数据表的字段名称。`id` 标签里面的`generator` 是用来设置主键增长的策略，`native` 值为自动增长，`assigned` 为自定义增长。
4.`property` 标签设置一般的属性值，`name` 属性还是实体类的属性，`column` 是数据表的字段，这个和前面的`id` 标签是一样的。

```xml
<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">

<hibernate-mapping>
    <class name="cc.wenshixin.entity.Student" table="STUDENT">
        <id name="id" column="id">
            <generator class="native" />
        </id>
        <property name="name" column="name"></property>
        <property name="sex" column="sex"></property>
        <property name="grade" column="grade"></property>
    </class>
</hibernate-mapping>
```

### 5.3 创建 hibernate 核心配置文件

名称为 hibernate.cfg.xml，必须放在 src目录下面，和上面的实体类映射文件一样，核心配置文件也是有 dtd文件约束的。

下面说一下 hibernate 核心文件的配置，具体见下面代码所示。
1.首先是 `hibernate-configuration` 标签包裹整个配置信息。
2.接着是 `session-factory` 标签包裹整个配置内容。
3.配置的标签是 `property`，`name` 为配置项的名字，标签里面是配置的内容。
4.配置的第一步是配置数据库相关的信息，数据库的驱动类，数据库连接的url参数，数据库的用户名，用户的密码。
5.hibernate 的功能选择，`show_sql` 是开启 hibernate中显示sql语句的功能，方便我们在开发中做调试；`format_sql`是对显示的sql语句进行格式话，方便我们阅读sql语句；`hbm2ddl.auto` 是关于表的更新操作，update 的意思是，如果表不存在就创建，如果表存在就更新表的内容。还有 create 的意思是，如果表不存在就创建，但如果表存在，就覆盖原来的表，创建新的表，那么原来表中的内容就没有了；`dialect` 是数据库方言的配置，在不同的类型的数据，一些 sql 语句的书写是不一样的，例如，分页操作要用的 sql语句，MySQL 数据库是用 `limit` 关键字来实现的，而Oracle 数据库是通过`rownum` 关键字来实现的，SQL server 数据库是通过 `top` 关键字来实现的，为了避免数据库的不同而对sql语句产生的差异，我们最好是加上方言的功能。
6.因为在 hibernate 的加载时，只会加载核心配置文件，所以要把实体类映射文件加入到 hibernate核心配置文件中，这样在加载核心配置文件的时候就会把映射文件也加载进去，注意映射文件的路径要写在 `mapping` 标签的`resource` 属性中。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE hibernate-configuration PUBLIC
		"-//Hibernate/Hibernate Configuration DTD 3.0//EN"
		"http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd">
<hibernate-configuration>
    <session-factory>
      <!-- 配置数据库信息 -->
      <property name="connection.driver_class">com.mysql.jdbc.Driver</property>
      <property name="connection.url">jdbc:mysql://localhost:3306/study?useUnicode=true&characterEncoding=UTF-8</property>
      <property name="connection.username">root</property>
      <property name="connection.password">123456</property>

      <!-- hibernate功能选择 -->
      <property name="show_sql">true</property>
      <property name="format_sql">true</property>
      <property name="hbm2ddl.auto">update</property>
	<property name="dialect">org.hibernate.dialect.MySQL5InnoDBDialect</property>
      
      <!-- 加载映射文件 -->
      <mapping resource="cc/wenshixin/entity/Student.hbm.xml"/>
    </session-factory>
</hibernate-configuration>

```
### 5.4 创建 hibernate测试文件

经过前面的步骤，hibernate 的使用就已经完成了，下面是通过测试文件来测试，执行后到数据库中看是否有相应的数据表和记录。



```java
package cc.wenshixin.hibernateTest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import cc.wenshixin.entity.Student;

public class HibernateTest {
	
	public static void main(String[] args) {
		//1.加载hibernate核心配置文件
		Configuration cfg = new Configuration().configure();
		
		//2.创建一个SessionFactory工厂，在这个过程中读取 hibernate 的核心配置文件，根据映射关系把数据表创建起来
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		
		//3.使用sessionFactory来生产session对象，这里的session类似于Connection的对象，连接到数据库
		Session session = sessionFactory.openSession();
		
		//4.开启事务
		Transaction transaction = session.beginTransaction();
		
		//5.执行具体的 curd 操作
		Student s = new Student("小明","男","计科2班");
		session.save(s);

		//6.提交事务
		transaction.commit();
		
		//7.关闭相关的资源
		session.close();
		sessionFactory.close();
	}
}

```

## 6.hibernate框架配置详解
---

### 6.1 hibernate映射配置文件

1.映射配置文件的位置和名称都是可以修改的，但我们习惯上，把映射配置文件的名称写成 类名称.hbm.xml，并且和实体类文件放在同一个包下
2. 映射配置文件中，标签 `name` 属性的值是写实体类相关的内容，`class` 标签中的 `name` 属性值是实体类的全路径，`id` 标签和 `property` 标签的 `name` 属性值是实体类属性的名称。
3. `id` 标签和 `property` 标签 `column` 属性是可以省略的，不写的话表的字段名称默认和 `name` 属性的值一样。
4. `property` 标签中还有 `type` 属性，设置生成表的字的字段的类型，不写的话，hibernate自动设置对应的类型。还有 `length` 属性，设置字段类型的长度，在实际的开发中，这个属性是要写的，用来减少数据库的冗余，提高数据表的查询效率。

### 6.2 hibernate 核心配置文件

1.核心配置文件中三个部分的要求，数据库配置信息部分是必须的，接着是选择使用 hibernate 的那些功能部分，最后是加载映射文件，这个也是必须的。

2.核心配置文件的文件名称和文件的位置都是固定的，名称：hibernate.cfg.xml，位置是直接在src源文件目录下。

## 7.hibernate的核心API
---

#### 7.1 Configuration

`Configuration cfg = new Configuration().configure();`
在 src目录下找到 hibernate..cfg.xml 配置文件，把配置文件放到创建的配置对象中，简单的可以理解为加载核心配置文件项。

### 7.2 SessionFactory

`SessionFactory sessionFactory = cfg.buildSessionFactory();`
使用 configuration 对象来创建 sessionFactory 的对象，在创建这个对象的过程中，要加载核心配置文件的映射文件，根据映射关系来把数据表创建起来，可能要有多个数据表的创建，所以创建一个 sessionFactory 的对象挺耗费资源的，一个项目建议只创建一个 sessionFactory 对象，一般我们是把这步代码写到静态代码块中，这样在类加载时就会执行静态代码块的代码，并且只执行一次。

### 7.3 Session

`Session session = sessionFactory.openSession();`
session 对象类似于我们在jdbc中的connection对象，session 里面有对数据表操作的各种方法，从而实现对数据表的 curd 操作。
	- 1.save方法，插入一条记录
	- 2.update方法，更新一条记录
	- 3.delete方法，删除一条记录
	- 4.根据id查询的get方法
需要说明的时，session 是线程不安全的，也即是 session 对象是不能被多个线程共用的，只能单一线程使用。

### 7.4 Transaction

`Transaction transaction = session.beginTransaction();`
开启数据库的事务，session对象处理完数据库操作时，这里需要手动的提交事务，事务有提交操作 `transaction.commit();`，还有回滚操作 `transaction.rollback();`，关于事务的相关概念，事务的四个特性：原子性、一致性、隔离性、持久性。
