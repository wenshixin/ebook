![hibernate](http://upload-images.jianshu.io/upload_images/5763525-8815aec4c3409034.gif?imageMogr2/auto-orient/strip)


## 1.表关系
---

### 1.1 数据表基本知识

首先来回顾一下数据库的基本知识。
- 主键：数据表中能够唯一标识一条记录的字段或者字段的组合就称为主键。一张数据表的每一条记录的主键都是唯一不重复的。主键可以唯一标识一条记录只是主键的一个作用，主键的另一个作用是和其他记录和存放数据的表进行关联，此时一张主键就成了另一张表的外键。
- 外键：一张数据表和其他数据表建立关联的字段就称为是外键。通过一个外键字段，一张数据表就可以知道与之关联的另一张数据表的所有数据，一张数据表的外键字段可以有多个，也即是一张数据表可以和多个数据表关联。拥有外键的数据表可以称之为基本表，与之关联的表称为信息表。
- 索引：一张数据表中主键是唯一标识一条记录的，而索引也具有这个功能，作为索引的字段的每个数据值都要是在这张数据表唯一不重复的。例如：一个人的身份证号码就可以作为一张数据表的索引字段。

主外键关系：外键一定是另一张表的主键。

### 1.2 三种表关系

- #### 1.2.1 一对一的表关系
表 A 中的一条记录只能和表 B 的一条记录，反之亦然。这种关系的表并不常见，因为既然可以存放在两张表中的数据，也可以用一张表来存放。一对一的表关系用于：
1.分割一个含有有许多列的表
2.由于安全性的考虑而隔离了表的某一部分
3.存储一些很容易删除的临时数据，删除这些表就是删除了这些数据
4.存储只应用于主表的一个子集的信息

举个例子，按法律规定，一个男人只有一个妻子，一个女人也只有一个丈夫。如果把男人放在一张表中，女人在另一张表中，那么男人表中的一条记录对应女人表的记录只能是唯一的，当然这种情况更多的是把他们存放在一张表中。

建表的原则是：
1. 唯一外键对应，假设一对一中，任意一方为多，那么在多的一方创建外键指向一的一方的主键，然后将外键设置为唯一的。例如：一个男人可以找很多女人，但是法律上的妻子一次只能有一个。
2.主键对应：一方的主键作为另一方的主键。

- #### 1.2.2 一对多的表关系
表 A 的一条记录在表 B 中可以匹配多行，但在表 B 中的一条记录在表 A 中只能有一个匹配行，这里的表 A 就称为关系的一方，表 B 就称为关系的多方。一对多的表关系是比较常见的，例如在学生表中，一个学生属于一个班级，而在班级表中，一个班级可以有很多学生，这种关系就称之为一对多的关系。这时学生表中就有一个字段作为班级表的外键，学生就是多的一方，班级是一的一方，在多的一方创建外键，指向一的一方的主键。

建表的原则是：
在多的一方创建外键指向一的一方的主键。

- #### 1.2.3 多对多的表关系
表 A 中的一条记录可以在表 B 中匹配多行，反之亦然。例如一个学生可以选多个课程，一个课程也有很多学生选择，这种关系就是多对多的关系，多对多的关系要借助第三张表来维护两方之间的表关系，学生和课程之间的关系存放在一张关系表中，这张关系表的主键字段是学生表主键和课程表主键的组合。

建表的原则是：
创建一个中间表，中间表中至少有两个字段作为外键来分别指向两张对地多双方的主键。


## 2.一对多映射
---

### 2.1 基本代码

下面通过学生和班级的例子来说明

- #### 学生实体类

学生实体类的属性中要加上班级实体类的对象。

```java
package cc.wenshixin.entity;

public class Student {

	private Integer id; // 学生学号
	private String name; // 学生姓名
	private String sex; // 学生性别

	private Banji banji; // 一个学生只属于一个班级

	public Student(String name, String sex) {
		this.name = name;
		this.sex = sex;
	}

	public Student() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Banji getBanji() {
		return banji;
	}

	public void setBanji(Banji banji) {
		this.banji = banji;
	}
}

```
- #### 班级实体类
班级中的学生用set集合来保存。

```java
package cc.wenshixin.entity;

import java.util.HashSet;
import java.util.Set;

public class Banji {

	private Integer id; //班级编号
	private String name; //班级名称
	
	private Set<Student> setStudent = new HashSet<Student>(); //一个班级有多个学生

	public Banji() {

	}

	public Banji(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Student> getSetStudent() {
		return setStudent;
	}

	public void setSetStudent(Set<Student> setStudent) {
		this.setStudent = setStudent;
	}
}

```
- ####配置实体类映射文件

学生实体类的映射文件
`<many-to-one name="banji" class="cc.wenshixin.entity.Banji" column="banji"></many-to-one>` 其中 `name` 为 实体类的班级属性名，`class` 为 `name` 属性值类的全路径名，`column` 为学生表中的外键字段的名称。

```xml
<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<hibernate-mapping>
    <class name="cc.wenshixin.entity.Student" table="student">
        <id name="id" type="java.lang.Integer" column="sid">
            <generator class="native" />
        </id>
        <property name="name" type="java.lang.String" column="sname">
        </property>
        <property name="sex" type="java.lang.String" column="sex">
        </property>
        <many-to-one name="banji" class="cc.wenshixin.entity.Banji" column="banji">
        </many-to-one>
    </class>
</hibernate-mapping>
```
班级实体类映射文件
```xml
	<set name="setStudent" table="student">
	    <key column="banji">
	    </key>
	    <one-to-many class="cc.wenshixin.entity.Student" />
	</set>
```
`name` 属性为班级实体类中的set集合名，`table` 为学生实体类创建的数据表名，可以省略不写，因为 hibernate 是默认是双向维护外键的，即在一方和多方都要配置外键，`key` 标签的 `column` 为学生实体类数据表中的外键名，要保持一致，`one-to-many` 标签中的 `class` 为学生实体类的全路径名。

```xml
<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<hibernate-mapping>
    <class name="cc.wenshixin.entity.Banji" table="banji">
        <id name="id" type="java.lang.Integer" column="bid">
            <generator class="native" />
        </id>
        <property name="name" type="java.lang.String" column="bname">
        </property>
        <set name="setStudent" table="student" inverse="true" cascade="save-update, delete">
            <key column="banji">
            </key>
            <one-to-many class="cc.wenshixin.entity.Student" />
        </set>
    </class>
</hibernate-mapping>
```
注意：要在核心配置文件中加载实体类的映射文件。


hibernate 工具类

便于在测试代码中更好使用 hibernate。

```java
package cc.wenshixin.utility;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtility {
	private static Configuration cfg = null;
	private static SessionFactory sessionFactory = null;
	
	static{
		cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
	}
	
	/*提供方法返回sessionFactory*/
	public static SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
}
```

### 2.2 一对多关系的操作

- #### 普通的保存

缺点是需要保存两次，效率不高。

```java
	@Test
	//普通保存
	public void testSave1()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			
			//添加学生和班级
			Banji banji = new Banji("计科二班");
			Student student = new Student("小明", "男");
			
			//班级对象中添加学生
			banji.getSetStudent().add(student);
			//学生对象中设置班级
			student.setBanji(banji);
			
			//保存班级和学生
			session.save(banji);
			session.save(student);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally{
			session.close();
		}
	}
```
- #### 一对多的级联保存

保存一方，与之关联的多方就会被自动保存，有方向性的保存操作。

需要在一方的映射文件中，set 标签中加上 `cascade="save-update"`，这样就可以只写保存班级，添加到班级里的学生就会自动保存。

```java
	@Test
	//一对多级联保存写法
	public void testSave2()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			
			//添加学生和班级
			Banji banji = new Banji("计科八班");
			Student student = new Student("小红", "女");
			
			banji.getSetStudent().add(student);

			//只需保存学生
			session.save(banji);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally{
			session.close();
		}
	}
```
- #### 普通的删除

只删除一个班上的学生，而不会删除班级。

```java
	@Test
	//普通的删除
	public void testDelete1()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try{
			//执行删除
			Banji banji = session.get(Banji.class, 15);
			Student student = session.get(Student.class, 11);
			
			banji.getSetStudent().remove(student);
			session.delete(student);
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}	finally {
			session.close();
		}
	}
```
- #### 一对多的级联删除

删除班级时，这个班上的学生也要全部被删除，但由于外键的约束，无法直接删除班级，并删除学生，在hibernate 中如果直接删除班级，那么与之关联的学生的外键值将被更改为 null，如果想把班级和学生同时删除，那么需要在班级实体类的映射文件中的key标签里面加上 `cascade="save-update, delete"`，不同的属性值之间用逗号隔开，实际上 hibernate 的删除操作也是先把 外键设为 null，切断关联，然后再将其删除。

```java
	@Test
	//一对多级联删除
	public void testDelete2()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try{
		//执行删除
			Banji banji = session.get(Banji.class, 12);
			session.delete(banji);
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}	finally {
			session.close();
		}
	}
```
- #### 一对多的级联修改

因为 hibernate 时双向维护外键的，所以在一方和多方的数据表中都要维护外键，修改任意一方的外键值，另一方的也要随之修改，这就会造成效率上的问题，为此我们可以让一方放弃维护外键，在一方的映射文件中 `set` 标签中加上 `inverse="true"` 配置即可,默认hibernate 是不放弃外键维护的，也即是 false。

```java
	@Test
	//一对多级联修改
	public void testUpdate()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx= session.beginTransaction();
		
		try{
			//执行修改操作
			Banji banji = session.get(Banji.class, 14);
			Student student = session.get(Student.class, 10);
			banji.getSetStudent().add(student);
			student.setBanji(banji);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}
```

## 3.多对多映射
---

### 3.1 基本代码

下面通过学生和课程的例子来说明

- #### 学生实体类

学生实体类的属性中要加上课程实体类的集合对象。

```java
package cc.wenshixin.entity;

import java.util.HashSet;
import java.util.Set;

public class Student {
	private Integer id; // 学生学号
	private String name; // 学生姓名
	private String sex; // 学生性别

	private Set<Course> setCourses = new HashSet<Course>(); // 一个学生可以选多个课程

	public Set<Course> getSetCourses() {
		return setCourses;
	}

	public void setSetCourses(Set<Course> setCourses) {
		this.setCourses = setCourses;
	}

	public Student(String name, String sex) {
		this.name = name;
		this.sex = sex;
	}

	public Student() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}

```

- #### 课程实体类

课程实体类的属性中要加上学生实体类的集合对象。

```java
package cc.wenshixin.entity;

import java.util.HashSet;
import java.util.Set;

public class Course {
	
	private Integer id; //课程编号
	private String name; //课程名称
	
	private Set<Student> setStudent = new HashSet<Student>(); //一个课程有多个学生选择

	public Course() {

	}

	public Course(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Student> getSetStudent() {
		return setStudent;
	}

	public void setSetStudent(Set<Student> setStudent) {
		this.setStudent = setStudent;
	}
	
}
```
- ####配置实体类映射文件

学生实体类的映射文件

`set` 标签中的 `table` 为第三张表的名字，`key` 标签中的 `column` 属性的值为第三张表的一个字段名，`many-to-many` 标签中的 `class` 为另一个多方的实体类的全路径名，`column` 属性的值为另一个实体类在第三张表中的字段名。

```xml
<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<hibernate-mapping>
    <class name="cc.wenshixin.entity.Student" table="student">
        <id name="id" type="java.lang.Integer" column="sid">
            <generator class="native" />
        </id>
        <property name="name" type="java.lang.String" column="sname">
        </property>
        <property name="sex" type="java.lang.String" column="sex">
        </property>
        <set name="setCourses" table="student_course">
            <key column="stutentid">
            </key>
            <many-to-many class="cc.wenshixin.entity.Course" column="courseid"/>
        </set>
    </class>
</hibernate-mapping>
```

课程实体类的映射文件

同实体类的映射配置解释一样。

```xml
<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<hibernate-mapping>
    <class name="cc.wenshixin.entity.Course" table="course">
        <id name="id" type="java.lang.Integer" column="cid">
            <generator class="native" />
        </id>
        <property name="name" type="java.lang.String" column="cname">
        </property>
        <set name="setStudent" table="student_course">
            <key column="courseid">
            </key>
            <many-to-many class="cc.wenshixin.entity.Student" column="stutentid"/>
        </set>
    </class>
</hibernate-mapping>
```
对应的映射配置关系图

![映射配置关系对应图](http://upload-images.jianshu.io/upload_images/5763525-ec56326fba6c8f46.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


### 3.2 多对多关系的操作

- #### 普通的保存

语句啰嗦。

```java
	@Test
	//普通保存
	public void testSave1()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try{
			//执行保存操作
			Student s1 = new Student("小明", "男");
			Student s2 = new Student("小红", "女");
			
			Course c1 = new Course("php动态网站开发");
			Course c2 = new Course("java程序设计");
			Course c3 = new Course("设计模式");
			
			//学生选课，课程被选择
			s1.getSetCourses().add(c1);
			s1.getSetCourses().add(c2);
			s2.getSetCourses().add(c2);
			s2.getSetCourses().add(c3);
			
			session.save(s1);
			session.save(s2);
			session.save(c1);
			session.save(c2);
			session.save(c3);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}
```
- #### 多对多级联保存

在学生实体类中映射配置的 `set` 标签中加上 `cascade="save-update"`，只需要保存学生即可，被学生选择的课程会被自动保存。

```java
	@Test
	//级联保存
	public void testSave2()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try{
			//执行保存操作
			Student s1 = new Student("小明", "男");
			Student s2 = new Student("小红", "女");
			
			Course c1 = new Course("php动态网站开发");
			Course c2 = new Course("java程序设计");
			Course c3 = new Course("设计模式");
			
			//学生选课，课程被选择
			s1.getSetCourses().add(c1);
			s1.getSetCourses().add(c2);
			s2.getSetCourses().add(c2);
			s2.getSetCourses().add(c3);
			
			session.save(s1);
			session.save(s2);

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}
```
- #### 普通的删除

只删除一个多方的数据，而不影响另一个多方。

```java
	@Test
	//普通删除
	public void testDelete1()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try{
			//删除操作
			Student s1 = session.get(Student.class, 1);
			session.delete(s1);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}
```
- #### 多对多级联删除

删除一个多方的同时影响另一个多方的数据，需要一个多方的在配置文件中 `set` 标签里面加 `cascade="save-update, delete"`。

```java
	@Test
	//级联删除
	public void testDelete2()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try{
			//删除操作
			Student s1 = session.get(Student.class, 1);
			session.delete(s1);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}
```
- #### 多对多级联修改

不改变两个多方的数据表的记录，而是维护第三张表的中的关系记录，下面的修改操作包括增加一条关系记录和删除一条关系记录。

```java
	@Test
	//级联修改
	public void testUpdate()
	{
		SessionFactory sessionFactory = HibernateUtility.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try{
			//修改操作
			Student s1 = session.get(Student.class, 1);
			Course c3 = session.get(Course.class, 3);
			//让学生选课程3
			s1.getSetCourses().add(c3);
			
			Student s2 = session.get(Student.class, 2);
			Course c1 = session.get(Course.class, 1);
			//让学生不选课程1
			s2.getSetCourses().remove(c1);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			session.close();
		}
	}
```
