![spring.jpg](http://upload-images.jianshu.io/upload_images/5763525-455d61d269a0d28c.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 1.Spring 框架介绍
---

Spring 是一个开源的轻量级 Java 开发框架，可以解决业务逻辑层和其他层的耦合太高的问题，它不仅可以用在 Java EE 上，对于 Java SE 同样可以使用。美其名曰，Spring 的出现算是给软件开发带来了春天，它的分层架构可以使 Spring 框架搭配其他的框架使用，如 Struts2，Hibernate，三者总称为 SSH 框架。Spring 不仅仅是一个框架，控制反转（IOC）和面向切面编程（AOP）的设计思想才是其精华所在。

Spring 中 Java EE 开发的三层结构：
- web 层：Spring MVC
- service 层：Bean 管理
- dao 层：Spring 的 JDBCTemplate 整合其他持久层框架，如：Hibernate

## 2.spring 中的概念
---

### 2.1 AOP（面向切面编程）

这个在 Strust2 中也提到了，Struts2 的拦截器就是面向切面编程的一种实现方式，在原有的功能基础上扩展功能不用修改源代码，而是通过配置文件。

### 2.2 IOC（控制反转）

将对象的创建权交给 Spring，而不是通过 new 的方式来创建对象，这样也符合软件开发中 **高内聚低耦合** 的设计理念，尽可能的降低对象与对象之间的调用关系。

## 3.搭建 Spring 框架

### 3.1 准备 Spring 的相关 jar 包

在 Spring 框架的官网上，下载按钮并不是在显眼的位置，这里提供一个 Spring 各个版本 [下载地址](https://repo.spring.io/release/org/springframework/spring/)，我下载的是 4.3.9 版本的 zip 压缩包。解压后，docs 文件夹是 Spring 的 API 文档和开发规范，libs 文件夹 是 Spring 的 jar 包和源码，scheme 文件夹 是 Spring 的约束文件。在 libs 文件夹内，每三个 jar 包是一组，包括开发用的 jar 包，说明文档，源码。

### 3.2 导入 Spring 的相关 jar 包

libs文件夹下的 spring-beans，spring-context，spring-core，spring-expression 这四个 jar 包，还有一个记录日志的 jar 包 commons-logging，这个 jar 包不导入的话在控制台会出现 `java.lang.ClassNotFoundException` 的错误，但这个包并不是 Spring 框架中的，属于 Apache 项目中的一个开发组件，这里也提供一个 [下载地址](http://www-us.apache.org/dist//commons/logging/binaries/commons-logging-1.2-bin.zip)，在 java web项目中的lib文件夹下导入这五个 jar 包。

### 3.3 Spring 的使用案例

实体类

```java
	package cc.wenshixin.entity;
	
	public class Student {
		
		public void study()
		{
			System.err.println("学习中。。。");
		}
	}
```
配置文件

Spring 的配置文件的名称和位置不是固定的，但建议直接放在 src 目录下便于和其他框架的配置文件统一管理，另外官方建议的配置文件名是 applicationContext.xml，但也可以自己设定配置文件名称。和 Struts2、Hibernate 框架中的配置文件的约束文件类型不同，前两者是 dtd 约束，而后者是 scheme 约束。关于scheme的约束模板，可以在这个 [地址](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html) （在靠后的部分）上找，也可以抄下面基本的模板约束。

```xml
	<?xml version="1.0" encoding="UTF-8"?>
	<beans xmlns="http://www.springframework.org/schema/beans"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	        xsi:schemaLocation="
	                http://www.springframework.org/schema/beans
	                http://www.springframework.org/schema/beans/spring-beans.xsd">
	  <bean id="student" class="cc.wenshixin.entity.Student"></bean>
	</beans>
```
测试类

使用IOC（控制反转）技术（在下面会详细介绍IOC的底层实现原理），对象创建的工作就交给 Spring 来完成，加上类在配置文件中的 id 名就可以返回创建的对象，这里面是工厂模式的设计思想。

```java
	package cc.wenshixin.test;
	
	import org.junit.Test;
	import org.springframework.context.ApplicationContext;
	import org.springframework.context.support.ClassPathXmlApplicationContext;
	
	import cc.wenshixin.entity.Student;
	
	public class Test1 {
		
		@Test
		public void test01()
		{
			//1.加载spring的配置文件，根据配置文件来创建对象
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			//2.得到配置中创建的对象
			Student student = (Student) context.getBean("student");
			student.study();
		}
	}
```

## 4.Spring 中的 bean 管理
---

### 4.1 IOC 的底层实现原理

IOC 的底层是通过 dom4j 解析 xml 文件、工厂设计模式，反射机制实现的，如下图所示。

![IOC底层原理.png](http://upload-images.jianshu.io/upload_images/5763525-3ed40a176e3108c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 4.2 bean 实例化对象的三种方式

**- 第一种 使用类的无参构造方法创建（重点）**

上面的使用案例中就是使用的这种创建对象方式，比较简单，在开发中经常使用。注意在实体类中要有无参数的构造函数，否则 Spring 无法创建实体类对象，出现异常。如果在实体类中有有参数的构造函数，要手动补上无参数的构造方法，来方便 Spring 的调用。

- 第二种 使用静态工厂创建

使用一个工厂类，创建静态的方法，返回对象

实体类代码同上

工厂类

```
	package cc.wenshixin.factory;
	
	import cc.wenshixin.entity.Student;
	
	public class StudentFactory1 {
		public static Student getStudent()
		{
			return new Student();
		}
	}
```
配置文件

```
  <!-- 使用静态工厂创建 -->
  <bean id="studentFactory1" class="cc.wenshixin.factory.StudentFactory1" factory-method="getStudent"></bean>
```
测试类

```java
	@Test
	public void test02()
	{
		//1.加载spring的配置文件，根据配置文件来创建对象
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.得到配置中创建的对象
		Student student = (Student) context.getBean("studentFactory1");
		student.study();
	}
```

- 第三种 使用实例工厂创建

使用一个工厂类，创建普通的方法，返回对象

实体类代码同上

工厂类代码

```java
	package cc.wenshixin.factory;
	
	import cc.wenshixin.entity.Student;
	
	public class StudentFactory2 {
		public Student getStudent()
		{
			return new Student();
		}
	}
```
配置文件

```xml
  <!-- 使用实例工厂创建 -->
  <bean id="studentFactory2" class="cc.wenshixin.factory.StudentFactory2"></bean>
  <bean id="stud" factory-bean="studentFactory2" factory-method="getStudent"></bean>
```
测试类

```java
	@Test
	public void test03()
	{
		//1.加载spring的配置文件，根据配置文件来创建对象
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.得到配置中创建的对象
		Student student = (Student) context.getBean("stud");
		student.study();
	}
```

### 4.3 bean 标签中的常用属性

- id 属性：给 bean 起名字，id 属性名称可以任意，但不能包含特殊符号，用于根据 id 属性值得到配置对象。
- class 属性：创建对象所在类的全路径
- name 属性：功能和 id 属性一样，但 name 属性名称中可以包含特殊符号
- scope 属性：
	- 1.`singleton` 属性默认值，单个对象
	- 2.`prototype` 多个对象
	- 3.`request` 创建对象并把对象放在 request 域中
	- 4.`session` 创建对象并把对象放在 session 域中
	- 5.`globalSession` 创建对象并把对象放在 globalSession 中

scope属性的前两个属性值常用，后面几个知道即可.

测试 `singleton` 属性值，默认的可以不写，修改上面测试类中的代码如下：

```java
	@Test
	public void test01()
	{
		//1.加载spring的配置文件，根据配置文件来创建对象
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.得到配置中创建的对象，使用单例模式，两个对象引用的地址相同
		Student s1 = (Student) context.getBean("student");
		Student s2 = (Student) context.getBean("student");
		System.out.println(s1);
		System.out.println(s2);
	}
```
测试 `prototype` 属性值，修改配置文件的代码如下：

```xml
	<bean id="student" class="cc.wenshixin.entity.Student" scope="prototype"></bean>
```
再次执行上面的测试类代码，观察两个对象引用的地址不同，即创建了多个对象。

### 4.3 bean 中属性注入

所谓属性注入就是在创建类对象时向对象的属性中设置值，名字起的有些高大上。

一般属性注入有三种方式

- 1.使用 set 方法注入

```java
	public class Student{
	  private String name;
	  public setName(String name){
	    this.name = name
	  }
	}
	//set方法注入
	Student student = new Student();
	student.setName("小明");
```

- 2.使用有参构造函数注入

```java
	public class Student{
	  private String name;
	  public Student(String name){
	    this.name = name
	  }
	}
	//构造函数注入
	Student student = new Student("小明");
```
- 3.使用接口注入

```java
public interface Dao{
  public void setName(String name);
}
public class DaoImpl implements Dao{
  private String name;
  public void setName(String name){
    this.name = name;
  }
}
```
但在 Spring 中，只支持前两种方式，也即是 set 方法注入和构造函数注入，通过配置文件来是使对象被创建时给对象的相关属性赋值。

**- 使用 set 方法注入属性**

修改实体类为

```java
	package cc.wenshixin.entity;
	
	public class Student {
		private String name;

		public void setName(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
	}
```
配置文件

```xml
  <!-- 使用方式注入属性 -->
  <bean id="student" class="cc.wenshixin.entity.Student">
    <property name="name" value="小明"></property>
  </bean>
```

测试方法

```
	@Test
	public void test01()
	{
		//1.加载spring的配置文件，根据配置文件来创建对象
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.得到配置中创建的对象
		Student s = (Student) context.getBean("student");
		//输入配置文件中注入的属性值
		System.out.println(s.getName());
	}
```
- 使用有参构造函数注入属性

实体类增加有参构造方法

配置文件修改为

```xml
  <!-- 使用有参构造函数注入属性 -->
  <bean id="student" class="cc.wenshixin.entity.Student">
    <!-- 使用有参构造注入 -->
    <constructor-arg name="name" value="小红"></constructor-arg>
  </bean>
```
测试方法同上

- 注入对象类型的属性

创建一个 servic 类和 dao 类，在 service 中得到 dao 的对象，这在 Java Web 开发中是很常见的。

示例代码如下：

实体类同上

dao 类

```java
	package cc.wenshixin.entity;
	
	public class Dao {
		public void insert()
		{
			System.out.println("Dao插入学生数据。。。");
		}
	}
```
service 类

```java
	package cc.wenshixin.entity;
	
	public class Service {
		//定义到类型属性
		private Dao dao;
		//生成set方法
	
		public void setDao(Dao dao) {
			this.dao = dao;
		}
		
		public void add()
		{
			System.out.println("Service添加操作");
			dao.insert();
		}
	}
```
配置文件

`property` 标签中，`name` 属性值为 `service` 类中的属性名称，`ref` 属性值为dao中配置中 `id` 属性值，这里就不能写 `value` 属性了，因为是一个对象，无法属性注入。

```xml
  <!-- 配置service和dao对象 -->
  <bean id="dao" class="cc.wenshixin.entity.Dao"></bean>
  <bean id="service" class="cc.wenshixin.entity.Service">
    <property name="dao" ref="dao"></property>
  </bean>
```
测试方法

```java
	@Test
	public void test03()
	{
		//1.加载spring的配置文件，根据配置文件来创建对象
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.得到配置中创建的对象
	  Service service = (Service) context.getBean("service");
	  service.add();
	}
```
- P 名称空间属性注入

在配置文件的约束中加上 `xmlns:p="http://www.springframework.org/schema/p"`。

实体类中需要默认的构造函数和属性对应的set方法

```
package cc.wenshixin.entity;

public class Student {
	private String name;
	
	public Student() {

	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}

```
配置文件

```xml
	<?xml version="1.0" encoding="UTF-8"?>
	<beans xmlns="http://www.springframework.org/schema/beans"
	        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	        注释：加上p名称空间
			xmlns:p="http://www.springframework.org/schema/p"
	        xsi:schemaLocation="
	                http://www.springframework.org/schema/beans
	                http://www.springframework.org/schema/beans/spring-beans.xsd">
	   <!-- p名称空间注入 -->
	   <bean id="student" class="cc.wenshixin.entity.Student" p:name="老王"></bean>
	</beans>
```
测试方法

```java
	@Test
	public void test01()
	{
		//1.加载spring的配置文件，根据配置文件来创建对象
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.得到配置中创建的对象
		Student s = (Student) context.getBean("student");
		System.out.println(s.getName());
	}
```
